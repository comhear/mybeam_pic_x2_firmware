/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Device configuration functions
 *
 *  May 2014 - P Gomme   First Release
 *  Nov 2014 - SLL       Updates to enable interrupts and separate Headset for v1 boards
 *  Dec 2014 - SLL       Update for v3 PIC device
 *  Apr 2015 - SLL       Add compiler switch options for debug UART and setup of Timer2/3 for 32-bit mode
 *  Jun 2015 - SLL       Updates include;
 *                        Replace timer 2/3 with timer 4/5 to free timer 2 for security code
 *                        Change PCnBT to SPARE0
 *                        Configure RG8 as input for SPARE7 and enable internal pull-up
 *
 ******************************************************************************/

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
#include <xc.h>
#elif defined(__C30__)
#if defined(__PIC24E__)
#include <p24Exxxx.h>
#elif defined (__PIC24F__)||defined (__PIC24FK__)
#include <p24Fxxxx.h>
#elif defined(__PIC24H__)
#include <p24Hxxxx.h>
#endif
#endif

#include <stdint.h>                                                 // For uint32_t definition
#include <stdbool.h>                                                // For true/false definition
#include <uart.h>
#include <ports.h>
#include <spi.h>
#include "hwdefs.h"
#include "user.h"


void InitApp(void)
{
// Setup analog functionality and port direction

//CONFIGURING UART1 & SPI1/2 PIN MAPPINGS
// SPI1 <-> WM8280, SPI2 <-> SPI Flash EEPROM
// Unlock Registers
    __builtin_write_OSCCONL(OSCCON & 0xffBF);

// Configure Input Functions (Table 10-2))
// **************************************
// SPI1 is used for connection to WM8280 and SPI2 to EEPROM
    RPINR20bits.SDI1R = 45;                                         // Assign SPI1 MISO to pin RPI45
    RPINR22bits.SDI2R = 27;                                         // SDI2 Data In SPI2 to RPI27

// UART1 is used for connection to FTDI, UART2 for USB and UART3 for BT
    RPINR18bits.U1RXR = 38;                                         // U1RX = RPI38
    RPINR19bits.U2RXR = 42;                                         // U2RX = RPI42
    RPINR17bits.U3RXR = 44;                                         // U3RX = RPI44

// Configure Output Functions (Table 10-3)
// **************************************
// SPI1 is used for connection to WM8280
    RPOR8bits.RP16R  = SS1OUT;                                      // Assign SPI1 Slave Select Out (nCS) (function 9) to RP16
    RPOR8bits.RP17R  = SDO1;                                        // Assign SPI1 Data Out (MOSI) (function 7) to RP17
    RPOR5bits.RP10R  = SCK1OUT;                                     // Assign SPI1 Clock Output (SCLK) (function 8) to RP10
    RPOR4bits.RP8R   = SS2OUT;                                      // nCS for SPI2
    RPOR7bits.RP14R  = SDO2;                                        // SPI2 MOSI (to EEPROM)
    RPOR14bits.RP29R = SCK2OUT;                                     // SCLK for SPI2

// UART1 is used for connection to FTDI, UART2 for USB and UART3 for BT
    RPOR10bits.RP21R = U1TX;                                        // Assign UART1 TX to RP21  (FDTI)
    RPOR6bits.RP12R  = U2TX;                                        // Assign UART2 TX to RP12  (2114)
    RPOR7bits.RP15R  = U3TX;                                        // Assign UART3 TX to RP15  (BT)

// Lock Registers
    __builtin_write_OSCCONL(OSCCON | 0x0040);

// Initialize peripherals
    SPI1STATbits.SPIEN = 0;
    SPI2STATbits.SPIEN = 0;

    SPI1STATbits.SPISIDL = 1;                                       // stop in Idle mode

    SPI1CON1bits.DISSCK = 0;                                        // enable internal SPI clk
    SPI1CON1bits.DISSDO = 0;                                        // SDO pin controlled by the module
    SPI1CON1bits.MODE16 = 0;                                        // xfers are 8-bits wide
    SPI1CON1bits.SMP    = 1;                                        // data sampled at end of data output time
    SPI1CON1bits.CKE = 0;                                           // CPHA ?
    SPI1CON1bits.SSEN = 1;                                          // Use SS pin for Slave Mode (assume this means module drives nCS in Master Mode)
    SPI1CON1bits.CKP = 0;                                           // CPOL ?
    SPI1CON1bits.MSTEN = 1;                                         // Master Mode
    SPI1CON1bits.PPRE = 1;                                          // Primary   Prescale = 16:1
    SPI1CON1bits.SPRE = 6;                                          // Secondary Prescale =  2:1  (gives SCK of 500KHz of 32Mhz Master Clock)

    SPI1CON2bits.FRMEN = 0;                                         // Framed support disabled
    SPI1CON2bits.SPIFSD = 0;                                        // Frame sync pulse output (master)
    SPI1CON2bits.SPIFPOL = 1;                                       // Frame sync pulse is active high
    SPI1CON2bits.SPIFE = 1;                                         // Frame sync pulse coincides with first bit clock
    SPI1CON2bits.SPIBEN = 0;                                        // Enhanced buffer disabled

// Configure SPI2 (SPI Flash EEPROM) interface
    SPI2CON1bits.DISSCK = 0;                                        // enable internal SPI clk
    SPI2CON1bits.DISSDO = 0;                                        // SDO pin controlled by the module
    SPI2CON1bits.MODE16 = 0;                                        // xfers are 8-bits wide
    SPI2CON1bits.SMP = 0;                                           // data sampled in middle of data output time
    SPI2CON1bits.CKE = 1;                                           // CPHA ?
    SPI2CON1bits.SSEN = 1;                                          // Use SS pin for Slave Mode (assume this means module drives nCS in Master Mode)
    SPI2CON1bits.CKP = 0;                                           // CPOL ?
    SPI2CON1bits.MSTEN = 1;                                         // Master Mode
    SPI2CON1bits.PPRE = 3;                                          // Prescale =
    SPI2CON1bits.SPRE = 6;

    SPI2CON2bits.FRMEN = 0;                                         // Framed support disabled
    SPI2CON2bits.SPIFSD = 0;                                        // Frame sync pulse output (master)
    SPI2CON2bits.SPIFPOL = 1;                                       // Frame sync pulse is active high
    SPI2CON2bits.SPIFE = 1;                                         // Frame sync pulse coincides with first bit clock
    SPI2CON2bits.SPIBEN = 0;                                        // Enhanced buffer disabled
     
// Initialize SPI Port 1 for 8280
    SPI1STATbits.SPIEN = 0;
    SPI1STAT = 0;
    SPI1CON1 = 0;
    SPI1CON2 = 0;
    SPI1CON1bits.MSTEN = 1;
    SPI1CON2 = 0;
    SPI1CON1bits.MODE16 = 0;                                        // 0 for 8 bit
    SPI1CON1bits.CKE = 1;                                           // 0= Serial output data changes on transition from Idle clock state to active clock state (see bit 6)
    SPI1CON1bits.CKP = 0;
    SPI1CON1bits.SMP = 0;                                           // 1= Input data sampled at end of data output time
    SPI1CON1bits.PPRE = 3;                                          // Prescale =
    SPI1CON1bits.SPRE = 6;                                          // Secondary Prescale =  2:1  (gives SCK of 500KHz of 32Mhz Master Clock)
    IEC0bits.SPI1IE = 0;
    IFS0bits.SPI1IF = 0;
    SPI1STATbits.SPIEN = 1;
    SPI2STATbits.SPIEN = 1;

    CNPU5bits.CN72PUE=1;
    LATFbits.LATF3 = 1;

// Initialize SPI Port 2 for eeprom
    SPI2STATbits.SPIEN = 0;
    SPI2STAT = 0;
    SPI2CON1 = 0;
    SPI2CON1bits.MSTEN = 1;
    SPI2CON2 = 0;
    SPI2CON1bits.MODE16 = 0;
    SPI2CON1bits.PPRE = 3;                                          // Primary   Prescale =
    SPI2CON1bits.SPRE = 6;                                          // Secondary Prescale =  2:1  (gives SCK of 500KHz of 32Mhz Master Clock)
    SPI2CON1bits.CKE = 1;
    SPI2CON1bits.CKP = 0;
    SPI2CON1bits.SMP = 0;
    IEC2bits.SPI2IE = 0;
    IFS2bits.SPI2IF = 0;
    SPI2STATbits.SPIEN = 1;

    LATBbits.LATB8 = 1;

// Initialize Uart 1 for PC communications 115200,No Parity, 8 bits, 1 stop bit
    U1BRG = BAUDRATEREG_BRGH_1;
    U1MODE = 0;
    U1MODEbits.BRGH = 1;
    U1STA = 0;
    U1STAbits.UTXISEL0=1;
    U1STAbits.URXISEL0=0;
    U1MODEbits.UARTEN = 1;
    U1STAbits.UTXEN = 1;
    IFS0bits.U1RXIF = 0;
    IEC0bits.U1RXIE = 1;
    IFS0bits.U1TXIF = 0;
    IPC2bits.U1RXIP = 1;

// Initialize Uart 2 for  USB interface 115200,No Parity, 8 bits, 1 stop bit
    U2BRG = BAUDRATEREG_BRGH_1;
    U2MODE = 0;
    U2MODEbits.BRGH = 1;
    U2STA = 0;
    U2STAbits.UTXISEL0 = 1;
    U2STAbits.URXISEL0 = 0;
    U2MODEbits.UARTEN = 1;
    U2STAbits.UTXEN = 1;
    IFS1bits.U2RXIF = 0;
    IEC1bits.U2RXIE = 1;
    IFS1bits.U2TXIF = 0;
    IPC7bits.U2RXIP = 1;

// Initialize Uart 3 for Bluetooth module 115200,No Parity, 8 bits, 1 stop bit
    U3BRG = BAUDRATEREG_BRGH_1;
    U3MODE = 0;
    U3MODEbits.BRGH = 1;
    U3STA = 0;
    U3STAbits.UTXISEL0 = 1;
    U3STAbits.URXISEL0 = 0;
    U3MODEbits.UARTEN = 1;
    U3STAbits.UTXEN = 1;
    IFS5bits.U3RXIF = 0;
    IFS5bits.U3TXIF = 0;
    IEC5bits.U3RXIE = 1;

// Initialise Timer4 for volume update delay
    T4CON = 0x0038;                                                 // Set 256 prescaler value
    TMR4 = 0x00;
    PR4 = 0xd555;                                                   // Set MSB of 32-bit counter to make MSB count in seconds
    PR5 = NV_DELAY;                                                 // Set NV write delay in seconds
    IPC7bits.T5IP = 0x01;
    IFS1bits.T5IF = 0;

    mPORTBOutputConfig(RESET_8280);
  
    mPORTCInputConfig(IRQ_8280 | 0x02);                             // FTDI_RX

    mPORTDInputConfig(BT_EVENT | HEADIN);

    mPORTDOutputConfig(0x0803);                                     // HID_TX, DACMUTE, AMPMUTE

    mPORTEOutputConfig(0x3ff);                                      // Configure PORT E as outputs for LEDs on 0-4 and USB control on 5-9

    mPORTFInputConfig(0xC4);                                        // BT_RTS, CIF1MISO, BT_RX

    mPORTFOutputConfig(0x13b);                                      // BT_CMD, BT_CTS, CIF1SS, CIF1SCLK, CIF1MOSI, BT_TX

    mPORTGInputConfig(SW_PWR_PAIR |                                 // Configure PORT G lines as button inputs (0,1,2,3,7,8)
                      SW_VOLUP    |
                      SW_VOLDN    |
                      SW_MODE     |
                      SW_PROCESS  |
                      SW_CALLPU);

    mPORTGOutputConfig(0x40);                                       // FDTI_TX
	
    DisableIntInputChange;

    InputChange_Clear_Intr_Status_Bit;
    ConfigIntCN(INT_ENABLE|INT_PRI_4);

// Set port directions
    TRISA = 0x0;                                                    // All outputs
    TRISB = 0xC0;                                                   // 6, 7
    TRISC = 0x100A;                                                 // 1, 3, 12
    TRISD = 0xF000;                                                 // 12, 13, 14, 15
    TRISE = 0x0;                                                    // All outputs
    TRISF = 0xC4;                                                   // 2, 6, 7
    TRISG = 0x38F;                                                  // 0, 1, 2, 3, 7, 8, 9

    AD1PCFGL = 0xffff;
    
    EnableIntInputChange;
}

void InitInt(void)
{
    DisableIntInputChange;

    InputChange_Clear_Intr_Status_Bit;
    ConfigIntCN(INT_ENABLE|INT_PRI_4);

    EnableCN77;                                                     // 0 - SW_PWR_PAIR
    EnableCN78;                                                     // 1 - SW_VOLUP
    EnableCN83;                                                     // 2 - SW_VOLDN
    EnableCN84;                                                     // 3 - SW_MODE
    EnableCN9;                                                      // 7 - SW_PROCESS
    EnableCN10;                                                     // 8 - SW_CALLPU

    EnableCN47;                                                     // 3 - IRQ8280
    EnableCN20;                                                     // 14 - BT_EVENT
    EnableCN19;                                                     // 12 - HEADIN
	
    CNEN2bits.CN20IE = 1;                                           // Enable BT_EVENT interrupt
    CNEN2bits.CN19IE = 1;                                           // Enable HEADIN interrupt
    CNEN3bits.CN47IE = 1;                                           // Enable IRQ_8280 interrupt
    IEC1bits.T5IE = 1;                                              // Enable timer5 Interrupt
	BT_RX_INT_ON;

    EnableIntInputChange;
}
//*********************** END OF FILE ****************************************