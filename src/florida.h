/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Definitions for WM8280 command functions
 *
 *  May 2014 - P Gomme  First Release
 *  Oct 2014 - SLL      Update with DSP Control Register and Filter Locations
 *  Sep 2015 - SLL      Volume updates
 *
 ******************************************************************************/

#ifndef FLORIDA_H
#define	FLORIDA_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* FLORIDA_H */

#define MISC_PAD_CTRL_1         0x0c20
#define HP1L_VOLUME             0x0681
#define HP1R_VOLUME             0x0689
#define HP2L_VOLUME             0x0691
#define HP2R_VOLUME             0x0699
#define HP3L_VOLUME             0x06A1
#define HP3R_VOLUME             0x06A9
#define SPKL_VOLUME 			0x06B1
#define SPKR_VOLUME 			0x06B9
#define AIF1_TX1_1_VOLUME 		0x0701
#define AIF1_TX2_1_VOLUME 		0x0709
#define AIF2_TX1_1_VOLUME 		0x0741
#define AIF2_TX2_1_VOLUME 		0x0749

#define MAX_VOL                 0x90                                // Set as +8dB to allow sub volume to be +6dB higher and prevent distortion
#define MIN_VOL                 0x40
#define SUB_OFFSET_DEFAULT      0x00                                // 0dB

#define DSP1_CONTROL_1          0x1100
#define DSP2_CONTROL_1          0x1200
#define DSP3_CONTROL_1          0x1300
#define DSP4_CONTROL_1          0x1400

#define DSP_CONTROL_START       0x0017
#define DSP_CONTROL_START_MASK  0xff00                              // Added to allow for DSP using sample rate 3 (16kHz)

void Write_Byte_SPI1(unsigned char Value);
void Florida_Write_Address(DWORD address);
void Florida_Write_Finish(void);
void Florida_Reset(void);
void Florida_Write_Reg(DWORD address, WORD data);
WORD Florida_Read(DWORD Address);

 /************************ END OF FILE ****************************************/