/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  MyBeam System Main Program
 *
 *  May 2014 - P Gomme   First Release
 *
 ******************************************************************************/

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>          /* For uint32_t definition */
#include <stdbool.h>         /* For true/false definition */

#include "system.h"          /* variables/params used by system.c */

/******************************************************************************/
/* System Level Functions                                                     */
/*                                                                            */
/* Custom oscillator configuration funtions, reset source evaluation          */
/* functions, and other non-peripheral microcontroller initialization         */
/* functions get placed in system.c                                           */
/*                                                                            */
/******************************************************************************/

/* Refer to the device Family Reference Manual Oscillator section for
information about available oscillator configurations.  Typically
this would involve configuring the oscillator tuning register or clock
switching useing the compiler's __builtin_write_OSCCON functions.
Refer to the C Compiler for PIC24 MCUs and dsPIC DSCs User Guide in the
compiler installation directory /doc folder for documentation on the
__builtin functions. */


void ConfigureOscillator(void)
{

// Disable Watch Dog Timer
    RCONbits.SWDTEN = 0;

// When clock switch occurs switch to Prim Osc (HS, XT, EC)with PLL
    __builtin_write_OSCCONH(0x03);                                  // Set OSCCONH for clock switch
    __builtin_write_OSCCONL(0x01);                                  // Start clock switching
    while(OSCCONbits.COSC != 0b011);

// Wait for Clock switch to occur
// Wait for PLL to lock, if PLL is used
    while(OSCCONbits.LOCK != 1);


        
}

//************************ END OF FILE ****************************************