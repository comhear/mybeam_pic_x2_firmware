/*----------------------------------------------------------------------------/
/  FatFs - FAT file system module  R0.09                  (C)ChaN, 2011
/-----------------------------------------------------------------------------/
/ FatFs module is a generic FAT file system module for small embedded systems.
/ This is a free software that opened for education, research and commercial
/ developments under license policy of following terms.
/
/  Copyright (C) 2011, ChaN, all right reserved.
/
/ * The FatFs module is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   personal, non-profit or commercial products UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/----------------------------------------------------------------------------*/


#include "global.h"

#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "ff.h"                                                     // FatFs configurations and declarations
#include "diskio.h"                                                 // Declarations of low level disk I/O functions
#include "integer.h"
#include "xmodem.h"                                                 // Provide definition of NUL


extern void print_fresult (int rc);
extern void pause(void);

void dump_win (void);
void dump_buffer(void);
extern void dump_sector(WORD sector);
void print_hex(BYTE);
void DelayMs(WORD delay);

extern BYTE win[256];
extern BYTE buff[256];
extern char strbuffer[];
extern DIR dj;

BYTE sfn[28];

//--------------------------------------------------------------------------/
//   Module Private Definitions                                             /
//--------------------------------------------------------------------------/

#if _FATFS != 6502                                                  // Revision ID

#endif

// Definitions on sector size
// Reentrancy related
#if _FS_REENTRANT
	#if _USE_LFN == 1
		#error Static LFN work area must not be used in re-entrant configuration.
	#endif
#define	ENTER_FF(fs)		{ if (!lock_fs(fs)) return FR_TIMEOUT; }
#define	LEAVE_FF(fs, res)	{ unlock_fs(fs, res); return res; }
#else
#define	ENTER_FF(fs)
#define LEAVE_FF(fs, res)	return res
#endif

#define	ABORT(fs, res)		{ fp->flag |= FA__ERROR; LEAVE_FF(fs, res); }

// File shareing feature
#if _FS_SHARE
#if _FS_READONLY
#error _FS_SHARE must be 0 on read-only cfg.
#endif
typedef struct {
	FATFS *fs;                                                  // File ID 1, volume (NULL:blank entry)
	DWORD clu;                                                  // File ID 2, directory
	WORD idx;                                                   // File ID 3, directory index
	WORD ctr;                                                   // File open counter, 0:none, 0x01..0xFF:read open count, 0x100:write mode
} FILESEM;
#endif

// Misc definitions
#define LD_CLUST(dir)	(WORD)((WORD)*(BYTE*)(dir+DIR_FstClusLO))
#define ST_CLUST(dir,cl) {ST_WORD(dir+DIR_FstClusLO, cl); ST_WORD(dir+DIR_FstClusHI, (DWORD)cl>>16);}

// Character code support macros
#define IsUpper(c)	(((c)>='A')&&((c)<='Z'))
#define IsLower(c)	(((c)>='a')&&((c)<='z'))
#define IsDigit(c)	(((c)>='0')&&((c)<='9'))

#if _DF1S                                                           // Code page is DBCS

#ifdef _DF2S                                                        // Two 1st byte areas
#define IsDBCS1(c)	(((BYTE)(c) >= _DF1S && (BYTE)(c) <= _DF1E) || ((BYTE)(c) >= _DF2S && (BYTE)(c) <= _DF2E))
#else                                                               // One 1st byte area
#define IsDBCS1(c)	((BYTE)(c) >= _DF1S && (BYTE)(c) <= _DF1E)
#endif

#ifdef _DS3S                                                        // Three 2nd byte areas
#define IsDBCS2(c)	(((BYTE)(c) >= _DS1S && (BYTE)(c) <= _DS1E) || ((BYTE)(c) >= _DS2S && (BYTE)(c) <= _DS2E) || ((BYTE)(c) >= _DS3S && (BYTE)(c) <= _DS3E))
#else                                                               // Two 2nd byte areas
#define IsDBCS2(c)	(((BYTE)(c) >= _DS1S && (BYTE)(c) <= _DS1E) || ((BYTE)(c) >= _DS2S && (BYTE)(c) <= _DS2E))
#endif

#else                                                               // Code page is SBCS

#define IsDBCS1(c)	0
#define IsDBCS2(c)	0

#endif


// Name status flags
#define NS_LOSS		0x01                                        // Out of 8.3 format
#define NS_LFN		0x02                                        // Force to create LFN entry
#define NS_LAST		0x04                                        // Last segment
#define NS_BODY		0x08                                        // Lower case flag (body)
#define NS_EXT		0x10                                        // Lower case flag (ext)
#define NS_DOT		0x20                                        // Dot entry

// FAT sub-type boundaries */
// Note that the FAT spec by Microsoft says 4085 but Windows works with 4087!
#define MIN_FAT16	4086                                        // Minimum number of clusters for FAT16
#define	MIN_FAT32	65526                                       // Minimum number of clusters for FAT32

// FatFs refers the members in the FAT structures as byte array instead of
// structure member because the structure is not binary compatible between
// different platforms
#define BS_jmpBoot		0                                   // Jump instruction (3)
#define BS_OEMName		3	                            // OEM name (8)
#define BPB_BytsPerSec		11	                            // Sector size [byte] (2)
#define BPB_SecPerClus		13	                            // Cluster size [sector] (1)
#define BPB_RsvdSecCnt		14	                            // Size of reserved area [sector] (2)
#define BPB_NumFATs		16	                            // Number of FAT copies (1)
#define BPB_RootEntCnt		17	                            // Number of root dir entries for FAT12/16 (2)
#define BPB_TotSec16		19	                            // Volume size [sector] (2)
#define BPB_Media		21	                            // Media descriptor (1)
#define BPB_FATSz16		22	                            // FAT size [sector] (2)
#define BPB_SecPerTrk		24	                            // Track size [sector] (2)
#define BPB_NumHeads		26	                            // Number of heads (2)
#define BPB_HiddSec		28	                            // Number of special hidden sectors (4)
#define BPB_TotSec32		32	                            // Volume size [sector] (4)
#define BS_DrvNum		36	                            // Physical drive number (2)
#define BS_BootSig		38	                            // Extended boot signature (1)
#define BS_VolID		39	                            // Volume serial number (4)
#define BS_VolLab		43	                            // Volume label (8)
#define BS_FilSysType		54	                            // File system type (1)
#define BPB_FATSz32		36	                            // FAT size [sector] (4)
#define BPB_ExtFlags		40	                            // Extended flags (2)
#define BPB_FSVer		42	                            // File system version (2)
#define BPB_RootClus		44	                            // Root dir first cluster (4)
#define BPB_FSInfo		48	                            // Offset of FSInfo sector (2)
#define BPB_BkBootSec		50	                            // Offset of backup boot sectot (2)
#define BS_DrvNum32		64	                            // Physical drive number (2)
#define BS_BootSig32		66	                            // Extended boot signature (1)
#define BS_VolID32		67	                            // Volume serial number (4)
#define BS_VolLab32		71	                            // Volume label (8)
#define BS_FilSysType32		82	                            // File system type (1)
#define	FSI_LeadSig		0	                            // FSI: Leading signature (4)
#define	FSI_StrucSig		484	                            // FSI: Structure signature (4)
#define	FSI_Free_Count		488	                            // FSI: Number of free clusters (4)
#define	FSI_Nxt_Free		492	                            // FSI: Last allocated cluster (4)
#define MBR_Table		446	                            // MBR: Partition table offset (2)
#define	SZ_PTE			16	                            // MBR: Size of a partition table entry
#define BS_55AA			510	                            // Boot sector signature
#define	DIR_Name		0	                            // Short file name (11)
#define	DIR_FstClus		28	                            // First cluster
#define	DIR_FileSize		29	                            // File size (3)
#define	SZ_DIR			32	                            // Size of a directory entry
#define	LLE			0x40	                            // Last long entry flag in LDIR_Ord
#define	DDE			0xE5	                            // Deleted directory enrty mark in DIR_Name[0]
#define	NDDE			0x05                                // Replacement of a character collides with DDE


//------------------------------------------------------------/
// Module private work area                                   /
//------------------------------------------------------------/
// Note that uninitialized variables with static duration are
// zeroed/nulled at start-up. If not, the compiler or start-up
// routine is out of ANSI-C standard.

#if _VOLUMES
static
FATFS *FatFs[_VOLUMES];                                             // Pointer to the file system objects (logical drives)
#else
#error Number of volumes must not be 0.
#endif

#if _FS_RPATH
static BYTE CurrVol;                                                // Current drive
#endif

#if _FS_SHARE
static FILESEM	Files[_FS_SHARE];                                   // File lock semaphores
#endif

#define	DEF_NAMEBUF		BYTE sfn[28]
#define INIT_BUF(dobj)		(dobj).fn = sfn
#define	FREE_BUF()

static FRESULT chk_mounted (                                        // FR_OK(0): successful, !=0: any error occurred
	const TCHAR **path,                                         // Pointer to pointer to the path name (drive number)
	FATFS **rfs,                                                // Pointer to pointer to the found file system object
	BYTE chk_wp                                                 // !=0: Check media write protection for write access
);

static FRESULT validate (                                           // FR_OK(0): The object is valid, !=0: Invalid
	FATFS *fs                                                   // Pointer to the file system object
);

void mem_set (void* dst, int val, UINT cnt);

static void mem_cpy (BYTE* dst, const BYTE* src, UINT cnt);

static int mem_cmp (const void* dst, const void* src, UINT cnt);

static FRESULT move_window (
	FATFS *fs,                                                  // File system object
	WORD sector                                                 // Sector number to make appearance in the fs->win[]
);                                                                  // Move to zero only writes back dirty window

static FRESULT remove_chain (
	FATFS *fs,                                                  // File system object
	DWORD clst                                                  // Cluster# to remove a chain from
);

static FRESULT dir_register (                                       // FR_OK:Successful, FR_DENIED:No free entry or too many SFN collision, FR_DISK_ERR:Disk error
	DIR *dj                                                     // Target directory with object name to be created
);

static FRESULT follow_path (                                        // FR_OK(0): successful, !=0: error code
	DIR *dj,                                                    // Directory object to return last directory and found object
	const TCHAR *path                                           // Full-path string to find a file or directory
);

DWORD clust2sect (                                                  // !=0: Sector number, 0: Failed - invalid cluster#
	FATFS *fs,                                                  // File system object
	DWORD clst                                                  // Cluster# to be converted
);
static DWORD create_chain (                                         // 0:No free cluster, 1:Internal error, 0xFFFFFFFF:Disk error, >=2:New cluster#
	FATFS *fs,                                                  // File system object
	DWORD clst                                                  // Cluster# to stretch. 0 means create a new chain.
);

FRESULT f_sync (
	FIL *fp                                                     // Pointer to the file object
);

static FRESULT dir_next (                                           // FR_OK:Succeeded, FR_NO_FILE:End of table, FR_DENIED:EOT and could not stretch
	DIR *dj,                                                    // Pointer to directory object
	int stretch                                                 // 0: Do not stretch table, 1: Stretch table if needed
);

static FRESULT dir_sdi (
	DIR *dj,                                                    // Pointer to directory object
	WORD idx                                                    // Directory index number
);

FRESULT put_fat (
	FATFS *fs,                                                  // File system object
	DWORD clst,                                                 // Cluster# to be changed in range of 2 to fs->n_fatent - 1
	DWORD val                                                   // New value to mark the cluster
);

DWORD get_fat (                                                     // 0xFFFFFFFF:Disk error, 1:Internal error, Else:Cluster status
	FATFS *fs,                                                  // File system object
	DWORD clst                                                  // Cluster# to get the link information
);

static FRESULT dir_find (
	DIR *dj                                                     // Pointer to the directory object linked to the file name
);

static FRESULT create_name (
	DIR *dj,                                                    // Pointer to the directory object
	const TCHAR **path                                          // Pointer to pointer to the segment in the path string
);

static int chk_chr (const char* str, int chr);

void dump_dir_struct(DIR *thedir);

void dump_filinfo_struct(FILINFO *thefilinfo);

static FRESULT sync (                                               // FR_OK: successful, FR_DISK_ERR: failed
	FATFS *fs                                                   // File system object
);

static FRESULT dir_read (
	DIR *dj                                                     // Pointer to the directory object that pointing the entry to be read
);

FRESULT f_stat (
	const TCHAR *path,                                          // Pointer to the file path
	FILINFO *fno                                                // Pointer to file information to return
);
static void get_fileinfo (                                          // No return code
	DIR *dj,                                                    // Pointer to the directory object
	FILINFO *fno                                                // Pointer to the file information to be filled
);

static FRESULT dir_remove (                                         // FR_OK: Successful, FR_DISK_ERR: A disk error
	DIR *dj                                                     // Directory object pointing the entry to be removed
);

//-----------------------------------------------------------------------/
// Mount/Unmount a Logical Drive                                         /
//-----------------------------------------------------------------------/

FRESULT f_mount (
	BYTE vol,                                                   // Logical drive number to be mounted/unmounted
	FATFS *fs                                                   // Pointer to new file system object (NULL for unmount)
)
{
#if TRACE
	printf("f_mounted()\n\r");
#endif

	FATFS *rfs;

	if (vol >= _VOLUMES)                                        // Check if the drive number is valid
		return FR_INVALID_DRIVE;
	rfs = FatFs[vol];                                           // Get current fs object

	if (rfs)
        {
        	rfs->fs_type = 0;                                   // Clear old fs object
	}

	if (fs)
        {
		fs->fs_type = 0;                                    // Clear new fs object
	}
	FatFs[vol] = fs;                                            // Register new fs object

	return FR_OK;
}

//-----------------------------------------------------------------------/
// Create File System on the Drive                                       /
//-----------------------------------------------------------------------/

FRESULT f_mkfs (
	BYTE drv                                                    // Logical drive number
)
{
	BYTE *tbl;
	WORD wsect;
	UINT i;
	FATFS *fs;
	DSTATUS stat;


#if TRACE
    printf("f_mkfs()\n\r");
#endif
	tbl = win;
	fs = FatFs[drv];
	if (!fs) 
		return FR_NOT_ENABLED;

// Get disk statics
	stat = disk_initialize(0);
	if (stat & STA_NOINIT) 
		return FR_NOT_READY;

// Initialize FAT area
	mem_set(tbl, 0, 256);                                       // clear the buffer
	tbl[0] = 0xff;                                              // mark first two sectors as reserved
	tbl[1] = 0xff;
	wsect = FATBASE;
	for (i = 0; i < N_FATS; i++)                                // Initialize each FAT copy
	{			
		wsect = (i * CSIZE) + wsect; 
		if (disk_write(tbl, wsect) != RES_OK)
		{
#if TRACE
			printf("f_mkfs60()\n\r");
#endif
			return FR_DISK_ERR;
		}
	}

//  Initialize each directory
	wsect = DIRBASE;

	tbl[0] = 0x00;                                              // overwrite the 0xff from above in the FAT initialise
	tbl[1] = 0x00;                                              // ditto

// Initialise table
	for(i = 0; i < 8;i++)                                       // 8 directory entries per sector
		tbl[(BYTE)(i * SZ_DIR)] = DDE;                      // mark first byte of each directory entry with entry is free tag

// Write table to disk
	for(i = 0; i < N_DIR; i++)
	{
		wsect = DIRBASE + (i * CSIZE);

		if(i == (N_DIR - 1))                                // last directory sector to write
		{
                    tbl[224] = 0;                                   // mark as end of directory entries
                    tbl[240] = 'E';                                 // mark as end of directory entries
		}

		if (disk_write(tbl, wsect) != RES_OK)
		{
                    printf("f_mkfs90()\n\r");
                    return FR_DISK_ERR;
		}

#if TRACE
		printf("f_mkfs()52 read sector to check\n\r");
		dump_sector(wsect);
#endif

	}

	mem_set(tbl, 0, 256);                                       // clear the buffer

	tbl[0] = 'C';
	tbl[1] = 'O';
	tbl[2] = 'M';
	tbl[3] = 'H';
	tbl[4] = 'E';
	tbl[5] = 'A';
	tbl[6] = 'R';
	tbl[7] = NUL;

	wsect = 0;                                                  // write COMHEAR to sector 0
        printf("\n\rWriting COMHEAR marker\r\n");

	if (disk_write(tbl, wsect) != RES_OK)
	{
#if TRACE
		printf("f_mkfs99()\n\r");
#endif
                printf("\n\rERROR!\r\n");
		return FR_DISK_ERR;
	}
#if TRACE
	printf("f_mkfs()57 read sector to check\n\r");

	dump_sector(wsect);

#endif
        printf("\n\rCompleted filesystem setup\r\n");
	return (disk_ioctl(0, CTRL_SYNC, 0) == RES_OK) ? FR_OK : FR_DISK_ERR;
}

//-----------------------------------------------------------------------/
// Open or Create a File                                                 /
//-----------------------------------------------------------------------/

FRESULT f_open (
	FIL *fp,                                                    // Pointer to the blank file object
	const TCHAR *path,                                          // Pointer to the file name
	BYTE mode                                                   // Access mode and file open mode flags
)
{
	FRESULT res;
	DIR dj;
	BYTE *dir;

#if TRACE
	printf("f_open() path = %s\n\r",path);
	pause();
#endif

	fp->fs = 0;                                                 // Clear file object

	mode &= FA_READ | FA_WRITE | FA_CREATE_ALWAYS | FA_OPEN_ALWAYS | FA_CREATE_NEW;
	res = chk_mounted(&path, &dj.fs, (BYTE)(mode & ~FA_READ));
	dj.fs = FatFs[0];

#if TRACE1
	printf("f_open() dj.fs->winsect = %d\n\r",dj.fs->winsect);
#endif

	dj.fn = sfn;  

	if (res == FR_OK)
	{
#if TRACE1
		printf("\n\rf_open()1\n\r");
#endif
		res = follow_path(&dj, path);                       // Follow the file path
#if TRACE1		
		printf("f_open()44\n\r"); 
		print_fresult(res);
#endif
	}

	dir = dj.dir;
	if (res == FR_OK) 
	{
		if (!dir)                                           // Current dir itself
		{
#if TRACE1
			printf("f_open()45\n\r");
			print_fresult(res);
#endif
			res = FR_INVALID_NAME;
		}
	}
// Create or Open a file
	if (mode & (FA_CREATE_ALWAYS | FA_OPEN_ALWAYS | FA_CREATE_NEW)) 
	{
		DWORD dw, cl;
#if TRACE1
		printf("f_open()11\n\r");
#endif
		if (res != FR_OK) 
		{
#if TRACE1
			printf("f_open()12\n\r");                   // No file, create new
#endif

			if (res == FR_NO_FILE)
                        {
				res = dir_register(&dj);            // There is no file to open, create a new entry
                        }
#if TRACE1
			printf("f_open()13\n\r");
			print_fresult(res);
#endif
			mode |= FA_CREATE_ALWAYS;                   // File is created
			dir = dj.dir;                               // New entry
		}
		else 
		{                                                   // Any object is already existing
			if (mode & FA_CREATE_NEW)                   // Cannot create as new file/
				res = FR_EXIST;
		}
		if (res == FR_OK && (mode & FA_CREATE_ALWAYS))      // Truncate it if overwrite mode
		{
#if TRACE1
			printf("f_open()21\n\r");
#endif
			*(BYTE*)(dir+DIR_FileSize) = (BYTE)(0);     // size = 0
			*((BYTE*)(dir+DIR_FileSize)+1)=(BYTE)((WORD)(0)>>8);
			*((BYTE*)(dir+DIR_FileSize)+2)=(BYTE)((DWORD)(0)>>16);

			cl = *(BYTE*)(dir+DIR_FstClus);

			*(BYTE*)(dir+DIR_FstClus) = 0;
#if TRACE1
			printf("f_open()22, dj.fs->wflag = %d\n\r",dj.fs->wflag);
			pause();
#endif
			dj.fs->wflag = 1;
#if TRACE1
			printf("f_open()23, dj.fs->wflag = %d\n\r",dj.fs->wflag);
			pause();
#endif
			if (cl) 
			{                                           // Remove the cluster chain if exist
#if TRACE1
				printf("f_open()91\n\r");
#endif
				dw = dj.fs->winsect;
				res = remove_chain(dj.fs, cl);
				if (res == FR_OK) 
				{
					dj.fs->last_clust = (BYTE)(cl - 1); // Reuse the cluster hole
					res = move_window(dj.fs, dw);
				}
			}
		}
	}
	else 
	{                                                           // Open an existing file

	}
	if (res == FR_OK) 
	{
		if (mode & FA_CREATE_ALWAYS)                        // Set file change flag if created or overwritten
			mode |= FA__WRITTEN;
		fp->dir_sect = dj.fs->winsect;                      // Pointer to the directory entry
		fp->dir_ptr = dir;
#if TRACE1
		printf("f_open()66, fp->dir_sect = %u, fp->dir_ptr = %s\n\r",fp->dir_sect, fp->dir_ptr);
		pause();
#endif
		fp->flag = mode;                                    // File access mode
		fp->sclust = *(BYTE*)(dir+DIR_FstClus);             // File start cluster
		fp->fsize = (DWORD)(((DWORD)*((BYTE*)(dir+DIR_FileSize)+2)<<16)   |   ((WORD)*((BYTE*)(dir+DIR_FileSize)+1)<<8)   |    *(BYTE*)(dir+DIR_FileSize));
		fp->fptr = 0;                                       // File pointer
		fp->dsect = 0;
		fp->fs = dj.fs; 
#if TRACE1
		printf("f_open()67, fp->sclust = %d, fp->fsize = %lu\n\r",fp->sclust, fp->fsize);
		printf("f_open(),68 dj.fs->winsect = %d, dj.index = %d\n\r",dj.fs->winsect, dj.index);
		pause();
#endif
	}
#if TRACE
	printf("f_open()88\n\r");
	pause();
#endif
	return res;
}

/*-----------------------------------------------------------------------*/
/* Read File                                                             */
/*-----------------------------------------------------------------------*/

FRESULT f_read (
	FIL *fp, 		/* Pointer to the file object */
	void *buf,		/* Pointer to data buffer */
	UINT btr,		/* Number of bytes to read */
	UINT *br		/* Pointer to number of bytes read */
)
{
	FRESULT res;
	DWORD clst, sect, remain;
	UINT rcnt, cc;
	BYTE csect, *rbuff = buf;
	WORD copy_index;

	*br = 0;	/* Initialize byte counter */

	res = validate(fp->fs);				/* Check validity */

	if (res != FR_OK)
	{
#if TRACE_FLOW
		printf("f_read()1\n\r");
#endif
 
		LEAVE_FF(fp->fs, res);
	}
	if (fp->flag & FA__ERROR)					/* Aborted file? */
	{
#if TRACE_FLOW
		printf("f_read()2\n\r");
#endif
		LEAVE_FF(fp->fs, FR_INT_ERR);
	}
	if (!(fp->flag & FA_READ)) 					/* Check access mode */
	{
#if TRACE_FLOW
		printf("f_read()3\n\r");
#endif
		LEAVE_FF(fp->fs, FR_DENIED);
	}

	remain = fp->fsize - fp->fptr;

#if TRACE_FLOW
		printf("f_read()22, fp->fsize = %lu, fp->fptr = %lu, remain = %lu\n\r", fp->fsize, fp->fptr,remain);
#endif

	if (btr > remain)
	{ 
#if TRACE_FLOW
		printf("f_read()4\n\r");
#endif	
		btr = (UINT)remain;		/* Truncate btr by remaining bytes */

#if TRACE_FLOW
			printf("f_read()21, btr = %d\n\r",btr);
#endif 

	}
	for ( ;  btr;								/* Repeat until all data read */
		rbuff += rcnt, fp->fptr += rcnt, *br += rcnt, btr -= rcnt) 
	{
#if TRACE_FLOW
			printf("f_read()20\n\r");
#endif 
		if ((fp->fptr % SSIZE) == 0) 
		{		/* On the sector boundary? */
#if TRACE_FLOW
			printf("f_read()5\n\r");
#endif	
			csect = (BYTE)(fp->fptr / SSIZE & (CSIZE - 1));	/* Sector offset in the cluster */
			if (!csect) 
			{						/* On the cluster boundary? */
#if TRACE_FLOW
				printf("f_read()6\n\r");
#endif	
				if (fp->fptr == 0) 
				{			/* On the top of the file? */
#if TRACE_FLOW
					printf("f_read()7\n\r");
#endif
					clst = fp->sclust;			/* Follow from the origin */
				} 
				else 
				{						/* Middle or end of the file */
#if TRACE_FLOW
				printf("f_read()8\n\r");
#endif
						clst = get_fat(fp->fs, fp->clust);	/* Follow cluster chain on the FAT */
				}
				if (clst < 2)
				{
 #if TRACE_FLOW
				printf("f_read()9\n\r");
#endif
					ABORT(fp->fs, FR_INT_ERR);
				}
				if (clst == 0xFFFFFFFF)
				{
 #if TRACE_FLOW
				printf("f_read()10\n\r");
#endif
					ABORT(fp->fs, FR_DISK_ERR);				
				}
				fp->clust = clst;				/* Update current cluster */
			}
			sect = clust2sect(fp->fs, fp->clust);	/* Get current sector */
			if (!sect)
			{
#if TRACE_FLOW
				printf("f_read()11\n\r");
#endif
				ABORT(fp->fs, FR_INT_ERR);
			}
			sect += csect;
			cc = btr / SSIZE;				/* When remaining bytes >= sector size, */
			if (cc) 
			{
#if TRACE_FLOW
				printf("f_read()12\n\r");
#endif
							/* Read maximum contiguous sectors directly */
				if (csect + cc > CSIZE)	/* Clip at cluster boundary */
				{
#if TRACE_FLOW
					printf("f_read()13\n\r");
#endif
					cc = CSIZE - csect;
				}
				if (disk_read(rbuff, sect) != RES_OK)
				{	
#if TRACE_FLOW
					printf("f_read()14\n\r");
#endif
					ABORT(fp->fs, FR_DISK_ERR);
				}
#if !_FS_READONLY && _FS_MINIMIZE <= 2			/* Replace one of the read sectors with cached data if it contains a dirty sector */

				if ((fp->flag & FA__DIRTY) && fp->dsect - sect < cc)
				{
#if TRACE_FLOW
					printf("f_read()15\n\r");
#endif
					mem_cpy(rbuff + ((fp->dsect - sect) * SSIZE), buff, SSIZE);
				}
#endif
				rcnt = SSIZE * cc;			/* Number of bytes transferred */
				continue;
			}

			if (fp->dsect != sect) 
			{			/* Load data sector if not in cache */

#if TRACE_FLOW
				printf("f_read()16\n\r");
#endif
#if !_FS_READONLY
				if (fp->flag & FA__DIRTY) 
				{		/* Write-back dirty sector cache */
#if TRACE_FLOW
					printf("f_read()17\n\r");
#endif

					if (disk_write(buff, fp->dsect) != RES_OK)
					{
#if TRACE_FLOW
						printf("f_read()18\n\r");
#endif
						ABORT(fp->fs, FR_DISK_ERR);
					}
					fp->flag &= ~FA__DIRTY;
				}
#endif
				if (disk_read(buff, sect) != RES_OK)	/* Fill sector cache */
				{
#if TRACE_FLOW
					printf("f_read()160\n\r");
#endif
					ABORT(fp->fs, FR_DISK_ERR);
				}
			}

			fp->dsect = sect;
		}
		rcnt = SSIZE - (fp->fptr % SSIZE);	/* Get partial sector data from sector buffer */
		if (rcnt > btr)
		{
#if TRACE_FLOW
			printf("f_read()170\n\r");
#endif 
			rcnt = btr;
		}

#if TRACE_FLOW
			printf("f_read()180, fp->fptr mod SSIZE = %lu\n\r",(fp->fptr % SSIZE));
#endif 

		copy_index = (WORD)(fp->fptr % SSIZE);
		mem_cpy(rbuff, &buff[copy_index], rcnt);	/* Pick partial sector */

	}

	LEAVE_FF(fp->fs, FR_OK);
}

/*-----------------------------------------------------------------------*/
/* Write File                                                            */
/*-----------------------------------------------------------------------*/

FRESULT f_write (
	FIL *fp,			/* Pointer to the file object */
	BYTE *buf,                      /* Pointer to the data to be written */
	UINT btw,			/* Number of bytes to write */
	UINT *bw			/* Pointer to number of bytes written */
)
{
	FRESULT res;
	BYTE clst;
	WORD  sect;
	UINT wcnt, cc;
	const BYTE *wbuff = buf;
	BYTE csect;



	*bw = 0;	/* Initialize byte counter */

#if TRACE
	printf("f_write(),99 fp->fsize = %lu\n\r", fp->fsize);
	pause();
#endif
	res = validate(fp->fs);			/* Check validity */
#if TRACE
	printf("f_write(),199 fp->fsize = %lu\n\r", fp->fsize);
	pause();
#endif

	if (res != FR_OK) 
		LEAVE_FF(fp->fs, res);
	if (fp->flag & FA__ERROR)				/* Aborted file? */
		LEAVE_FF(fp->fs, FR_INT_ERR);
	if (!(fp->flag & FA_WRITE))				/* Check access mode */
		LEAVE_FF(fp->fs, FR_DENIED);
	if ((DWORD)(fp->fsize + btw) < fp->fsize) 
		btw = 0;	/* File size cannot reach 4GB */

	for ( ;  btw;							/* Repeat until all data written */
		wbuff += wcnt, fp->fptr += wcnt, *bw += wcnt, btw -= wcnt) 
	{
		if ((fp->fptr % SSIZE) == 0) 
		{	/* On the sector boundary? */
#if TRACE
			printf("f_write()1\n\r");
#endif
			csect = (BYTE)(fp->fptr / SSIZE & (CSIZE - 1));	/* Sector offset in the cluster */
			if (!csect) 
			{
#if TRACE					/* On the cluster boundary? */
				printf("f_write()2\n\r");
#endif
				if (fp->fptr == 0) 
				{		/* On the top of the file? */
#if TRACE
					printf("f_write()3\n\r");
#endif
					clst = fp->sclust;		/* Follow from the origin */
					if (clst == 0)			/* When no cluster is allocated, */
					{
#if TRACE
						printf("f_write()4\n\r");
#endif
						clst = (BYTE)create_chain(fp->fs, 0);	/* Create a new cluster chain */
						fp->sclust = clst;
					}
				} 
				else 
				{					/* Middle or end of the file */
#if TRACE
                                                printf("f_write()5\n\r");
#endif
						clst = (BYTE)create_chain(fp->fs, fp->clust);	/* Follow or stretch cluster chain on the FAT */
				}
#if TRACE
				printf("f_write()6 clst = %d\n\r",clst);
#endif
				if (clst == 0) 
					break;                          /* Could not allocate a new cluster (disk full) */
				if (clst == 1) 
					ABORT(fp->fs, FR_INT_ERR);
				if (clst == 0xFF) 
					ABORT(fp->fs, FR_DISK_ERR);
				fp->clust = clst;			/* Update current cluster */
			}

			if (fp->flag & FA__DIRTY) 
			{                                               /* Write-back sector cache */
#if TRACE
				printf("f_write()7\n\r");
#endif
				if (disk_write(buff, fp->dsect) != RES_OK)
				{
#if TRACE
					printf("f_write()8\n\r");
#endif
					ABORT(fp->fs, FR_DISK_ERR);
				}
				fp->flag &= ~FA__DIRTY;
			}
			sect = clust2sect(fp->fs, fp->clust);	/* Get current sector */
#if TRACE
			printf("f_write()55 sect = %d\n\r",sect);
#endif
			if (!sect) 
				ABORT(fp->fs, FR_INT_ERR);
			sect += csect;

                        if (sect >= MAX_SECTORS)                // Try to prevent file wrapping to sector 0
				ABORT(fp->fs, FR_MEMORY_OVERFLOW);

			cc = btw / SSIZE;			/* When remaining bytes >= sector size, */
#if TRACE
			printf("f_write()56 cc = %d\n\r",cc);
#endif
			if (cc) 
			{
#if TRACE						/* Write maximum contiguous sectors directly */
				printf("f_write()9\n\r");
#endif
				if (csect + cc > CSIZE)	/* Clip at cluster boundary */
				{
#if TRACE
					printf("f_write()10\n\r");
#endif
					cc = CSIZE - csect;
				}
				if (disk_write(wbuff, sect) != RES_OK)
					ABORT(fp->fs, FR_DISK_ERR);

				if (fp->dsect - sect < cc) 
				{ /* Refill sector cache if it gets invalidated by the direct write */
#if TRACE
					printf("f_write()11\n\r");
#endif
					mem_cpy(buff, wbuff + ((fp->dsect - sect) * SSIZE), SSIZE);
					fp->flag &= ~FA__DIRTY;
				}

				wcnt = SSIZE * cc;		/* Number of bytes transferred */
				continue;
			}

			if (fp->dsect != sect) 
			{		/* Fill sector cache with file data */
				if (fp->fptr < fp->fsize)
				{
#if TRACE
					printf("f_write()12 = %d\n\r",sect);
#endif
				}
				if (fp->fptr < fp->fsize && disk_read(buff, sect) != RES_OK)
					ABORT(fp->fs, FR_DISK_ERR);
			}

			fp->dsect = sect;
		}
		wcnt = SSIZE - (fp->fptr % SSIZE);/* Put partial sector into file I/O buffer */
		if (wcnt > btw) 
			wcnt = btw;

		mem_cpy(&buff[fp->fptr % SSIZE], wbuff, wcnt);	/* Fit partial sector */
		fp->flag |= FA__DIRTY;

	}

#if TRACE
	printf("f_write(),77 fp->fptr = %lu, fp->fsize = %lu\n\r", fp->fptr, fp->fsize);
	pause();
#endif

	if (fp->fptr > fp->fsize)
		fp->fsize = fp->fptr;	/* Update file size if needed */
	fp->flag |= FA__WRITTEN;						/* Set file change flag */

	LEAVE_FF(fp->fs, FR_OK);
}

/*-----------------------------------------------------------------------*/
/* Delete a File or Directory                                            */
/*-----------------------------------------------------------------------*/

FRESULT f_unlink (
	const TCHAR *path		/* Pointer to the file or directory path */
)
{
	FRESULT res;
	DIR dj;
	BYTE *dir;
	DWORD dclst;
	DEF_NAMEBUF;

	res = chk_mounted(&path, &dj.fs, 1);

	dj.fs = FatFs[0];

	if (res == FR_OK) 
	{
		INIT_BUF(dj);	// dj.fn = sfn[28]
		res = follow_path(&dj, path);		/* Follow the file path */

		if (res == FR_OK) 
		{					/* The object is accessible */
			dir = dj.dir;
#if DEL
			printf("f_unlink()11,dir = %s\n\r",dir);
#endif
			if (!dir) 
			{
				res = FR_INVALID_NAME;		/* Cannot remove the start directory */
#if DEL
				printf("f_unlink()1\n\r");
				print_fresult(res);
#endif
			} 

			dclst = (WORD)((WORD)*(BYTE*)(dir + DIR_FstClus));
#if DEL
			printf("f_unlink(),dclst = %lu\n\r",dclst);
#endif			
			if (res == FR_OK) 
			{
				res = dir_remove(&dj);		/* Remove the directory entry */
				if (res == FR_OK) 
				{
					if (dclst)				/* Remove the cluster chain if exist */
					{
#if DEL
						printf("f_unlink()11\n\r");
#endif
						res = remove_chain(dj.fs, dclst);
					}
					if (res == FR_OK) 
					{
#if DEL
						printf("f_unlink()12\n\r");
#endif
						res = sync(dj.fs);
					}
					else
					{
#if DEL
						printf("f_unlink()2\n\r");
						print_fresult(res);
#endif
					}
				}
				else
				{
#if DEL
					printf("f_unlink()3\n\r");
					print_fresult(res);
#endif
				}



			}
			else
			{
#if DEL
				printf("f_unlink()4\n\r");
				print_fresult(res);
#endif
			}


		}
		else
		{
#if DEL
			printf("f_unlink()5\n\r");
			print_fresult(res);
#endif
		}
	}
	LEAVE_FF(dj.fs, res);
}

/*-----------------------------------------------------------------------*/
/* Close File                                                            */
/*-----------------------------------------------------------------------*/

FRESULT f_close (
	FIL *fp		/* Pointer to the file object to be closed */
)
{
	FRESULT res;

#if _FS_READONLY
	FATFS *fs = fp->fs;
	res = validate(fs);
	if (res == FR_OK) fp->fs = 0;	/* Discard file object */
	LEAVE_FF(fs, res);

#else

#if TRACE
	printf("f_close()2, fp->fsize = %lu\n\r",fp->fsize);
	pause();
#endif
	
	res = f_sync(fp);		/* Flush cached data */

#if TRACE
	printf("f_close()1\n\r");
#endif

	if (res == FR_OK) 
		fp->fs = 0;	/* Discard file object */
	return res;
#endif
}
/*-----------------------------------------------------------------------*/
/* Read Directory Entry in Sequence                                      */
/*-----------------------------------------------------------------------*/

FRESULT f_readdir (
	DIR *dj,			/* Pointer to the open directory object */
	FILINFO *fno		/* Pointer to file information to return */
)
{
	FRESULT res;
	DEF_NAMEBUF;

        dump_dir_struct(dj);  // PNG DBG

	res = validate(dj->fs);			/* Check validity of the object */

#if TRACE
	printf("f_readdir()0\n\r");
	print_fresult(res);
	pause();
#endif

	if (res == FR_OK) 
	{
		if (!fno) 
		{
			res = dir_sdi(dj, 0);			/* Rewind the directory object */

#if TRACE
			printf("f_readdir()1\n\r");
			print_fresult(res);
			pause();
#endif
		} 
		else 
		{
			INIT_BUF(*dj);
			res = dir_read(dj);				/* Read a directory item */

#if TRACE
			printf("f_readdir()2, dj->fs->fs_type = %x\n\r",dj->fs->fs_type);
			print_fresult(res);
#endif
			if (res == FR_NO_FILE) 
			{		/* Reached end of dir */
				dj->sect = 0;
				res = FR_OK;
			}
			if (res == FR_OK) 
			{				/* A valid entry is found */

				get_fileinfo(dj, fno);		/* Get the object information */

				res = dir_next(dj, 0);		/* Increment index for next */
				if (res == FR_NO_FILE) 
				{
#if TRACE
					printf("f_readdir()3\n\r");
					print_fresult(res);
#endif
					dj->sect = 0;
					res = FR_OK;
				}
			}
		}
	}
        dump_filinfo_struct(fno);  // PNG DBG
	LEAVE_FF(dj->fs, res);
}
/*-----------------------------------------------------------------------*/
/* Create a Directroy Object                                             */
/*-----------------------------------------------------------------------*/

FRESULT f_opendir (
	DIR *dj,			/* Pointer to directory object to create */
	const TCHAR *path	/* Pointer to the directory path */
)
{
	FRESULT res;
	DEF_NAMEBUF;

	res = chk_mounted(&path, &dj->fs, 0);

	dj->fs = FatFs[0];


	if (res == FR_OK) 
	{
		INIT_BUF(*dj);
		res = follow_path(dj, path);			/* Follow the path to the directory */

		if (res == FR_OK) 
		{						/* Follow completed */
			if (dj->dir) 
			{						/* It is not the root dir */	/* The object is not a directory */
				res = FR_NO_PATH;				
			}
			if (res == FR_OK) 
			{
				res = dir_sdi(dj, 0);			/* Rewind dir */
			}
		}
		if (res == FR_NO_FILE) 
			res = FR_NO_PATH;
	}

	LEAVE_FF(dj->fs, res);
}

/*-------------------------------------------------------------------------*/
/* Seek File R/W Pointer                                                 */
/*-----------------------------------------------------------------------*/

FRESULT f_lseek (
	FIL *fp,		/* Pointer to the file object */
	DWORD ofs		/* File pointer from top of file */
)
{
	FRESULT res;
	DWORD clst, bcs, nsect, ifptr;

	res = validate(fp->fs);		/* Check validity of the object */
	if (res != FR_OK) 
		LEAVE_FF(fp->fs, res);
	if (fp->flag & FA__ERROR)			/* Check abort flag */
		LEAVE_FF(fp->fs, FR_INT_ERR);

	/* Normal Seek */

	if (ofs > fp->fsize	&& !(fp->flag & FA_WRITE)) 
		ofs = fp->fsize;				/* In read-only mode, clip offset with the file size */			

	ifptr = fp->fptr;
	fp->fptr = nsect = 0;
	if (ofs) 
	{
		bcs = 4096;	/* Cluster size (byte) */
		if (ifptr > 0 && (ofs - 1) / bcs >= (ifptr - 1) / bcs) 
		{	/* When seek to same or following cluster, */
			fp->fptr = (ifptr - 1) & ~(bcs - 1);	/* start from the current cluster */
			ofs -= fp->fptr;
			clst = fp->clust;
		} 
		else 
		{									/* When seek to back cluster, */
			clst = fp->sclust;						/* start from the first cluster */

			if (clst == 0) 
			{						/* If no cluster chain, create a new chain */
				clst = create_chain(fp->fs, 0);
				if (clst == 1) 
					ABORT(fp->fs, FR_INT_ERR);
				if (clst == 0xFFFFFFFF) 
					ABORT(fp->fs, FR_DISK_ERR);
				fp->sclust = clst;
			}

			fp->clust = clst;
		}
		if (clst != 0) 
		{
			while (ofs > bcs) 
			{						/* Cluster following loop */
				if (fp->flag & FA_WRITE) 
				{			/* Check if in write mode or not */
					clst = create_chain(fp->fs, clst);	/* Force stretch if in write mode */
					if (clst == 0) 
					{				/* When disk gets full, clip file size */
						ofs = bcs; break;
					}
				} 
				else
					clst = get_fat(fp->fs, clst);	/* Follow cluster chain if not in write mode */
				if (clst == 0xFFFFFFFF) 
					ABORT(fp->fs, FR_DISK_ERR);
				if (clst <= 1 || clst >= 128) 
					ABORT(fp->fs, FR_INT_ERR);
				fp->clust = clst;
				fp->fptr += bcs;
				ofs -= bcs;
			}
			fp->fptr += ofs;
			if (ofs % 256) 
			{
				nsect = clust2sect(fp->fs, clst);	/* Current sector */
				if (!nsect) 
					ABORT(fp->fs, FR_INT_ERR);
				nsect += ofs / 256;
			}
		}
	}
	if (fp->fptr % 256 && nsect != fp->dsect) 
	{	/* Fill sector cache if needed */


		if (fp->flag & FA__DIRTY) 
		{			/* Write-back dirty sector cache */
			if (disk_write(buff, fp->dsect) != RES_OK)
				ABORT(fp->fs, FR_DISK_ERR);
			fp->flag &= ~FA__DIRTY;
		}

		if (disk_read(buff, nsect) != RES_OK)	/* Fill sector cache */
			ABORT(fp->fs, FR_DISK_ERR);

		fp->dsect = nsect;
	}

	if (fp->fptr > fp->fsize) 
	{			/* Set file change flag if the file size is extended */
		fp->fsize = fp->fptr;
		fp->flag |= FA__WRITTEN;
	}

	LEAVE_FF(fp->fs, res);
}


TCHAR* f_gets (
	TCHAR* buff,	/* Pointer to the string buffer to read */
	int len,		/* Size of string buffer (characters) */
	FIL* fil		/* Pointer to the file object */
)
{
	int n = 0;
	TCHAR c, *p = buff;
	BYTE s[2];
	UINT rc;

	while (n < len - 1) 
	{			/* Read bytes until buffer gets filled */
		f_read(fil, s, 1, &rc);

		if (rc != 1) 
			break;			/* Break on EOF or error */

		c = s[0];

		*p++ = c;
		n++;

		if (c == '\n') 
			break;		/* Break on EOL */
	}
	*p = 0;
	return n ? buff : 0;			/* When no data read (eof or error), return with error. */
}

/*--------------------------------------------------------------------------

   Module Private Functions

---------------------------------------------------------------------------*/

/* searches down the directory listing checking for a valid filename (first character not zero or DDE (0xE5)
 if DDE found calls dir_next() to bump dj->dir pointer to point to next directory entry.
 if first character 0, end of directory listing
 if a valid filename is found stops searching and returns pointing to this entry */

static FRESULT dir_read (
	DIR *dj			/* Pointer to the directory object that pointing the entry to be read */
)
{
	FRESULT res;
	BYTE c, *dir;
        int canBreak;

	res = FR_NO_FILE;

#if TRACE
	printf("dir_read()1, dj->fs->fs_type = %x\n\r",dj->fs->fs_type);
#endif

	while (dj->sect) 
	{
                if(dj->sect == 0x30)
                {
                    canBreak=1;
                }
		res = move_window(dj->fs, dj->sect);
		if (res != FR_OK) 
			break;

                if( (dj->index % 8)==0)  /* Change to next page*/
                {
                    dj->index=0;
                    dj->dir = win;
                }

		dir = dj->dir;					/* Ptr to the directory entry of current index */

#if TRACE
		printf("dir_read()10 %s\n\r",dir);
		printf("dir_read()10, dj->fs->fs_type = %x\n\r",dj->fs->fs_type);
#endif

		c = dir[DIR_Name];

		if (c == 0) 					/* Reached to end of table */
		{
#if TRACE
			printf("dir_read()2\n\r");
#endif
			res = FR_NO_FILE; 
			break; 
		}	
	
		if (c != DDE)	/* Is it a valid entry? */
		{
#if TRACE
			printf("dir_read()3 Valid directory entry\n\r");
#endif
			break;
		}

		res = dir_next(dj, 0); // Next entry, make dj->dir pointer point to next entry in the win[] buffer that already contains the directory sector (48)

#if TRACE
		printf("dir_read()4 get next directory entry\n\r");
		print_fresult(res);
#endif

		if (res != FR_OK) 
			break;
	}

	if (res != FR_OK)       
		dj->sect = 0;

	return res;
}

/*-----------------------------------------------------------------------*/
/* Put a filename in the directory                                       */
/* Search the directory from the start for a new entry                   */
/*-----------------------------------------------------------------------*/
#if !_FS_READONLY

static FRESULT dir_register(/* FR_OK:Successful, FR_DENIED:No free entry or too many SFN collision, FR_DISK_ERR:Disk error */
                            DIR *dj /* Target directory with object name to be created */
                            )
{
    FRESULT res;
    BYTE c, *dir;




    /* Non LFN configuration
    reset directory index to 0, update dj->sect with correct sector (32)
    and dj->dir = Ptr to the entry in the sector */
    
    res = dir_sdi(dj, 0);
#if TRACE
    printf("dir_register(),1\n\r");
    print_fresult(res);
#endif

    if (res == FR_OK)
    {
        do
        { /* Find a blank entry for the SFN */
#if TRACE
            printf("dir_register(),2 dj->sect = %lu\n\r", dj->sect);
#endif
            res = move_window(dj->fs, dj->sect); // dj->sect set by dir_sdi(), load win[] from this sector

            if( (dj->index % 8)==0) /* Change to next page */
            {
                dj->index=0;
                dj->dir = win;
            }

            if (res != FR_OK)
            {
#if TRACE
                printf("dir_register(),3\n\r");
#endif
                break;
            }
            c = *dj->dir;
            if (c == DDE || c == 0) /* Is it a blank entry? */
            {
#if TRACE
                printf("dir_register(),4\n\r");
#endif
                break;
            }

            res = dir_next(dj, 1); /* Next entry with table stretch */
        } while (res == FR_OK);
    }

    if (res == FR_OK)
    { /* Initialize the SFN entry */

#if TRACE
        printf("dir_register(),5\n\r");
#endif

        res = move_window(dj->fs, dj->sect);

        if( (dj->index % 8)==0)  /* Change to next page */
        {
            dj->index=0;
            dj->dir = win;
        }

#if TRACE

        printf("dir_register(),6 dj->sect = %lu\n\r", dj->sect);
        print_fresult(res);
#endif

        if (res == FR_OK)
        {
            dir = dj->dir;
            mem_set(dir, 0, SZ_DIR); /* Clean the entry PNG TODO revert val to 0 */


#if TRACE
            printf("dir_register()62, strlen(dj->fn) = %d\n\r", strlen(dj->fn));
            pause();
#endif

            mem_cpy(dir, dj->fn, 28); /* Put SFN */
#if TRACE
            printf("dir_register(),7 dj->dir = %s\n\r", dj->dir);

#endif

            dj->fs->wflag = 1;
#if TRACE
            printf("dir_register()77, dj->fs->wflag = %d\n\r", (BYTE) (dj->fs->wflag));
            pause();
#endif
        }
    }

    return res;
}
#endif /* !_FS_READONLY */


/*-----------------------------------------------------------------------*/
/* Check if the file system object is valid or not                       */
/*-----------------------------------------------------------------------*/
static
FRESULT chk_mounted (	/* FR_OK(0): successful, !=0: any error occurred */
	const TCHAR **path,	/* Pointer to pointer to the path name (drive number) */
	FATFS **rfs,		/* Pointer to pointer to the found file system object */
	BYTE chk_wp			/* !=0: Check media write protection for write access */
)
{

	DSTATUS stat;
	FATFS *fs;

#if TRACE1
	printf("chk_mounted()\n\r");
#endif

	/* Check if the file system object is valid or not */

	fs = FatFs[0];

	if (!fs) 
	{
#if TRACE1
		printf("chk_mounted() FR_NOT_ENABLED\n\r");
#endif
		return FR_NOT_ENABLED;		/* Is the file system object available? */
	}

	if (fs->fs_type) 
	{
#if TRACE1					/* If the logical drive has been mounted */
		printf("chk_mounted()3\n\r");
#endif
		stat = disk_status(fs->drv);
		if (!(stat & STA_NOINIT)) 
		{		/* and the physical drive is kept initialized (has not been changed), */

#if TRACE1
			printf("chk_mounted()4\n\r");
#endif
			return FR_OK;				/* The file system object is valid */
		}
	}

	/* The file system object is not valid. */
	/* Following code attempts to mount the volume. (analyze BPB and initialize the fs object) */

	fs->fs_type = 0;					/* Clear the file system object */

	stat = disk_initialize(fs->drv);	/* Initialize low level disk I/O layer */
	if (stat & STA_NOINIT)				/* Check if the initialization succeeded */
	{
#if TRACE1
		printf("chk_mounted()5\n\r");	
#endif
		return FR_NOT_READY;			/* Failed to initialize due to no media or hard error */
	}

	/* An FAT volume is found. Following code initializes the file system object */
#if TRACE1
	printf("chk_mounted()6\n\r");
#endif
	/* Initialize cluster allocation information */

	fs->free_clust = (BYTE)N_FATENT;
	fs->last_clust = 1;
	fs->winsect = 0;		/* Invalidate sector cache */
	fs->wflag = 0;
	fs->cdir = 0;			/* Current directory (root dir) */
	fs->fs_type = FS_FAT16;		/* FAT sub-type */

	return FR_OK;
}
/*-----------------------------------------------------------------------*/
/* Check if the file/dir object is valid or not                          */
/*-----------------------------------------------------------------------*/

static
FRESULT validate (	/* FR_OK(0): The object is valid, !=0: Invalid */
	FATFS *fs		/* Pointer to the file system object */
)
{
#if TRACE
	printf("validate()1, fs->fs_type = %x\n\r",fs->fs_type);
	pause();
#endif	

	if (!fs || !fs->fs_type)
	{
#if TRACE
		if(!fs)
			printf("validate() !fs\n\r");

		if(!fs->fs_type)
			printf("validate() !fs->fs_type\n\r");

		pause();
#endif	
		return FR_INVALID_OBJECT;
	}

	if (disk_status(0) & STA_NOINIT)
		return FR_NOT_READY;

	return FR_OK;
}
/*-----------------------------------------------------------------------*/
/* Change window offset                                                  */
/*-----------------------------------------------------------------------*/

static FRESULT move_window (
	FATFS *fs,		/* File system object */
	WORD sector             /* Sector number to make appearance in the fs->win[] */
)				/* Move to zero only writes back dirty window and does not read a new sector to win[] */
{
	WORD wsect;
	BYTE nf;

	wsect = fs->winsect;

#if TRACE2
	printf("move_window() current win sect = %d, new win sector = %d\n\r",wsect, sector);	
#endif

	if (wsect != sector) 
	{	/* Changed current window */
#if !_FS_READONLY

		if (fs->wflag) /* Write back dirty window if needed */
		{
#if TRACE2
			printf("move_window(), (fs->wflag) sector = %u\n\r",wsect);

			printf("move_window() ready to dump win[]\n\r");
			pause();
#endif

			if (disk_write(win, wsect) != RES_OK)
			{
#if TRACE2
				printf("move_window()3\n\r");
#endif

				return FR_DISK_ERR;
			}
			fs->wflag = 0;
			if (wsect < (FATBASE + FSIZE)) 
			{	/* In FAT area */
#if TRACE2
				printf("move_window()4\n\r");
#endif				
				for (nf = N_FATS; nf > 1; nf--) 
				{	/* Reflect the change to all FAT copies */
#if TRACE2
					printf("move_window()5\n\r");
#endif
					wsect += CSIZE;
					disk_write(win, wsect);
				}
			}
		}
#endif
		if (sector) 
		{

#if TRACE2
			printf("move_window()47 sector = %d\n\r",sector);
#endif

			if (disk_read(win, sector) != RES_OK)
			{
#if TRACE2
				printf("move_window()7\n\r");
#endif							
				return FR_DISK_ERR;
			}
			fs->winsect = sector;
		}
	} 

	return FR_OK;
}
/*-----------------------------------------------------------------------*/
/* FAT handling - Remove a cluster chain                                 */
/*-----------------------------------------------------------------------*/
#if !_FS_READONLY
static
FRESULT remove_chain (
	FATFS *fs,			/* File system object */
	DWORD clst			/* Cluster# to remove a chain from */
)
{
	FRESULT res;
	DWORD nxt;

	if (clst < 2 || clst >= N_FATENT) 
	{	/* Check range */
		res = FR_INT_ERR;
	} 
	else 
	{
		res = FR_OK;
		while (clst < N_FATENT) 
		{			/* Not a last link? */
			nxt = get_fat(fs, clst);			/* Get cluster status */
			if (nxt == 0) 
				break;				/* Empty cluster? */
			if (nxt == 1) 
			{ 
				res = FR_INT_ERR; 
				break; 
			}	/* Internal error? */
			if (nxt == 0xFFFFFFFF) 
			{ 
				res = FR_DISK_ERR; 
				break; 
			}	/* Disk error? */
			res = put_fat(fs, clst, 0);			/* Mark the cluster "empty" */
			if (res != FR_OK) 
				break;
			if (fs->free_clust != 0xFF) 
			{	/* Update FSInfo */
				fs->free_clust++;
				fs->fsi_flag = 1;
			}
			clst = nxt;	/* Next cluster */
		}
	}

	return res;
}
#endif

/*-----------------------------------------------------------------------*/
/* Follow a file path                                                    */
/*-----------------------------------------------------------------------*/

static FRESULT follow_path (	/* FR_OK(0): successful, !=0: error code */
	DIR *dj,			/* Directory object to return last directory and found object */
	const TCHAR *path	/* Full-path string to find a file or directory */
)
{
	FRESULT res;

	// BYTE *dir;

	if (*path == '/' || *path == '\\')	/* Strip heading separator if exist */
		path++;
	dj->sclust = 0;						/* Start from the root dir */

	if ((UINT)*path < ' ') 
	{			/* Null path means the start directory itself */
		res = dir_sdi(dj, 0);
                if(res != FR_OK)
                {
                    print_fresult(res);
                }
		dj->dir = 0;
	} 
	else 
	{							/* Follow path */
#if TRACE1
		printf("follow_path() path = %s\n\r",path);
#endif
		for (;;) 
		{
			res = create_name(dj, &path);	/* Get a segment */
			if (res != FR_OK)
                        {
                            printf("\n\rcreate_name() failed L %d",__LINE__);
                            print_fresult(res);
                            break;
                        }
                       
#if TRACE1
			printf("follow_path()1\n\r");
#endif
			res = dir_find(dj);				/* Find it */
#if TRACE1
			printf("follow_path()2\n\r");
			print_fresult(res);
#endif
			if (res != FR_OK) 
			{				/* Failed to find the object */
				if (res != FR_NO_FILE)
				{
#if TRACE1
					printf("follow_path()22\n\r");
#endif
					break;	/* Abort if any hard error occured */
				}
				break;
			}
			break;
		}
	}

	return res;
}

/*-----------------------------------------------------------------------*/
/* Get sector# from cluster#                                             */
/*-----------------------------------------------------------------------*/


DWORD clust2sect (	/* !=0: Sector number, 0: Failed - invalid cluster# */
	FATFS *fs,		/* File system object */
	DWORD clst		/* Cluster# to be converted */
)
{
	clst -= 2;
	if (clst >= (N_FATENT - 2)) 	/* Invalid cluster# */
		return 0;		
	return clst * CSIZE + DATABASE;
}

/*-----------------------------------------------------------------------*/
/* FAT handling - Stretch or Create a cluster chain                      */
/*-----------------------------------------------------------------------*/
#if !_FS_READONLY
static DWORD create_chain (	/* 0:No free cluster, 1:Internal error, 0xFFFFFFFF:Disk error, >=2:New cluster# */
	FATFS *fs,			/* File system object */
	DWORD clst			/* Cluster# to stretch. 0 means create a new chain. */
)
{
	DWORD cs, ncl, scl;
	FRESULT res;

	if (clst == 0) 	/* Create a new chain */
	{		
		scl = fs->last_clust;		/* Get suggested start point */
#if TRACE
		printf("create_chain(),scl = %lu\n\r",scl);			
#endif
		if (scl >= N_FATENT) 
			scl = 1;
	}
	else 
	{					/* Stretch the current chain */
		cs = get_fat(fs, clst);			/* Check the cluster status */
		if (cs < 2) return 1;			/* It is an invalid cluster */
		if (cs < N_FATENT) 
			return cs;	/* It is already followed by next cluster */
		scl = clst;
	}

	ncl = scl;				/* Start cluster */

	for (;;) 	// searches FAT for a free cluster (00), if found a cluster number skips over this entry and continues the search
	{
		ncl++;							/* Next cluster */
		if (ncl >= N_FATENT) 
		{		/* Wrap around */
			ncl = 2;
			if (ncl > scl) 
				return 0;	/* No free cluster */
		}
		cs = get_fat(fs, ncl);	// Get the cluster status, returns 0 = free cluster, if number cluster has been allocated, if FF = reserved
#if TRACE
		printf("create_chain(),cs = %lu\n\r",cs);
#endif
		if (cs == 0)
		{
#if TRACE
			printf("create_chain()33\n\r"); 
#endif
			break;
		}				/* Found a free cluster */
		if (cs == 0xFFFFFFFF || cs == 1)/* An error occurred */
		{
#if TRACE
			printf("create_chain()34\n\r");
#endif
			return cs;
		}
		if (ncl == scl)
		{
#if TRACE
			printf("create_chain()35\n\r");
#endif
			return 0;		/* No free cluster */
		}
	}

	res = put_fat(fs, ncl, 0x0FFFFFFF);	/* Mark the new cluster "last link" */
#if TRACE
	printf("create_chain(),22\n\r");
	print_fresult(res);
#endif
	if (res == FR_OK && clst != 0) 
	{
#if TRACE
		printf("create_chain()36\n\r");
#endif
		res = put_fat(fs, clst, ncl);	/* Link it to the previous one if needed */
	}
	if (res == FR_OK) 
	{
#if TRACE
		printf("create_chain()37\n\r");
#endif
		fs->last_clust = (BYTE)ncl;			/* Update FSINFO */
		if (fs->free_clust != 0xFF) 
		{
#if TRACE
			printf("create_chain()38\n\r");
#endif
			fs->free_clust--;
			fs->fsi_flag = 1;
		}
	} 
	else 
	{
#if TRACE
		printf("create_chain()40\n\r");
#endif
		ncl = (res == FR_DISK_ERR) ? 0xFFFFFFFF : 1;
	}
#if TRACE	
	printf("create_chain(),55 ncl = %lu\n\r",ncl);
#endif
	return ncl;		/* Return new cluster number or error code */
}
#endif /* !_FS_READONLY */

/*----------------------------------------------------------------------*/
/* Synchronize the File Object 											*/
/*																		*/
/* f_sync is only called by f_close()                                         							*/
/*----------------------------------------------------------------------*/

FRESULT f_sync (
	FIL *fp		/* Pointer to the file object */
)
{
	FRESULT res;

	BYTE *dir;


	res = validate(fp->fs);		/* Check validity of the object */
	if (res == FR_OK) 
	{
#if TRACE
		printf("f_sync()1\n\r");
#endif
		if (fp->flag & FA__WRITTEN) 
		{	/* Has the file been written? */
#if TRACE
			printf("f_sync()2\n\r");
#endif
#if !_FS_TINY	/* Write-back dirty buffer */
			if (fp->flag & FA__DIRTY) 
			{
#if TRACE
				printf("f_sync()3\n\r");
#endif
				if (disk_write(buff, fp->dsect) != RES_OK)
				{
#if TRACE
					printf("f_sync()4\n\r");
#endif
					LEAVE_FF(fp->fs, FR_DISK_ERR);
				}
				fp->flag &= ~FA__DIRTY;
			}
#endif
			/* Update the directory entry */
			res = move_window(fp->fs, fp->dir_sect);
#if TRACE_SMALL
			printf("f_sync()5, fp->dir_sect = %d\n\r",fp->dir_sect);
#endif
			if (res == FR_OK) 
			{

				dir = fp->dir_ptr;

#if TRACE_SMALL
				printf("f_sync()6 fp->fsize = %lu, dir = %s\n\r",fp->fsize, dir);
				pause();
#endif

                                *(BYTE*)(dir+DIR_FileSize) = (BYTE)(fp->fsize);		/* size = 0 */
                                *((BYTE*)(dir+DIR_FileSize)+1)=(BYTE)((WORD)(fp->fsize)>>8);
                                *((BYTE*)(dir+DIR_FileSize)+2)=(BYTE)((DWORD)(fp->fsize)>>16);

				*(BYTE*)(dir+DIR_FstClus) = fp->sclust;

				fp->flag &= ~FA__WRITTEN;
				fp->fs->wflag = 1;
#if TRACE_SMALL
				printf("f_sync()7\n\r");
#endif
				res = sync(fp->fs);
#if TRACE
				printf("f_sync()8\n\r");
#endif
			}
		}
	}

	LEAVE_FF(fp->fs, res);
}

/*-----------------------------------------------------------------------*/
/* Directory handling - Move directory index next                        */
/* Moves dj->dir pointer to point to next directory entry in the win[] buffer,
 * where win[] buffer contains the directory sector (48)	 */
/*-----------------------------------------------------------------------*/

static FRESULT dir_next (	/* FR_OK:Succeeded, FR_NO_FILE:End of table, FR_DENIED:EOT and could not stretch */
	DIR *dj,		/* Pointer to directory object */
	int stretch		/* 0: Do not stretch table, 1: Stretch table if needed */
)
{
	DWORD clst;
	WORD i;



#if TRACE1
	printf("dir_next(), dj->fs->fs_type = %x\n\r", dj->fs->fs_type);
#endif

	stretch = stretch;		/* To suppress warning on read-only cfg. */
	i = dj->index + 1;

	if (!i || !dj->sect)	/* Report EOT when index has reached 65535 */
		return FR_NO_FILE;

	if( (i % 8)==0 ) 		// 8 directory filenames per sector
	{
		dj->sect += 16;		// bump to start of next cluster (only use the first sector of each cluster

#if TRACE1		
		printf("dir_next()1, dj->sect = %ld\n\r",dj->sect);
#endif

		if (dj->clust == 0) 
		{	/* Static table */
#if TRACE1
			printf("dir_next()2\n\r");
#endif
			if (i >= N_ROOTDIR)	/* Report EOT when end of table */
			{
#if TRACE1
				printf("dir_next()3\n\r");
#endif
				return FR_NO_FILE;
			}
		}
		else 
		{					/* Dynamic table */
#if TRACE1
			printf("dir_next()4\n\r");
#endif
			if (((i / (256 / SZ_DIR)) & (CSIZE - 1)) == 0) {	/* Cluster changed? */
				clst = get_fat(dj->fs, dj->clust);				/* Get next cluster */
				if (clst <= 1) return FR_INT_ERR;
				if (clst == 0xFFFFFFFF) return FR_DISK_ERR;
				if (clst >= N_FATENT) {					/* When it reached end of dynamic table */
#if !_FS_READONLY
					BYTE c;
					if (!stretch) return FR_NO_FILE;			/* When do not stretch, report EOT */
					clst = create_chain(dj->fs, dj->clust);		/* Stretch cluster chain */
					if (clst == 0) return FR_DENIED;			/* No free cluster */
					if (clst == 1) return FR_INT_ERR;
					if (clst == 0xFFFFFFFF) return FR_DISK_ERR;
					/* Clean-up stretched table */
					if (move_window(dj->fs, 0)) return FR_DISK_ERR;	/* Flush active window */
					mem_set(win, 0, 256);			/* Clear window buffer */
					dj->fs->winsect = (WORD)(clust2sect(dj->fs, clst));	/* Cluster start sector */
					for (c = 0; c < CSIZE; c++) {		/* Fill the new cluster with 0 */
						dj->fs->wflag = 1;
						if (move_window(dj->fs, 0)) return FR_DISK_ERR;
						dj->fs->winsect++;
					}
					dj->fs->winsect -= c;						/* Rewind window address */
#else
					return FR_NO_FILE;			/* Report EOT */
#endif
				}
				dj->clust = clst;				/* Initialize data for new cluster */
				dj->sect = clust2sect(dj->fs, clst);
			}
		}
	}
#if TRACE1
	printf("dir_next()5 i = %d\n\r", i);
#endif
	dj->index = i;

	dj->dir = win + (DWORD)(i * SZ_DIR);
        
#if TRACE1
	printf("dir_next()6  dj->fs->fs_type = %x\n\r", dj->fs->fs_type);
#endif

	return FR_OK;
}
/*-----------------------------------------------------------------------*/
/* Directory handling - Set directory index                              */
/*-----------------------------------------------------------------------*/

static FRESULT dir_sdi (
	DIR *dj,		/* Pointer to directory object */
	WORD idx		/* Directory index number */
)
{
	DWORD clst;
	WORD inter_calc;
	WORD inter_calc1;
	WORD temp;
	WORD inter;

        dump_dir_struct(dj);

	dj->index = idx;
	clst = dj->sclust;

#if DEL
	printf("dir_sdi(), clst = %lu, dj->index = %u\n\r",clst,dj->index);
#endif

	if (clst >= N_FATENT)	/* Check start cluster range */
		return FR_INT_ERR;


	if (clst == 0) 
	{	/* Static table (root-dir in FAT12/16) */
		dj->clust = clst;
		if (idx >= N_ROOTDIR)		/* Index is out of range */
			return FR_INT_ERR;

#if DEL
		printf("dir_sdi()1, sector = %ld\n\r",dj->sect);
#endif
		// number of directory entries per directory sector = 8
                //if inter_calc = 0 sector = 32, inter_calc = 1 sector = 48, inter_calc = 2 sector = 64
		inter_calc = idx >> 3; // divide by 8	// which sector is idx in. 
#if DEL
		printf("dir_sdi()21, inter_calc = %u\n\r",inter_calc);
#endif
		temp = DIRBASE;
		inter_calc1 = inter_calc << 4; // multiply by 16
		inter_calc = temp + inter_calc1; 

		dj->sect = (DWORD)inter_calc;	/* Sector# */
#if DEL
		printf("dir_sdi()2, dj->sector = %lu\n\r",dj->sect);
#endif
	}


	inter = (idx % 8);
#if DEL
	printf("inter = %d\n\r",inter);
#endif
	inter = inter << 5; // multiply by 32, each directory entry is 32 bytes long
#if DEL
	printf("1, inter = %d\n\r",inter);
#endif

	dj->dir = win + inter;	/* Ptr to the entry in the sector */

	return FR_OK;	/* Seek succeeded */
}
/*-----------------------------------------------------------------------*/
/* FAT access - Change value of a FAT entry                              */
/*-----------------------------------------------------------------------*/
#if !_FS_READONLY

FRESULT put_fat (
	FATFS *fs,	/* File system object */
	DWORD clst,	/* Cluster# to be changed in range of 2 to fs->n_fatent - 1 */
	DWORD val	/* New value to mark the cluster */
)
{

	FRESULT res;


	if (clst < 2 || clst >= N_FATENT) 	/* Check range */
	{	
		res = FR_INT_ERR;
	} 
	else 
	{
		res = move_window(fs, FATBASE);
		if (res == FR_OK)
		{
#if TRACE
			printf("put_fat(),1 clst = %lu, val = %lx\n\r",clst,val);
#endif
                        win[clst] = (BYTE)(val);
		}
		fs->wflag = 1;
	}

	return res;
}
#endif /* !_FS_READONLY */

/*-----------------------------------------------------------------------*/
/* FAT access - Read value of a FAT entry                                */
/*-----------------------------------------------------------------------*/


DWORD get_fat (	/* 0xFFFFFFFF:Disk error, 1:Internal error, Else:Cluster status */
	FATFS *fs,	/* File system object */
	DWORD clst	/* Cluster# to get the link information */
)
{
	BYTE *p;

	if (clst < 2 || clst >= N_FATENT)	/* Chack range */
		return 1;

	if (move_window(fs, FATBASE)) 	
		return 0xFFFFFFFF;				/* An error occurred at the disk I/O layer */
	p = &win[clst];
#if TRACE
	printf("get_fat(),1 return value = %u\r\n",(WORD)((WORD)*(BYTE*)(p)));
#endif
// pjs	return LD_WORD(p);
		return (WORD)((WORD)*(BYTE*)(p));

}


/*-----------------------------------------------------------------------*/
/* Directory handling - Find an object in the directory                  */
/*-----------------------------------------------------------------------*/

static FRESULT dir_find (
	DIR *dj			/* Pointer to the directory object linked to the file name */
)
{
	FRESULT res;
	BYTE c, *dir;

	#if TRACE1
            printf("dir_find()0 dj->fn = %s\n\r",dj->fn);
	#endif

	res = dir_sdi(dj, 0);			/* Rewind directory object */

	#if TRACE1
            printf("dir_find()1,\n\r");
            print_fresult(res);
	#endif

	if (res != FR_OK)
        {
            print_fresult(res);
            return res;
        }

	#if TRACE1
            printf("dir_find()2 dj->fn = %s - L %d\n\r",dj->fn,__LINE__);
	#endif

	do 
	{
		#if TRACE1
                    printf("dir_find() dj->sect = %lu\n\r",dj->sect);
		#endif

		res = move_window(dj->fs, dj->sect);

                if( (dj->index % 8)==0) /* Change to next page */
                {
                    dj->index=0;
                    dj->dir = win;
                }

		#if TRACE1
                    printf("dir_find()3,\n\r");
		#endif		
		
		if (res != FR_OK) 
		{
                    printf("\r\nmove_window() failed L %d\r\n",__LINE__);
                    print_fresult(res);
			#if TRACE1
                            printf("dir_find()97\n\r");
			#endif

			break;
		}
		dir = dj->dir;					/* Ptr to the directory entry of current index */		

		c = dir[DIR_Name];

		if (c == 0) 
		{ 
			res = FR_NO_FILE;
			#if TRACE1
			printf("dir_find()87\n\r");
			#endif

			break; 				/* Reached to end of table */
		}	
		
		#if TRACE1
                    printf("dir_find()33 dir = %s, dj->fn = %s, strlen(dj->fn) = %d \n\r",dir, dj->fn, strlen(dj->fn));
                    pause();
		#endif

		/* Non LFN configuration */
		if (!mem_cmp(dir, dj->fn, 28)) /* Is it a valid entry? */
		{
			#if TRACE1
                            printf("dir_find() Found Directory Entry\n\r");
			#endif
			break;
		}

		res = dir_next(dj, 0);		/* Next entry */
	} while (res == FR_OK);

	return res;
}
/*-----------------------------------------------------------------------*/
/* Remove an object from the directory                                   */
/*-----------------------------------------------------------------------*/
static
FRESULT dir_remove (	/* FR_OK: Successful, FR_DISK_ERR: A disk error */
	DIR *dj				/* Directory object pointing the entry to be removed */
)
{
	FRESULT res;

#if DEL
	printf("dir_remove()1, dj->dir = %s\n\r",dj->dir);
#endif
			
	res = dir_sdi(dj, dj->index);

#if DEL
	printf("dir_remove()2, dj->dir = %s\n\r",dj->dir);
#endif

	if (res == FR_OK) 
	{
//          res = move_window(dj->fs, dj->sect);  SL Removed to fix deleting bug

#if DEL
            printf("dir_remove()3, dj->dir = %s\n\r",dj->dir);
#endif
            if (res == FR_OK)
		{
#if DEL
                    printf("dir_remove()4 dj->dir = %s\n\r",dj->dir);
#endif
                    *dj->dir = DDE;			/* Mark the entry "deleted" */
                    dj->fs->wflag = 1;
		}
	}

	return res;
}



/*-----------------------------------------------------------------------*/
/* Pick a segment and create the object name in directory form           */
/*-----------------------------------------------------------------------*/

static FRESULT create_name (
	DIR *dj,			/* Pointer to the directory object */
	const TCHAR **path	/* Pointer to pointer to the segment in the path string */
)
{
	/* Non-LFN configuration */
	BYTE c,*sfn;
	UINT ni, si, i;
	const char *p;

        p = *path;

#if TRACE1
	printf("create_name(), path = %s\n\r",p);
#endif

	/* Create file name in directory form */
	for (p = *path; *p == '/' || *p == '\\'; p++) ;	/* Strip duplicated separator */
	sfn = dj->fn;
	mem_set(sfn,0, 28);

	si = i = 0; 
	ni = 28;

	for (;;) 
	{
		c = (BYTE)p[si++];
		if (c <= ' ' || c == '/' || c == '\\')
                {
                    break;	/* Break on end of segment */
                }
		if (c >= 0x80 || i >= ni) 
		{
#if TRACE1
			printf("create_name() c = %c, ni = %d\n\r",c,ni);
#endif
			return FR_INVALID_NAME;
		}
	
		if (chk_chr("\"*:<>\?|\x7F", c))	/* Reject illegal chrs for SFN */
		{
#if TRACE1
			printf("create_name()2\n\r");
#endif

			return FR_INVALID_NAME;
		}

		sfn[i++] = c;
	}
	*path = &p[si];						/* Return pointer to the next segment */
	c = (c <= ' ') ? NS_LAST : 0;		/* Set last segment flag if end of path */

	if (!i)
	{
#if TRACE1
		printf("create_name()3\n\r"); 
#endif

		return FR_INVALID_NAME;		/* Reject nul string */
	}

	return FR_OK;
}

/*-----------------------------------------------------------------------*/
/* Get File Status                                                       */
/*-----------------------------------------------------------------------*/

FRESULT f_stat (
	const TCHAR *path,	/* Pointer to the file path */
	FILINFO *fno		/* Pointer to file information to return */
)
{
	FRESULT res;
	DIR dj;
        BYTE sfn[28];

	res = chk_mounted(&path, &dj.fs, 0);

	dj.fs = FatFs[0];
        

	if (res == FR_OK) 
	{
                dj.fn = (BYTE*)sfn;
#if TRACE2
                printf("\r\npath == %s\r\n",path);
#endif
		res = follow_path(&dj, path);	/* Follow the file path */
		if (res == FR_OK) 
		{				/* Follow completed */
			if (dj.dir)
                        {
                            /* Found an object */
                            get_fileinfo(&dj, fno);
                        }
			else
                        {/* It is root dir */
				res = FR_INVALID_NAME;
                                printf("\n\rFR_INVALID_NAME\r\n");
                        }
		}
#if TRACE2
                else
                {
                    printf("\n\rfollow_path() failed - L %d\r\n",__LINE__);
                    print_fresult(res);
                }
#endif
	}
        else
        {
            printf("\r\nf_stat() - call to chk_mounted() failed!!\r\n");
        }

	LEAVE_FF(dj.fs, res);
}

/*-----------------------------------------------------------------------*/
/* Get file information from directory entry                             */
/*-----------------------------------------------------------------------*/

static
void get_fileinfo (		/* No return code */
	DIR *dj,			/* Pointer to the directory object */
	FILINFO *fno	 	/* Pointer to the file information to be filled */
)
{
	UINT i;
	BYTE *dir;
	TCHAR *p, c;

	p = fno->fname;
	if (dj->sect) 
	{
		dir = dj->dir;  // point to current sfn entry in the window

		for (i = 0; i < (_MAX_LFN - 1); i++) 
		{	/* Copy name body */
			c = dir[i];
			if (c == NUL)
				break;
			*p++ = c;
		}

		fno->fsize = (DWORD)(((DWORD)*((BYTE*)(dir + DIR_FileSize)+2)<<16) | ((WORD)*((BYTE*)(dir + DIR_FileSize)+1)<<8) | *(BYTE*)(dir + DIR_FileSize));
	}

	*p = 0;		/* Terminate SFN str by a \0 */

        dump_dir_struct(dj);
        dump_filinfo_struct(fno);
}

/*-----------------------------------------------------------------------*/
/* Clean-up cached data                                                  */
/*-----------------------------------------------------------------------*/
#if !_FS_READONLY
static FRESULT sync (	/* FR_OK: successful, FR_DISK_ERR: failed */
	FATFS *fs	/* File system object */
)
{
	FRESULT res;

	// printf("sync()10\n\r");
	res = move_window(fs, 0);
	// printf("sync()1\n\r");
	if (res == FR_OK) 
	{
		/* Update FSInfo sector if needed */

		/* Make sure that no pending write process in the physical drive */
		if (disk_ioctl(0, CTRL_SYNC, 0) != RES_OK)
			res = FR_DISK_ERR;
	}

	return res;
}
#endif

/*-----------------------------------------------------------------------*/
/* String functions                                                      */
/*-----------------------------------------------------------------------*/

/* Copy memory to memory */
static void mem_cpy (BYTE* dst, const BYTE* src, UINT cnt) 
{
	BYTE *d = dst;
	const BYTE *s = src;

#if _WORD_ACCESS == 1
	while (cnt >= sizeof(int)) 
	{
		*(int*)d = *(int*)s;
		d += sizeof(int); s += sizeof(int);
		cnt -= sizeof(int);
	}
#endif
	while (cnt--)
		*d++ = *s++;
}

/* Fill memory */
void mem_set (void* dst, int val, UINT cnt)
{
	BYTE *d = (BYTE*)dst;

	while (cnt--)
		*d++ = (BYTE)val;
}

/* Compare memory to memory */
static int mem_cmp (const void* dst, const void* src, UINT cnt) 
{
	const BYTE *d = (const BYTE *)dst, *s = (const BYTE *)src;
	int r = 0;

	while (cnt-- && (r = *d++ - *s++) == 0) ;
	return r;
}

/* Check if chr is containe in the string */
static int chk_chr (const char* str, int chr) 
{
	while (*str && *str != chr) 
		str++;
	return *str;
}

void dump_dir_struct(DIR *thedir)
{
#if 0
    if (thedir)
    {
        printf("\n\rdump_dir_struct()");
        printf("\r\nindex == %d ", thedir->index); /* Current read/write index number */
        printf("\r\nsclust == %ld", thedir->sclust); /* Table start cluster (0:Root dir) */
        printf("\r\nclust == %ld", thedir->clust); /* Current cluster */
        printf("\r\nsect == %ld", thedir->sect); /* Current sector */
        printf("\r\nfn == %s\r\n", thedir->fn); /* Pointer to the SFN (in/out)  */
    }
#endif
}


void dump_filinfo_struct(FILINFO *thefilinfo)
{
#if 0
    if (thefilinfo)
    {
        printf("\n\rdump_filinfo_struct()");
        printf("\n\rsize == %ld", thefilinfo->fsize); /* File size */
        printf("\n\rfname == %s", thefilinfo->fname[28]);
    }
#endif
}
//------------------------------ END OF FILE ---------------------------------

