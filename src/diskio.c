/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  This file contains low-level I/O routines to access physical media as
 *  required by the FatFS filesystem.
 *
 *  May 2014 - P Gomme   First release
 *  Jan 2015 - SLL       Cleanup redundant declarations
 *
 ******************************************************************************/

#include "global.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "diskio.h"
#include "spieeprom.h"
#include "ff.h"

static volatile DSTATUS Stat = STA_NOINIT;                          // Disk status

extern BYTE win[256];
extern BYTE buff[256];

void dump_win (void);
void dump_buffer(void);
void print_hex(BYTE);


DSTATUS disk_initialize (BYTE temp)
{
	Stat &= ~STA_NOINIT;                                        // Clear STA_NOINIT
	return (Stat); 
}

//-----------------------------------------------------------------------/
// Get Disk Status                                                       /
//-----------------------------------------------------------------------/

DSTATUS disk_status (BYTE drv)                                      // Physical drive nmuber (0)
{
	if (drv) 
		return STA_NOINIT;                                  // Supports only single drive
	Stat = 0;                                                   // Make good
	return (Stat); 
}

//-----------------------------------------------------------------------/
// Read Sector(s)                                                        /
//-----------------------------------------------------------------------/

DRESULT disk_read(
                  BYTE *buff,                                       // Pointer to the data buffer to store read data
                  WORD sector                                       // Start sector number (LBA)
                  )
{
    DWORD address;

#if TRACE_FLOW
    printf("DISK_READ(), sector = %d\n\r", sector);
#endif	

    address = (DWORD) sector * 256;                                 // Convert to byte address

    EEPROM_wait_for_write_complete();                               // Just in case

    EEPROM_Read_Sector(address, buff);                              // EEPROM sector size is only 256 bytes

    return RES_OK;
}

//-----------------------------------------------------------------------/
// Write Sector(s)                                                       /
//-----------------------------------------------------------------------/

#if _READONLY == 0
DRESULT disk_write (
	const BYTE *buff,                                           // Pointer to the data to be written
	WORD sector                                                 // Start sector number (LBA)
)
{
	DWORD address;
	WORD cluster;

	if (Stat & STA_NOINIT) 
		return RES_NOTRDY;
	if (Stat & STA_PROTECT) 
		return RES_WRPRT;

	address = (DWORD)sector * 256;                              // Convert to byte address

	cluster = sector % CSIZE;                                   // If we are on a whole cluster, erase the cluster

#if TRACE_FLOW
	printf("DISK_WRITE(), sector = %d, remain cluster = %d, address = %lx\n\r",sector, cluster, address);
        printf("\n0x%02x  0x%02x  0x%02x  0x%02x  0x%02x  0x%02x  0x%02x  0x%02x  \r\n",
               *buff, *(buff+1), *(buff+2), *(buff+3), *(buff+4), *(buff+5), *(buff+6), *(buff+7) );
#endif

	if(cluster == 0)
        {
		EEPROMEraseCluster(address);                        // Clear out one cluster (16 * sectors) first before writing data back
        }

	EEPROM_wait_for_write_complete();                           // Just in case

	EEPROM_Write_Sector(address, buff);                         // EEPROM sector size is only 256 bytes

	return RES_OK;
}
#endif

DRESULT disk_ioctl (
	BYTE drv,                                                   // Physical drive nmuber (0)
	BYTE ctrl,                                                  // Control code
	void *buff                                                  // Buffer to send/receive data block
)
{
	DRESULT res;
	BYTE clusters;
	DWORD *dp,st,ed;
	DWORD address;
	
	if (drv) 
		return RES_PARERR;

	if (Stat & STA_NOINIT) 
		return RES_NOTRDY;

	res = RES_ERROR;

	switch (ctrl) 
	{
		case CTRL_SYNC :                                    // Flush write-back cache, Wait for end of internal process
			res = RES_OK;
			break;

		case CTRL_ERASE_SECTOR :                            // erase a block of sectors, buff contains start and end sectors to erase
			dp = buff;
			st = dp[0];
			ed = dp[1];
			clusters = ((ed - st) + 1) % 4;             // 4, 1024 byte sectors in one cluster (4K bytes)

			if (clusters != 0)
			{
				break;
			}
			clusters = ((ed - st) + 1) / 4;
			st++;                                       // get over sector zero erase problem
			address = st * 4096;                        // get start address on cluster boundary

			while(clusters > 0)
			{
				EEPROMEraseCluster(address);
				address += 4096;                    // bump address to next (4k byte) cluster
				clusters--;
			}
			res = RES_OK;
			break;

		case GET_SECTOR_COUNT :                             // Get number of sectors on the disk (WORD)
			*(DWORD*)buff = N_FATENT;                   // (size of SPI_Flash memory) / 1024 (sector size)
			res = RES_OK;
			break;

		case GET_SECTOR_SIZE :                              // Get bytes per sector
			*(WORD*)buff = 512;
			res = RES_OK;
			break;

		case GET_BLOCK_SIZE :                               // Get erase block size in unit of sectors (DWORD)
			*(DWORD*)buff = 8;                          // 512 bytes * 8 = 4k bytes minimum ersae size
			res = RES_OK;
			break;

		case MMC_GET_TYPE :                                 // Get card type flags (1 byte)
			res = RES_OK;
			break;

		case MMC_GET_CID :                                  // Receive CID as a data block (16 bytes)
			res = RES_OK;
			break;

		case MMC_GET_OCR :                                  // Receive OCR as an R3 resp (4 bytes)
			res = RES_OK;
			break;

		case MMC_GET_SDSTAT :                               // Receive SD statsu as a data block (64 bytes)
			res = RES_OK;
			break;

		default:
			res = RES_PARERR;
	}
	return res;
}

//------------------------------ END OF FILE ----------------------------------