/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/******************************************************************************/
/*  User Level #define Macros
 *
 *  May 2014 - P Gomme   First Release
 *  Nov 2014 - SL        MOved #define statements for clock rates from user.c
 *  Dec 2014 - SLL       Update for v3 PIC device
 *
 ******************************************************************************/


/* Application specific user parameters used in user.c may go here            */

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

/*  User level functions prototypes (e.g. InitApp) go here                    */

void InitApp(void);         // I/O and Peripheral Initialization
void InitInt(void);         // Enable Interrupts for switches and 8280IRQ

/* Constant definitions go here                                               */
#define BAUDRATE1               115200
#define BAUDRATE2               9600
#define SYSCLK                  32000000

#define BAUDRATEREG_BRGH_1      SYSCLK/8/BAUDRATE1-1
#define BAUDRATEREG_BRGH_2      SYSCLK/8/BAUDRATE2-1
