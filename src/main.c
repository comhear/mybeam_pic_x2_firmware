//******************************************************************************
//
// Copyright (c) 2015 ComHear, Inc.  All rights reserved.
//
// This software as well as any related documentation is furnished under
// license and may only be used or copied in accordance with the terms of the
// license. The information in this file is furnished for informational use
// only, is subject to change without notice, and should not be construed as
// a commitment by ComHear, Inc.
//
// ComHear, Inc assumes no responsibility or liability for any errors or
// inaccuracies that may appear in this document or any software that may be
// provided in association with this document.
//
// Except as permitted by such license, no part of this document may be
// reproduced, stored in a retrieval system, or transmitted in any form or by
// any means without the express written consent of ComHear, Inc.
//
// *****************************************************************************

// XMODEM Download Implementation
// Copyright 2001-2010 Georges Menie (www.menie.org)
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of California, Berkeley nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//******************************************************************************
//  MyBeam System Main Program
//
//  May 2014 - P Gomme  First Release
//  Oct 2014 - SLL      Multiple updates adding the following functionality;
//                         Headset detect
//                         Interrupr driven switch detect and handling
//                         Binary filter coefficient and text parameter download
//  Dec 2014 - SLL      Update for v3 PIC device
//  Apr 2015 - SLL      Multiple updates to baseline as v3.1. New features and changes include:
//                         USB debug port added but must use UART2 so compiler switch added to allow FTDI on UART2 as option
//                         Data store to sector 65 of the EEPROM implemented for volume level to location 0
//                         Changed volume steps from 1dB to 3dB
//                         Implemented DSP watchdog functionality
//                         Changed commands for tri colours LEDs using initial v2 Front Panel implementation
//                         Disconnected unused input based on BT interrupt detection to reduce noise floor
//  Jul 2015 - SLL      Multiple updates to baseline as v3.2. New features include:
//                         Replace timer 2/3 with timer 4/5 for volume write delay to free timer 2 for security code
//                         Update DSP revision display to use new two register values and display for both DSP1 and DSP4
//                         Change Bluetooth streaming feedback prevention to disable Mic_Bias1 instead of disable AIF2TX6
//                         Add watchdog enable/disable control from parameter address 0x500000
//                         Add LED brightness increase/decrease from parameter address 0x500001
//                         Implement new control code and mode status register to use all v3.2 DSP builds
//                         Update phase test routine to use new DSP4 delay function to balance the speaker 6 & 7 paths
//                         Add Self-Test routine including parameter address 0x500002 to select module revision
//                         Add Bar sound mode with parameter address 0x500003 to enable
//                         Add Front Panel SW revision retrieval
//  Sep 2015 - SLL      Multiple updates for v3.3 release. New features include;
//                         Addition of Bootloader function (Jeff Claar)
//                         Move BT Re_pair until after outputs enabled to ensure that the pairing tone is audible
//                         Fix non functional call answer button by disabling BT interrupts while answer or end command is issued and forcing mode change
//                         Modify front panel version retrieval to allow detectiontion of v1 or missing board and modify all LED calls to use detected board
//                         Activate volume LEDs
//                         Add new feature to read version and parameter information from modified filter files (old files still load normally)
//                         Tidy text indentation of load_firmware for ease of reading - no change to source code
//                         Modify toneon test command to add arguemnts for channel and level and delete toneoff command
//                         Add loading of first 1k bytes of fw1 & fw2 from header files (with parameter file disable mechanism)
//                         Simplify volume change and add mechanism for offset to external sub output via parameter file
//                         Remove occasional buzz from BT at reset due to bad start-up and iterference by 8281 interrupt
//                         Update Call Answer button handler to do nothing if BT mode is not In Call or Incoming Request
//  Oct 2015 - SLL       Multiple updates for v3.4 release. Changes include;
//                         Maximise volume increase maximum range
//                         Fix sensitivity of Call Answer function
//                         Add capability to swap volume key functionality for units assembled with the keys reversed
//                         Correct render mode regs for Widebeam call mode
//                         Correct possible bluetooth code crashes (change in Bluetooth.c only)
//                         Remove unnecesaary filter update falgs from SetMode
//                         Add check of parameter revistion to force EEPROM wipe if old version loaded. Threshold set in global.h
//  Nov 2015 - SLL       Create version suitable for X2 hardware
//                         Improve robustness to BT unexpected behaviour
//
//******************************************************************************

//******************************************************************************
// Files to Include                                                            *
//******************************************************************************

#if defined(__XC16__)
#include <xc.h>
#elif defined(__C30__)
#if defined(__PIC24E__)
#include <p24Exxxx.h>
#elif defined (__PIC24F__)||defined (__PIC24FK__)
#include <p24Fxxxx.h>
#elif defined(__PIC24H__)
#include <p24Hxxxx.h>
#endif
#endif

#include "system.h"                                                 // System funct/params, like osc/peripheral config
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>                                                 // Includes uint16_t definition
#include <string.h>
#include <stdbool.h>                                                // Includes true/false definition
#include <uart.h>
#include <ports.h>
#include <libpic30.h>
#include "integer.h"
#include "conio.h"
#include "spieeprom.h"
#include "ff.h"
#include "ffconf.h"
#include "diskio.h"
#include "user.h"                                                   // User funct/params, such as InitApp
#include "florida.h"
#include "fwloader.h"
#include "xmodem.h"
#include "hwdefs.h"
#include "global.h"
#include "Bluetooth.h"
#include "bootloader.h"
#include "fw1.h"                                                    // First 1k bytes of firmware data memory for security
#include "fw2.h"

//*****************************************************************************
// Global Variable Declaration                                                *
//*****************************************************************************

DECLARE_LAUNCH_BOOTLOADER

BYTE terminal = 2;
WORD data_in_index;
WORD data_out_index;
WORD receive_data_in_index;
WORD receive_data_out_index;
BYTE receive_buffer[RECEIVE_BUFFER_LEN];
WORD max_chars;

FATFS fs[2];                                                        // Work area (file system object) for logical drives
static FIL fdst;                                                    // file objects

FRESULT res;                                                        // FatFs function common result code
BYTE win[256];                                                      // Window buffer for FILE SYSTEM
BYTE buff[256];                                                     // File data read / write buffer buffer for FILE SYSTEM

int Page_Index;
DWORD file_byte_count;

WORD max_chars;
BYTE dump_buff[256];                                                // Global to avoid using too much stack
BYTE cli_buffer[128];
BYTE second_user_buffer[256];
BYTE break_flag;
BYTE scroll_buffer[4][32];
BYTE scroll_line = 0;
BYTE cursor_pos[10];
BYTE store_buffer[256];
BYTE nv_buffer[256];

char strbuffer[40];

BYTE xbuff[134];                                                    // 128 for XModem + 3 head chars + 2 crc + nul,
WORD chars_in_buffer;

BYTE DSP_Status = 0x01;                                             // Initialise device mode status to default
BYTE old_mode = 0x01;                                               // Initialise pre-headphone mode save register
BOOL Command_Locked = FALSE;
BOOL auto_nman = TRUE;
BOOL dev_mode = FALSE;
BOOL bt_req = FALSE;
BOOL mode_change = TRUE;                                            // Ensure mode is initialised in main loop
BOOL first_time = TRUE;
BOOL incoming = FALSE;
BOOL password_valid = FALSE;
BOOL watchdog_en = FALSE;
BYTE module_rev = 1;                                                // Set defualt module revision to x1
BOOL bar_mode = FALSE;                                              // default small array so Bar mode not used
BYTE filter1_info[FLT_HDR_LEN];
BYTE filter2_info[FLT_HDR_LEN];
int config_rev = 0;
int param_rev = 0;
int sub_offset = SUB_OFFSET_DEFAULT;
BOOL fw_header_en = TRUE;
BOOL key_swap = FALSE;
BYTE key_code = 0;
long int count;
int timeout = 1000;

BYTE temp_buffer[512];
BYTE user_buffer[256];
BYTE user_buffer_backup[256];

unsigned char Rx2_data[256];
unsigned int buffer2_count = 0;
BOOL rx2_complete = FALSE;

void write_DAC_word(WORD data);
int loadConfigandDSP(void);
int checkValidFilesystem(void);
int createFilesystem(void);
void print_fresult(int rc);
void developmentMode(void);
BYTE getUartTerm(void);
int getcht(WORD timeout);                                           // msec timeout
void cli(void);
BYTE get_extension(BYTE *pch);
FRESULT scan_files(char* path);
void dump_sector(WORD sector);
void help_func(void);
BYTE pause(void);
void test_spi_to_Florida(void);
int save_file(BYTE *buffer, BYTE file_type);
BYTE loadCores(void);
void load_filter(DWORD Addr);
void load_fw_header(int core);
void load_tone(DWORD Addr);
void load_script(void);
void storeFirmware(void);
void readFirmware(void);
unsigned long copyFWfromDspToFlash(unsigned long startAddr, unsigned long length, unsigned long sector);
void copyFlashToDsp(unsigned long addr, unsigned long sector, unsigned long);
long xmodemReceive(BYTE file_type);
unsigned short crc16_ccitt(const unsigned char *buf, int len);
void putch3(char);
void USB_reset(void);
void DelayMs(WORD delay);
void type_command(BYTE extension);
void startupBanner();
void adjustIPVolume(int);
void SetMode(void);
void test_tone(void);
void speaker_test(int);
void phase_test(int);
void nv_read(void);
void nv_write(void);
void nv_default(void);

//*****************************************************************************
//* Main Program                                                              *
//*****************************************************************************

int16_t main(void)
{
    __C30_UART = terminal;                                          // Define debug terminal 1= FDTI, 2 = 2114 USB

    unsigned int i;
    WORD loop = 0;
    BYTE bt_stat = 0;

    ConfigureOscillator();                                          // Configure the oscillator for the device

    InitApp();                                                      // Initialise IO ports and peripherals, including mapping physical pins

    USB_RST(LOW);                                                   // USB,BT and 8280 resets non active
    RST_8280(HIGH);
    BT_CMD(HIGH);
    DACMUTE(HIGH);                                                  // Ensure DAC and AMPs muted
    AMPMUTE(LOW);

    USB_VOLDN(HIGH);                                                // Initialise discrete output lines for USB volume & mute
    USB_VOLUP(HIGH);
    USB_PMUTE(HIGH);
    USB_RMUTE(HIGH);

    if (f_mount(0, &fs[0]))
    {
        printf("System Not Mounted\n\r");
        HALT;
    }

//    re_start:
    if ((checkValidFilesystem() != 0) || (param_rev == -1))
    {                                                               // Filesystem is invalid
        printf("\n\rNo File System on Flash Memory\n\r");
        printf("Format and install File System? (y / n) ");
        if (getUartTerm() == 'y')
        {
            if (createFilesystem())
            {
                printf("\r\nUnable to create filesystem\r\n");
            }
            else
            {
                printf("\n\rFilesystem created on SPI Flash EEPROM\r\n");
            }
        }
        param_rev = PARAM_LIMIT;
    }

    for (i=0; i < 256; i++)                                         // Save state of user buffer for Development Mode
        user_buffer_backup[i] = user_buffer[i];

    USB_reset();

    i = 0;
    do
    {
        bt_rst();
        DelayMs(1000);                                              // No delay for mux so add one here
        bt_stat = query_call();
        i++;
    } while ((bt_stat != 0x31) && (bt_stat != 0x33) && (i < 3));    // Ensure BT module reset to connectable or connected but give up after 3 goes

    Florida_Reset();                                                // 1s needed between USB reset and Florida reset but BT reset takes more than that

    while ((Florida_Read(0x0d23)& 0x100) == 0);                     // Wait for boot sequence to complete

    loadConfigandDSP();
//    if (loadConfigandDSP() == -1)                                   // Load scripts and firmware into florida
//    {
//        printf("\n\rOld parameter file detected - give option to wipe EEPROM");
//        param_rev = -1;
//        goto re_start;
//    }

    LED_PON(ON);                                                    // Initialise LEDs off
    LED_PROCESS(OFF);
    LED_CHG(OFF);
    LED_MODE_ORG(OFF);
    LED_MODE_GRN(OFF);

    InitInt();                                                      // Initialise interrupts after all scripts loaded
    nv_read();                                                      // Read non-volitile memory for stored settings;
    adjustIPVolume(VOL_INIT);                                       // Set stored volume level

    Florida_Write_Reg(ALGO_EN, 0x01);                               // Enable DSP4 algorithm for KAP operation
    Florida_Write_Reg(DECODER_EN, 0x01);                            // Enable MBC decoder
    Florida_Write_Reg(SPATIAL_REG, 0x01);                           // Enable Spatial component of KAP
    Florida_Write_Reg(0x400,0xff);                                  // Enable all hp & skr outputs
    Florida_Write_Reg(0x519,0x03);                                  // Enable AIF1 TX for speaker 9 & 10
    Florida_Write_Reg(0x559,0x03);                                  // Enable AIF2 TX for sub and line-out
    Florida_Write_Reg(0x0c0f, 0x0000);                              // Set interrupt to initialise ISR and check headset status

    CNEN2bits.CN20IE = 0;                                           // Disable BT_EVENT interrupt
    re_pairing_mode();                                              // Pair to last device

    startupBanner();                                                // Display startup info
    printf("\n\rComhear Beaming rev %02x%04x", Florida_Read(DSP1_REVISION_MAJOR), Florida_Read(DSP1_REVISION_MINOR));
    printf("\n\rComhear KAP rev %02x%04x", Florida_Read(DSP4_REVISION_MAJOR), Florida_Read(DSP4_REVISION_MINOR));
    printf("\n\rBeaming Filter 1 - %s", filter1_info);
    printf("\n\rBeaming Filter 2 - %s", filter2_info);
    printf("\n\rConfiguration script rev %04x", config_rev);
    printf("\n\rParameter script rev %04x", param_rev);
    printf("\n\rNORMAL OPERATION\r\n");

    DelayMs(2000);
    CNEN2bits.CN20IE = 1;                                           // Enable BT_EVENT interrupt after re-pair settled

    while(1)                                                        // Main process loop - should never exit
    {
        if (dev_mode)                                               // Go into development mode if flag set by key interrupt routine
            developmentMode();

        if (mode_change)                                            // Change mode if flag set by command interrupt
        {
            SetMode();
            mode_change = FALSE;
        }

        if (key_code)
        {
            switch (key_code)
            {
                case 1:
                   CNEN2bits.CN20IE = 0;                            // Disable BT_EVENT interrupt
                   if ((DSP_Status & 0x10) == 0x10)                 // If Call flag set end it
                   {
                       printf("\n\rEnd Call");
                       end_call();
                   }
                   else if (incoming)                               // If Incoming Call answer it
                   {
                       printf("\n\rAnswer Call");
                       answer_call();
                       incoming = FALSE;                            // Clear incoming call flag
                   }
                   else
                   {
                       printf("\n\rAnswer/End button pressed");
                   }

                   CNEN2bits.CN20IE = 1;                            // Enable BT_EVENT interrupt
                   break;
                case 2:
                   printf("\n\rPairing Mode");
                   CNEN2bits.CN20IE = 0;                            // Disable BT_EVENT interrupt
                   LED_PROCESS(ON);
                   pairing_mode();                                  // Put BT module in pairing mode
                   CNEN2bits.CN20IE = 1;                            // Enable BT_EVENT interrupt
                   break;
                case 3:
                    printf("\n\rVolume up");
                    adjustIPVolume(VOL_UP);
                    break;
                case 4:
                    printf("\n\rVolume down");
                    adjustIPVolume(VOL_DOWN);
                    break;
                case 5:
                    printf("\n\rMode button pressed");
                    if (((DSP_Status & 0x03) != 0x02) && !(auto_nman)) // If headset not inserted and in manual operation cycle TBB, WDB BAR
                    {
                        if (bar_mode)
                        {
                            if (DSP_Status == 0x01)                 // if Beam no kap change to KAP on
                                DSP_Status = 0x09;
                            else if (DSP_Status == 0x09)            // if Beam, KAP change to Bar, no KAP
                                DSP_Status = 0x03;
                            else if (DSP_Status == 0x03)            // if Bar, no KAP change to Bar, KAP
                                DSP_Status = 0x0b;
                            else if (DSP_Status == 0x0b)            // if Bar, KAP change to WDB
                                DSP_Status = 0x05;
                            else                                    // otherwise assume WDB and change to Beam, no KAP
                                DSP_Status = 0x01;
                        }
                        else
                        {
                            if (DSP_Status == 0x01)                 // if Beam no kap change to KAP on
                                DSP_Status = 0x09;
                            else if (DSP_Status == 0x09)            // if Beam, KAP change to WDB
                                DSP_Status = 0x05;
                            else                                    // otherwise assume WDB and change to Beam, no KAP
                                DSP_Status = 0x01;
                        }
                        SetMode();
                    }
                    break;
                case 6:
                    printf("\n\rProcess button pressed");           // If Process button pressed toggle KAP on and off
                    if ((DSP_Status & 0x08) == 0x08)
                        DSP_Status = (DSP_Status & 0xf7);
                    else
                        DSP_Status = (DSP_Status | 0x08);
                    SetMode();
                    break;
                case 7:
                    if (!(auto_nman))                               // If both volume buttons pressed toggle mode between Auto and Manual
                    {
                        auto_nman = TRUE;
                        printf("\n\rChange to AUTO mode TBB");
                        DSP_Status = 0x01;                          // Always reset Auto mode to mode TBB
                        SetMode();
                    }
                    else
                    {
                        auto_nman = FALSE;
                        printf("\n\rChange to MANUAL mode");
                    }
                    break;
                default:                                            // No valid key code
                    break;
            }
            key_code = 0;
        }

        if (bt_req)                                                 // If flag set by BT ISR
        {
            CNEN2bits.CN20IE = 0;                                   // Disable BT_EVENT interrupt
            CNEN2bits.CN19IE = 0;                                   // Disable HEADIN interrupt
            CNEN3bits.CN47IE = 0;                                   // Disable IRQ_8280 interrupt to prevent interference with BT query call
            bt_stat = query_call();

            switch (bt_stat)
            {
                case 0x44:                                          // ASCII "D" indicates streaming mode
                    printf("\n\rPair LED on");
                    LED_PROCESS(ON);
                    Florida_Write_Reg(0x0A00, 0x0000);              // Disconnect Line In Left
                    Florida_Write_Reg(0x0A02, 0x0014);              // Reconnect BT In Left
                    Florida_Write_Reg(0x0A08, 0x0000);              // Disconnect Line In Right
                    Florida_Write_Reg(0x0A0A, 0x0015);              // Reconnect BT In Right
                    Florida_Write_Reg(0x0300, 0x0033);              // Disable mic inputs
                    printf("\n\rDisable Microphone Inputs to stop feedback via bluetooth module in streaming mode");
                    if ((DSP_Status & 0x10) == 0x10)                // If Call flag set
                    {
                        DSP_Status &= 0xef;                         // Clear Call flag
                        SetMode();
                    }
                    break;
                case 0x36:                                          // ASCII "6" indicates call mode
                    Florida_Write_Reg(0x0A00, 0x0000);              // Disconnect Line In Left
                    Florida_Write_Reg(0x0A02, 0x0014);              // Reconnect BT In Left
                    Florida_Write_Reg(0x0A08, 0x0000);              // Disconnect Line In Right
                    Florida_Write_Reg(0x0A0A, 0x0015);              // Reconnect BT In Right
                    DSP_Status |= 0x10;                             // Set Call flag
                    if ((DSP_Status & 0x03) == 0x02)                // If headphone inserted
                    {
                        Florida_Write_Reg(0x0769, 0x0080);          // 0dB
                        printf("\n\r0dB on Line Out for headset call");
                    }
                    else
                    {
                        Florida_Write_Reg(0x0769, 0x0060);          // -16dB
                        printf("\n\r-16dB on Line Out optimised for array call");
                    }
                    Florida_Write_Reg(0x0300, 0x003f);              // Enable mic inputs as BT verfied in valid state

                    SetMode();
                    break;
                case 0x31:                                          // ASCII "1" indicates connectable
                    printf("\n\rPair LED off");
                    LED_PROCESS(OFF);
                    Florida_Write_Reg(0x0300, 0x003f);              // Enable mic inputs as BT verfied in valid state
                    break;
                case 0x35:                                          // ASCII "5" indicates incoming call request mode
                    incoming = TRUE;                                // Set incoming call flag
                    break;
                case 0x33:                                          // ASCII "3" indicates connected idle mode
                case 0x34:                                          // ASCII "4" indicates outgoing call request mode
                case 0x43:                                          // ASCII "C" indicates active call in handset but not headset
                    printf("\n\rPair LED on");
                    LED_PROCESS(ON);
                    Florida_Write_Reg(0x0A00, 0x0010);              // Reconnect Line In Left
                    Florida_Write_Reg(0x0A02, 0x0000);              // Disconnect BT In Left
                    Florida_Write_Reg(0x0A08, 0x0011);              // Reconnect Line In Right
                    Florida_Write_Reg(0x0A0A, 0x0000);              // Disconnect BT In Right
                    Florida_Write_Reg(0x0769, 0x0080);              // 0dB for line out connection
                    Florida_Write_Reg(0x0300, 0x003f);              // Enable mic inputs as BT verfied in valid state
                    printf("\n\r0dB on Line Out for normal operation");
                    if ((DSP_Status & 0x10) == 0x10)                // If Call flag set
                    {
                        DSP_Status &= 0xef;
                        SetMode();
                    }
                    break;
                case 0x30:                                          // ASCII "0" Limbo - logically off but physically on
                default:                                            // Any other value is fault condition
                    AMPMUTE(LOW);                                   // Mute output to remove any re-connect noise
                    Florida_Write_Reg(0x0400, 0x003f);              // Mute 8281 speaker outputs
                    bt_rst();
                    DelayMs(500);                                   // No delay for mux so add one here
                    bt_stat = query_call();
                    DelayMs(500);
                    re_pairing_mode();
                    DelayMs(3000);                                  // Let re-par complete before any interrupts allowed
                    AMPMUTE(HIGH);
                    Florida_Write_Reg(0x0400, 0x00ff);              // Enable 8281 speaker outputs
            }
            bt_req = FALSE;                                         // Reset interrupt flag
            CNEN3bits.CN47IE = 1;                                   // Enable IRQ_8280 interrupt
            CNEN2bits.CN19IE = 1;                                   // Enable HEADIN interrupt
            CNEN2bits.CN20IE = 1;                                   // Enable BT_EVENT interrupt
        }

        DelayMs(1);
        if (watchdog_en)
        {
            loop++;
            if (loop == 60000)                                      // Watchdog polling delay set to approx 60s
            {
                loop = 0;
                if (Florida_Read(DSP1_WATCHDOG))                    // Check that DSP1 has set watchdog bit and reset value or restart system if not
                    Florida_Write_Reg(DSP1_WATCHDOG, 0x00);
                else
                {
                    printf("\n\rDSP1 WATCHDOG FAILURE !!\r\n");
                    asm("reset");
                }
                if (Florida_Read(DSP2_WATCHDOG))                    // Check that DSP2 has set watchdog bit and reset value or restart system if not
                    Florida_Write_Reg(DSP2_WATCHDOG, 0x00);
                else
                {
                    printf("\n\rDSP2 WATCHDOG FAILURE !!\r\n");
                    asm("reset");
                }
                if (Florida_Read(DSP4_WATCHDOG))                    // Check that DSP4 has set watchdog bit and reset value or restart system if not
                    Florida_Write_Reg(DSP4_WATCHDOG, 0x00);
                else
                {
                    printf("\n\rDSP4 WATCHDOG FAILURE !!\r\n");
                    asm("reset");
                }
            }
        }
    }

    for(;;);
    return (0);                                                     // Point never reached - put here to avoid "control reaches end of non-void function" warning
}
//*****************************************************************************
//* End of Main Program - start of functions                                  *
//*****************************************************************************


void startupBanner(void)
{
    printf("\f\n\rMyBeam 10 Channel Binaural Beamer");
    printf("\n\rCopyright 2015 ComHear, Inc - All rights reserved");
    printf("\n\rPIC Firmware rev %s",SW_VERSION);
    printf("\n\rBuilt %s\t%s",__DATE__,__TIME__);
}


void writeFloridaConfig(void)
{
    FRESULT st;
    FILINFO fno;
    int result = 0;
    
    st = f_stat("config.txt", &fno);
    if (st == FR_OK)
    {
        st = f_open(&fdst, "config.txt", FA_OPEN_EXISTING | FA_READ);
        if (st != FR_OK)
        {
            print_fresult(st);
            result = 1;
        }
        else
        {
            printf("\n\rLoading config.txt\n\r");
            load_script();
        }
    }
    else
    {
        print_fresult(st);
        result = 1;
    }

    if(result != 0)                                                   // No default config on filesystem
        printf("Could not find/load/use filesystem config file.\r\n");
}


void writeFloridaFilter(DWORD Address, char filename[10])
{
    FRESULT st;
    FILINFO fno;

    st = f_stat(filename, &fno);
    if (st == FR_OK)
    {
        st = f_open(&fdst, filename, FA_OPEN_EXISTING | FA_READ);
        if (st != FR_OK)
        {
            printf("\n\rFailed to open filter coefficient file [%s]...\n\rQuitting\r\n", filename);
        }
        else
        {
            printf("Loading filter file %s\n\r", filename);
            load_filter(Address);
            f_close(&fdst);
        }
    }
    else
        printf("Could not find filter coefficient file %s\n\r", filename);
}


void writeFloridaTone(DWORD Address, char filename[10])
{
    FRESULT st;
    FILINFO fno;

    st = f_stat(filename, &fno);
    if (st == FR_OK)
    {
        st = f_open(&fdst, filename, FA_OPEN_EXISTING | FA_READ);
        if (st != FR_OK)
        {
            printf("\n\rFailed to open tone file [%s]...\n\rQuitting\r\n", filename);
        }
        else
        {
            printf("Loading tone wav file %s\n\r", filename);
            load_tone(Address);
            f_close(&fdst);
        }
    }
    else
        printf("Could not find tone wav file %s\n\r", filename);
}


BYTE writeFloridaParameter(void)
{
    FRESULT st;
    FILINFO fno;
    int result = 0;

    st = f_stat("parameter.txt", &fno);
    if (st == FR_OK)
    {
        st = f_open(&fdst, "parameter.txt", FA_OPEN_EXISTING | FA_READ);
        if (st != FR_OK)
        {
            print_fresult(st);
            result = 1;
        }
        else
        {
            printf("Loading Parameter.txt\r\n");
            load_script();
        }
    }
    else
    {
        print_fresult(st);
        result = 1;
    }

    if(result != 0)                                                   // No parameter file found
	{
        printf("Could not find/load/use filesystem parameter file.\r\n");
		return(-1);
	}
	else
		return(1);
}


// Attempts to load all four DSP cores of the WM8280 if there is an image
// for them on the filesystem. Each firmware image should be named in the format:
// fwx.wmfw - where x is the core that the image should be loaded to, e.g
// fw1.wmfw -> DSP Core 1, fw2.wmfw -> DSP Core 2, etc
// It is the responsibility to make sure the image is suitable for the core
// which its name targets it at. No validation is performed to ensure the
// image is valid or will fit the core.
BYTE loadCores(void)
{
    int core;
    BYTE dspCoreLoadedFlags = 0;
    FRESULT st;
    FILINFO fno;
    char filename[9];
    for (core = 1; core <= 4; core++)
    {
        sprintf(filename, "fw%d.wmfw", core);
        st = f_stat(filename, &fno);
        if (st == FR_OK)
        {
            st = f_open(&fdst, filename, FA_OPEN_EXISTING | FA_READ);
            if (st != FR_OK)
            {
                printf("\n\rFailed to open DSP image file %d - [%s]...\n\rQuitting\r\n",
                       core, filename);
            }
            else
            {
                printf("Loading firmware file fw%d.wmfw\n\r", core);
                load_firmware(core, f_size(&fdst), 0);              // Load firmware only
                f_close(&fdst);
                dspCoreLoadedFlags |= (1 << (core - 1));
            }
        }
        if ((core < 3) && (fw_header_en))
        {
            printf("Loading firmware secure block %d\n\r", core);
            load_fw_header(core);
        }
    }
    return (dspCoreLoadedFlags);
}


int loadConfigandDSP(void)                                          // Returns 0 or success, non-zero on failure
{
    int result = 0;
    int temp;
    BYTE dspCoresLoaded = 0;
    BYTE param;

    writeFloridaConfig();
    dspCoresLoaded = loadCores();
    writeFloridaFilter(FILTER_START_1,"flt1.cflt");
    writeFloridaFilter(FILTER_START_2,"flt2.cflt");
    param = writeFloridaParameter();

    if ((param_rev < PARAM_LIMIT) && (param == 1))
    {
        result = -1;
        goto Abort_load;
    }
    
    Florida_Write_Reg(0x0d0f, 0x0001);                              // Mask Florida output interrupts
    Florida_Write_Reg(0x0c0f, 0x0400);                              // IRQ/ = CMOS, Active Low
                                                                    // Check which cores were loaded and start them but do not change sample rate
    if(dspCoresLoaded & 0x01)                                       // Core 1
    {
        temp = (Florida_Read(DSP1_CONTROL_1) & DSP_CONTROL_START_MASK);
        Florida_Write_Reg(DSP1_CONTROL_1, (temp  | DSP_CONTROL_START));
        Florida_Write_Reg(DSP1_CONTROL_1, (temp  | DSP_CONTROL_START));
    }
    if(dspCoresLoaded & 0x02)                                       // Core 2
    {
        temp = (Florida_Read(DSP2_CONTROL_1) & DSP_CONTROL_START_MASK);
        Florida_Write_Reg(DSP2_CONTROL_1, (temp  | DSP_CONTROL_START));
        Florida_Write_Reg(DSP2_CONTROL_1, (temp  | DSP_CONTROL_START));
    }
    if(dspCoresLoaded & 0x04)                                       // Core 3
    {
        temp = (Florida_Read(DSP3_CONTROL_1) & DSP_CONTROL_START_MASK);
        Florida_Write_Reg(DSP3_CONTROL_1, (temp  | DSP_CONTROL_START));
        Florida_Write_Reg(DSP3_CONTROL_1, (temp  | DSP_CONTROL_START));
    }
    if(dspCoresLoaded & 0x08)                                       // Core 4
    {
        temp = (Florida_Read(DSP4_CONTROL_1) & DSP_CONTROL_START_MASK);
        Florida_Write_Reg(DSP4_CONTROL_1, (temp  | DSP_CONTROL_START));
        Florida_Write_Reg(DSP4_CONTROL_1, (temp  | DSP_CONTROL_START));
    }

    DelayMs(200);
    DACMUTE(HIGH);                                                  // Enable DAC output
    AMPMUTE(HIGH);                                                  // Enable AMP output

Abort_load:
    return (result);
}


int createFilesystem(void)                                          // Init SPI Flash EEPROM and install filesystem
{
    FRESULT st;
    int result = 0;

    EEPROMEraseChip();
    EEPROM_wait_for_write_complete();

    st = f_mount(0, &fs[0]);
    if (FR_OK == st)
    {
        printf("System Mounted\n\r");
    }
    else
    {
        printf("Error ");
        print_fresult(st);
        printf(", can not Mount system on Flash Memory\n\r");
        result = 1;
    }

    if (result == 0)
    {
        st = f_mkfs(0);                                             // Create an FAT volume on the logical drive 0. 2nd argument is ignored.
        if (FR_OK == st)
        {
            printf("File System Installed on Flash Memory\n\r");
            st = f_mount(0, &fs[0]);
            if (FR_OK == st)
            {
                printf("System Mounted\n\r");
                result = 0;
            }
        }
        else
        {
            printf("Can not install File System on Flash Memory\n\r");
            result = 2;
        }
    }
    else
        printf("\n\rWas unable to mount filesystem and run MKFS\r\n");

    return (result);
}

 
int checkValidFilesystem(void)                                      // Checks if a valid filesystem exists on the flash (0 == Valid, non-zero == invalid)
{
    int result = 0;

    disk_read(dump_buff, 0);                                        // Read directory sector
    if (strncmp("COMHEAR",(const char *) &dump_buff[0], 7) != 0)    // Check for our signature to show filesystem initialised
    {
        result = 1;
        printf("\n\rINCORRECT FILESYSTEM ID\r\n");
    }
    return (result);
}


void print_fresult(int rc)                                          // Translate filesystem error codes into meaningful text and print them
{
    switch (rc)
    {
    case 0: printf("%s\n\r", "OK");
        break;
    case 1: printf("%s\n\r", "DISK_ERR");
        break;
    case 2: printf("%s\n\r", "INT_ERR");
        break;
    case 3: printf("%s\n\r", "NOT_READY");
        break;
    case 4: printf("%s\n\r", "NO_FILE");
        break;
    case 5: printf("%s\n\r", "NO_PATH");
        break;
    case 6: printf("%s\n\r", "INVALID_NAME");
        break;
    case 7: printf("%s\n\r", "DENIED");
        break;
    case 8: printf("%s\n\r", "EXIST");
        break;
    case 9: printf("%s\n\r", "INVALID_OBJECT");
        break;
    case 10: printf("%s\n\r", "WRITE_PROTECTED");
        break;
    case 11: printf("%s\n\r", "INVALID_DRIVE");
        break;
    case 12: printf("%s\n\r", "NOT_ENABLED");
        break;
    case 13: printf("%s\n\r", "NO_FILE_SYSTEM");
        break;
    case 14: printf("%s\n\r", "MKFS_ABORTED");
        break;
    case 15: printf("%s\n\r", "TIMEOUT");
        break;
    case 16: printf("%s\n\r", "LOCKED");
        break;
    case 17: printf("%s\n\r", "NOT_ENOUGH_CORE");
        break;
    case 18: printf("%s\n\r", "TOO_MANY_OPEN_FILES");
        break;
    case 19: printf("%s\n\r", "INVALID_PARAMETER");
        break;
    default: printf("%s\n\r", "INVALID_PARAMETER");
    };
}


void developmentMode(void)
{
    unsigned int i = 0;
    int data = 0;
    int x = 0;

    CNEN2bits.CN20IE = 0;                                           // Disable BT_EVENT interrupt
    CNEN3bits.CN47IE = 0;                                           // Disable IRQ_8280 interrupt
 
    printf("\n\r*** DEVELOPMENT MODE - Locked ***\r\n");
    printf("\n\rEnter password to proceed\r\n");

    while (1)
    {
        for (i = 0; i < (sizeof (cli_buffer) / sizeof (cli_buffer[0])); i++) // Clear command buffer
        {
            cli_buffer[i] = 0;
        }
        printf("\n\rCH>");

        i = 0;
        break_flag = 0;
        second_user_buffer[0] = '/';
        second_user_buffer[1] = NUL;
        for (;;)
        {
            if (break_flag != 2)
            {
                data = getUartTerm();
            }
            else
            {
                break_flag = 0;                                     // Break_flag was 2 now reset it back
            }
            if (data > 0x1f || data == CR)                          // Printable character OR <CR> place character in the buffer
            {
                cli_buffer[i] = data;
                putch2(data);                                       // Echo back to print on terminal

                if (data == CR)                                     // Got one complete line
                {
                    break;
                }
                if (i < 128)
                    i++;
            }
            if (data == BS)
            {
                if (i > 0)
                {
                    i--;
                    putch2(BS);                                     // Move cursor to the left by one
                    putch2(' ');                                    // Blank out character
                    putch2(BS);                                     // Move cursor to the left by one
                    cli_buffer[i] = ' ';                            // Remove last character in buffer
                }
            }
            if (data == ESC)
            {
                while (1)
                {
                    data = getcht(100);                             // Wait for 100 millisec delay

                    if (data < 0)                                   // Timeout, no escape sequence
                    {
                        break_flag = 3;                             // Escape key pressed
                        break;
                    }

                    if (data == 0x5B)                               // '[' character, second part of CSI
                    {
                        data = getUartTerm();

                        if (data == 0x41)                           // Up arrow
                        {
                            if (scroll_line == 0)
                            {
                                scroll_line = 3;
                            }
                            else
                            {
                                scroll_line--;
                            }

                            putch2(0x1b);
                            putch2(0x5b);
                            putch2('6');
                            putch2('n');

                            x = 0;

                            do                                      // Get cursor position
                            {
                                data = getUartTerm();
                                cursor_pos[x++] = data;

                            }
                            while ((data != 'R') && (x < 10));

                            putch2(0x1b);                           // Move cursor to row and col position
                            putch2(0x5b);
                            putch2(cursor_pos[2]);

                            if (cursor_pos[3] != ';')
                                putch2(cursor_pos[3]);

                            putch2(';');
                            putch2('1');
                            putch2('H');

                            putch2(0x1b);                           // Clear the line
                            putch2(0x5b);
                            putch2('2');
                            putch2('K');

                            putch2(0x1b);                           // Move cursor to row and col position
                            putch2(0x5b);
                            putch2(cursor_pos[2]);

                            if (cursor_pos[3] != ';')
                                putch2(cursor_pos[3]);

                            putch2(';');
                            putch2('1');
                            putch2('H');

                            printf("CH>");

                            printf("%s", scroll_buffer[scroll_line]);

                            data = getUartTerm();

                            if (data == CR)                         // Scroll buffer line selected to execute
                            {
                                for (x = 0; x < 32; x++)            // Copy scroll_buffer into cli_buffer
                                {
                                    data = scroll_buffer[scroll_line][x];

                                    if (data == NUL)
                                    {
                                        cli_buffer[x] = CR;
                                        break_flag = 1;
                                        break;
                                    }
                                    cli_buffer[x] = data;
                                }
                            }
                            else
                            {
                                if (data == ESC)                    // History on scroll buffer not selected, return back to the DOS prompt OR scroll key pressed to rotate and display scroll buffer
                                    continue;
                                else
                                {
                                    break_flag = 2;
                                    break;
                                }
                            }
                        }

                    }
                    break;
                }
            }
            if (break_flag == 1 || break_flag == 3)                 // Execute the command or abort scroll buffer selection because escape key pressed
                break;
        }

        if (cli_buffer[0] == CR || break_flag == 3)
            continue;
        else
        {
            printf("\n\r");

            for (i = 0; i < 32; i++)                                // Copy cli_buffer into scroll_buffer
            {
                data = cli_buffer[i];

                if (data == CR)
                {
                    scroll_buffer[scroll_line][i] = NUL;
                    break;
                }
                scroll_buffer[scroll_line][i] = data;
            }

            if (scroll_line >= 3)
                scroll_line = 0;
            else
                scroll_line++;

            cli();                                                  // Decode the command in cli_buffer and execute
        }
    }
}

void launch_bootldr()
{
    set_launch_bootloader(1);
    asm("goto %0" : : "r"(0x0));
}

void cli(void)
{
    BYTE *pch, *pch1;
    FRESULT st;
    long check;
    int sec_num,level,channel,mod;
    FILINFO fno;
    BYTE ext;
    DWORD address;
    WORD number, data, index;
    WORD reg0, reg1;
    unsigned int i;

    pch = (unsigned char *) strtok((char *) cli_buffer, "\n\r ");   // Read keyboard buffer
    pch1 = NULL;                                                    // Default initialisation

    if (password_valid == FALSE)
    {
        if (strncmp("please",(const char *) pch, 6) == 0)
        {
            password_valid = TRUE;
            printf("\n\rPassword accepted. Development Mode Unlocked\r\n");
            return;
        }
        else if (strncmp(".",(const char *) pch, 1) == 0)
        {
            adjustIPVolume(VOL_UP);
            return;
        }
        else if (strncmp(",",(const char *) pch, 1) == 0)
        {
            adjustIPVolume(VOL_DOWN);
            return;
        }
        else if (strncmp("=",(const char *) pch, 1) == 0)
        {
            adjustIPVolume(VOL_RESET);
            return;
        }
        else
        {
            printf("\n\rIncorrect Password.\r\n");
            return;
        }
    }

    while (1)
    {
        if ((strncmp("about",(const char *) pch, 5) == 0) || (strncmp("ver",(const char *) pch, 3) == 0)) // Show startup banner
        {
            startupBanner();
            reg0 = Florida_Read(0);
            reg1 = Florida_Read(1);
            printf("\n\rComhear Beaming rev %02x%04x", Florida_Read(DSP1_REVISION_MAJOR), Florida_Read(DSP1_REVISION_MINOR));
            printf("\n\rComhear KAP rev %02x%04x", Florida_Read(DSP4_REVISION_MAJOR), Florida_Read(DSP4_REVISION_MINOR));
            printf("\n\rAudio Device ID = 0x%04x, revision 0x%04x", reg0, reg1);
            DelayMs(40);
            break;
        }

        if (strncmp("cfg_florida",(const char *) pch, 11) == 0)     // Load cfg
        {
            writeFloridaConfig();
            DACMUTE(LOW);                                           // Enable DAC output
            AMPMUTE(HIGH);                                          // Enable AMP output
            break;
        }

        if (strncmp("toneon",(const char *) pch, 6) == 0)           // Tone generator 1 on
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");          // argument 1 = channel 1- 12
            pch1 = (unsigned char *) strtok(NULL, "\n\r ");         // argument 2 = level 1 - 32 -1dB - -32dB)
            channel = atoi((const TCHAR *) pch);
            level = atoi((const TCHAR *) pch1);
            if ((channel <= 0) || (channel > 12))
                channel = 1;
            address = 0x678 + (channel * 8) + ((channel > 6) * 0x90); // Calculate register address for channel output
            if (level <= 0)
                level = 20;                                         // Default level -20dB
            if (level > 32)
                level = 32;                                         // Minimum level -32dB

            printf("Testing Channel %i level -%idB\n\r", channel, level);
            Florida_Write_Reg(0x880, 0x04);                         // Route tone output to EQ1 ( to allow attenuation in the mixer
            Florida_Write_Reg(0x881, (0x80 - (level * 2)));         // Set mixer attenuation
            Florida_Write_Reg(0xe10, 0x6319);                       // Enable EQ1
            number = Florida_Read(0x768);                           // Store Line Out connection address
            Florida_Write_Reg(0x768, 0x0000);                       // Disconnect Line Out to prevent pickup of speaker test tones
            data = Florida_Read(address);                           // Store channel connection address
            Florida_Write_Reg(address, 0x0050);                     // Connect tone to channel
            test_tone();
            Florida_Write_Reg(address, data);                       // Reconnect channel
            Florida_Write_Reg(0x768, number);                       // Reconnect Line Out
            break;
        }

        if( (strncmp("?",(const char *) pch, 1) == 0) || (strncmp("help",(const char *) pch,4)==0) ) // List help screen
        {
            help_func();
            break;
        }

        if (strncmp("dir",(const char *) pch, 3) == 0)              // List directory
        {
            for (i=0; i < 256; i++)                                 // Restore user buffer to state before script download for dir command to work
                user_buffer[i] = user_buffer_backup[i];
            scan_files((char *) user_buffer);
            break;
        }

        if (strncmp("del",(const char *) pch, 3) == 0)              // Delete a file
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");
            st = f_unlink((const TCHAR *) pch);
            if (st == FR_OK)
            {
                printf("\n\rFile %s deleted\n\r", pch);
            }
            else
            {
                printf("\n\rCan not delete file %s\n\r", pch);
            }
            break;
        }

        if (strncmp("ds",(const char *) pch, 2) == 0)               // Dump a sector
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");
            sec_num = atoi((const TCHAR *) pch);                    // Get optional sector number to dump from
            dump_sector(sec_num);
            break;
        }

        if (strncmp("xfer",(const char *) pch, 4) == 0)             // Transfer files from PC to SPI Flash memory
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");          // filename.ext

            max_chars = 0;                                          // Check if buffer overflows (1024) when using xfer

            st = f_stat((const TCHAR *) pch, &fno);                 // Check if file already exists in file system
            if (FR_OK == st)
            {
                printf("File %s exists, overwrite (y / n) ", pch);

                if (getUartTerm() != 'y')
                {
                    break;
                }
            }

            st = f_open(&fdst,(const TCHAR *) pch, FA_CREATE_ALWAYS | FA_WRITE);
            if (FR_OK != st)
            {
                print_fresult(st);
                break;
            }

            printf("\n\rSend data, Transfer->Send File using Xmodem Protocol, from Terminal now...\n\r");

            check = xmodemReceive(get_extension(pch));              // Check file_type:- 0 = .txt, 1 = .bat, 2 = .wmfm, 3 = .cflt, 4 = .beep

            f_close(&fdst);                                         // Close open files

            if (check < 0)
            {
                printf("\n\rXmodem receive error: status: %ld\r\n", check);
            }
            else
            {
                printf("\n\rXmodem successfully received %ld bytes\r\n", check);
            }
            printf("max_chars = %d\n\r", max_chars);
            break;
        }

        if (strncmp("florida",(const char *) pch, 7) == 0)          // Test spi interface to Florida
        {
            test_spi_to_Florida();
            break;
        }

        if (strncmp("reset_florida",(const char *) pch, 13) == 0) // Reset Florida
        {
            Florida_Reset();
            break;
        }

        if (strncmp("load",(const char *) pch, 4) == 0)             // Transfer file from SPI Flash to either SPI Florida or iic Codec
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");          // Parse into two arguments
            pch1 = (unsigned char *) strtok(NULL, "\n\r ");

            if (pch1 == NULL)                                       // One arg: pch should be a filename
            {
                st = f_open(&fdst,(const TCHAR *) pch, FA_OPEN_EXISTING | FA_READ);
                ext = get_extension(pch);
            }
            else                                                    // Two arg: first one should be "dspx" (where x is 1 to 4) or "codec", second should be a file name
            {
                st = f_open(&fdst,(const TCHAR *) pch1, FA_OPEN_EXISTING | FA_READ);
                ext = get_extension(pch1);
            }

            if (FR_OK != st)
            {
                print_fresult(st);
                f_close(&fdst);
                break;
            }

            if (ext == 0 && pch1 == NULL)                           // One arg .txt (florida, default)
            {
                printf("\n\rScript loading not supported!\n\r");
                break;
            }
            if (ext == 1 && pch1 == NULL)                           // One arg .bat
            {
                printf("\n\rBatch file loading not supported!\n\r");
                break;
            }
            if (((ext == 2) || (ext == 3) || (ext == 4)) && pch1 == NULL)  // One arg .wmfw or .cflt or .beep
            {
                printf("\n\rInvalid parameters!\n\r");
                break;
            }
            if (((ext == 0) || (ext ==1)) && pch1 != NULL)          // Two arg .txt or .bat
            {
                printf("\n\rInvalid parameters!\n\r");
                break;
            }

            if (ext == 2 && pch1 != NULL)                           // Two arg .wmfw
            {
                if (strncmp("dsp1",(const char *) pch, 4) == 0)
                {
                    load_firmware(1, f_size(&fdst), 0);
                    break;
                }

                if (strncmp("dsp2",(const char *) pch, 4) == 0)
                {
                    load_firmware(2, f_size(&fdst), 0);
                    break;
                }

                if (strncmp("dsp3",(const char *) pch, 4) == 0)
                {
                    load_firmware(3, f_size(&fdst), 0);
                    break;
                }

                if (strncmp("dsp4",(const char *) pch, 4) == 0)
                {
                    load_firmware(4, f_size(&fdst), 0);
                    break;
                }
            }
            if (ext == 3 && pch1 != NULL)                           // Two arg .cflt
            {
                if (strncmp("dsp1",(const char *) pch, 4) == 0)
                {
                    writeFloridaFilter(FILTER_START_1, (char *) pch1);
                    break;
                }

                if (strncmp("dsp2",(const char *) pch, 4) == 0)
                {
                    writeFloridaFilter(FILTER_START_2, (char *) pch1);
                    break;
                }
            }
            if (ext == 4 && pch1 != NULL)                           // Two arg .beep
            {
                if (strncmp("dsp4",(const char *) pch, 4) == 0)
                {
                    writeFloridaTone(TONE_START_1, (char *) pch1);
                    break;
                }

                if (strncmp("dsp4",(const char *) pch, 4) == 0)
                {
                    writeFloridaTone(TONE_START_2, (char *) pch1);
                    break;
                }
            }
            printf("command error\n\r");
            break;
        }


        if (strncmp("verify",(const char *) pch, 6) == 0)           // Compare data from Florida with file in SPI memory.
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");          // Dsp core
            pch1 = (unsigned char *) strtok(NULL, "\n\r ");         // Filename

            if (pch1 != NULL)
            {
                st = f_open(&fdst,(const TCHAR *) pch1, FA_OPEN_EXISTING | FA_READ);
                if (FR_OK != st)
                {
                    print_fresult(st);
                    f_close(&fdst);
                    break;
                }

                ext = get_extension(pch1);

                if (ext == 2)                                       // Check for .wmfw
                {
                    if (strncmp("dsp1",(const char *) pch, 4) == 0)
                    {
                        load_firmware(1, f_size(&fdst), 1);
                        break;
                    }

                    if (strncmp("dsp2",(const char *) pch, 4) == 0)
                    {
                        load_firmware(2, f_size(&fdst), 1);
                        break;
                    }

                    if (strncmp("dsp3",(const char *) pch, 4) == 0)
                    {
                        load_firmware(3, f_size(&fdst), 1);
                        break;
                    }

                    if (strncmp("dsp4",(const char *) pch, 4) == 0)
                    {
                        load_firmware(4, f_size(&fdst), 1);
                        break;
                    }
                }
            }
            printf("command error\n\r");
            break;
        }


        if (strncmp("read",(const char *) pch, 4) == 0)             // Read Florida data from given start address
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");          // Address in hex
            number = (WORD) strtoul((const char *) pch1, NULL, 10);
            address = strtoul((const char *) pch, NULL, 16);        // Get the address
            printf("\n\rReading from address [%ld] == 0x%04x\r\n", address, Florida_Read(address));
            break;
        }


        if (strncmp("write",(const char *) pch, 5) == 0)            // Write data to 16-bit address in Florida
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");          // Address in hex
            pch1 = (unsigned char *) strtok(NULL, "\n\r ");         // Data in hex
            data = (WORD) strtoul((const char *) pch1, NULL, 16);
            address = strtoul((const char *) pch, NULL, 16);        // Get the address
            Florida_Write_Reg(address, data);
            break;
        }


        if (strncmp("type",(const char *) pch, 4) == 0)             // Print contents of file to screen
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");
            st = f_open(&fdst,(const TCHAR *) pch, FA_OPEN_EXISTING | FA_READ);

            if (FR_OK != st)
            {
                print_fresult(st);
                break;
            }

            type_command(get_extension(pch));                       // File_type:- 0 = .txt, 1 = .bat, 2 = .wmfw, 3 = .cflt, 4 = .beep

            f_close(&fdst);                                         // Close open files
            break;
        }


        if (strncmp("MKFS",(const char *) pch, 4) == 0)             // Place File System on Flash memory and initialise Bluetooth NV memory
        {
            printf("\n\rErasing Flash Memory\n\r");

            EEPROMEraseChip();

            EEPROM_wait_for_write_complete();

            printf("Mounting system\n\r");

            st = f_mount(0, &fs[0]);

            if (st == 0)
                printf("System Mounted\n\r");
            else
            {
                printf("Error ");
                print_fresult(st);
                printf(", can not Mount system on Flash Memory\n\r");
                break;
            }

            printf("Installing File System\n\r");

            st = f_mkfs(0);                                         // Create an FAT volume on the logical drive 0. 2nd argument is ignored.

            if (st == 0)
                printf("File System Installed on Flash Memory\n\r");
            else
                printf("Can not install File System on Flash Memory\n\r");

            nv_default();
            bt_default();
            SetBluetooth();
            break;
        }

        if (strncmp("BTS",(const char *) pch, 3) == 0)              // Setup Bluetooth
        {
            SetBluetooth();
            break;
        }

        if (strncmp("BL", (const char*) pch, 2) == 0)
        {
            launch_bootldr();                                       // Doesn't return
            break;
        }

        if (strncmp("BTRST",(const char *) pch, 5) == 0)            // Reset Bluetooth to Default Settings
        {
            bt_default();
            break;
        }

        if (strncmp("BTD",(const char *) pch, 3) == 0)              // Display Bluetooth Settings
        {
            bt_settings();
            break;
        }

        if (strncmp("BTV",(const char *) pch, 3) == 0)              // Display Bluetooth Version
        {
            bt_version();
            break;
        }

        if (strncmp("BTP",(const char *) pch, 3) == 0)              // Bluetooth Pairing Mode
        {
            pairing_mode();
            break;
        }

        if (strncmp("BTM", (const char *) pch, 3) ==0)
        {
            max_level();
            break;
        }

        if (strncmp("BTN", (const char *) pch, 3) ==0)
        {
            norm_level();
            break;
        }

        if (strncmp("Kon",(const char *) pch, 3) == 0)              // KAP On
        {
            Florida_Write_Reg(KAP_REG, 0x01);                       // Set KAP on
            Florida_Write_Reg(SPATIAL_REG, 0x01);
            break;
        }

        if (strncmp("Koff",(const char *) pch, 3) == 0)             // KAP Off
        {
            Florida_Write_Reg(KAP_REG, 0x00);                       // Set KAP off
            Florida_Write_Reg(SPATIAL_REG, 0x00);
            break;
        }

        if (strncmp("mode",(const char *) pch, 4) == 0)             // Change DSP mode
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");
            mod = atoi((const TCHAR *) pch);
            if (mod < 1)
                mod = 1;                                            // Limit input range to 1 - 5
            if (mod > 5)
                mod = 5;
            switch (mod)
            {
                case 1:
                    DSP_Status = 0x01;                              // Beam, no KAP, no headset
                    break;
                case 2:
                    DSP_Status = 0x09;                              // Beam, KAP, no headset
                    break;
                case 3:
                    DSP_Status = 0x03;                              // Bar, no KAP, headset
                    break;
                case 4:
                    DSP_Status = 0x0b;                              // Bar, KAP, headset
                    break;
                case 5:
                    DSP_Status = 0x05;                              // WDB
                    break;
            }
            SetMode();
            break;
        }

        if (strncmp("Reboot",(const char *) pch, 6) == 0)           // Restart system
        {
            asm("reset");
            break;
        }

        if (strncmp("V+",(const char *) pch, 2) == 0)               // Volume Up
        {
            adjustIPVolume(VOL_UP);
            break;
        }

        if (strncmp("V-",(const char *) pch, 2) == 0)               // Volume Down
        {
            adjustIPVolume(VOL_DOWN);
            break;
        }
		
        if (strncmp("VRST",(const char *) pch, 4) == 0)             // Volume Reset
        {
            adjustIPVolume(VOL_RESET);
            break;
        }   
        
        if (strncmp("speaker",(const char *) pch, 7) == 0)          // Play test noise to each speaker
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");
            level = atoi((const TCHAR *) pch);
            if (level <= 0)
                level = 2;                                          // Default level -12dB
            if (level == 21)
                level = 0;                                          // Make 0dB possible but not default
            if (level > 20)
                level = 20;                                         // Minimum level -120dB
            speaker_test(20 - level);                               // Covert level to attenuation register value
            break;
        }

        if (strncmp("phase",(const char *) pch, 5) == 0)            // Play test noise to each speaker
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");
            level = atoi((const TCHAR *) pch);
            if (level <= 0)
                level = 2;                                          // Default level -12dB
            if (level == 21)
                level = 0;                                          // Make 0dB possible but not default
            if (level > 20)
                level = 20;                                         // Minimum level -120dB
            phase_test(20 - level);                                 // Covert level to attenuation register value
            break;
        }

        if (strncmp("nv_write",(const char *) pch, 8) == 0)         // Update values in non-volatile memory
        {
            pch = (unsigned char *) strtok(NULL, "\n\r ");          // Index in hex
            pch1 = (unsigned char *) strtok(NULL, "\n\r ");         // Data in hex
            data = (BYTE) strtoul((const char *) pch1, NULL, 16);
            index = (BYTE) strtoul((const char *) pch, NULL, 16);   // Get the index
            if (index > 15)                                         // Ensure index in range 0 - 15
                index = 15;
            nv_read();
            nv_buffer[index] = data;
            printf("\n\rWriting 0x%02x to index 0x%02x\n\r", data, index);
            nv_write();
            printf("Non-volatile memory updated\n\r");
            break;
        }

        if (strncmp("nv_read",(const char *) pch, 7) == 0)          // Read and display values in non-volatile memory
        {
            int i;
            nv_read();
            for (i=0; i<16; i++)
                printf("NV[%i] = %02x\n\r", i, nv_buffer[i]);
            printf("Non-volatile memory read\n\r");
            break;
        }

        if (strncmp("nv_default",(const char *) pch, 10) == 0)       // Reset non-volatile memory
        {
            nv_default();
            printf("Non-volatile memory reset to deafult values\n\r");
            break;
        }

        if (strncmp("mic",(const char *) pch, 3) == 0)              // Toggle mic enable on/off
        {
            int state;
            state = Florida_Read(0x300);
            if ((state & 0x0c) == 0x0c)
            {
                Florida_Write_Reg(0x300, 0x0033);
                printf("Mic input disabled\n\r");
            }
            else
            {
                Florida_Write_Reg(0x300, 0x003f);
                Florida_Write_Reg(0x0769, 0x0060);                  // -16dB
                printf("Mic input enabled\n\r");
            }
            break;
        }

        printf("Command not recognised\n\r");
        break;
    }
    
}


static void flushinput(void)
{
    while (getcht(((DLY_1S) * 3) >> 1) >= 0)
    {
    }
}

static int check(int crc, const unsigned char *buf, int sz)
{
    if (crc)
    {
        unsigned short crc = crc16_ccitt(buf, sz);
        unsigned short tcrc = (buf[sz] << 8) + buf[sz + 1];
        if (crc == tcrc)
            return 1;
    }
    else
    {
        int i;
        unsigned char cks = 0;
        for (i = 0; i < sz; ++i)
        {
            cks += buf[i];
        }
        if (cks == buf[sz])
            return 1;
    }

    return 0;
}


BYTE getUartTerm(void)
{
    BYTE data;

    while (receive_data_out_index == receive_data_in_index)         // If they are equal, no chars in buffer
        DelayMs(1);                                                 // Delay required to prevent While command being optimised out

    if (terminal == 1)
        IEC0bits.U1RXIE = 0;
    else
        IEC1bits.U2RXIE = 0;

    data = receive_buffer[receive_data_out_index];

    if (receive_data_out_index < (RECEIVE_BUFFER_LEN - 1))
        receive_data_out_index++;
    else
        receive_data_out_index = 0;

    if (terminal == 1)
        IEC0bits.U1RXIE = 1;
    else
        IEC1bits.U2RXIE = 1;

    return (data);
}


BYTE get_extension(BYTE *pch)                                       // Returns file_type:- 0 = .txt, 1 = .bat, 2 = .wmfm, 3 = .cflt, 4 = .beep
{
    BYTE ext[MAX_EXT_LEN], data, i;

    data = *pch++;

    while (data != '.' && data != '\r' && data != '\n')             // Search for '.' start of file extension
    {
        data = *pch++;
    }

    for (i = 0; i < MAX_EXT_LEN; i++)
    {
        ext[i] = *pch++;
    }

    if (strncmp("txt",(const char *) ext, 3) == 0)
        return (0);

    if (strncmp("wmfw",(const char *) ext, 4) == 0)
        return (2);

    if (strncmp("cflt",(const char *) ext, 4) == 0)
        return (3);

    if (strncmp("beep",(const char *) ext, 4) == 0)
        return (4);

    return (1); // default .bat
}


FRESULT scan_files(char* path)                                      // Start node to be scanned (also used as work area)
{
    FRESULT res;
    FILINFO fno;
    DIR dir;

#if TRACE
    printf("scan_files()\n\r");
#endif

    res = f_opendir(&dir, path);                                    // Open the directory
    if (res != FR_OK)
    {
        printf("\n\rf_opendir() @ L %d \n\rReason:\r\n", __LINE__);
        print_fresult(res);
    }

#if TRACE
    printf("scan_files()1\n\r");
    print_fresult(res);
    pause();
#endif

    if (res == FR_OK)
    {
        for (;;)
        {
            res = f_readdir(&dir, &fno);                            // Read a directory item
            if (res != FR_OK)
            {
                printf("\n\rf_readdir() @ L %d \n\rReason:\r\n", __LINE__);
                print_fresult(res);
            }

#if TRACE
            printf("scan_files()20\n\r");
            print_fresult(res);
            pause();
#endif

            if (res != FR_OK || fno.fname[0] == 0 || fno.fname[0] == 0xff)
            {
#if TRACE
                printf("scan_files()22\n\r");
                pause();
#endif
                break;                                              // Break on error or end of dir
            }
            if (fno.fname[0] == 0xE5)
            {
#if TRACE
                printf("scan_files(),78\n\r");
                pause();
#endif
                continue;
            }

            printf("%s %lu bytes\n\r", fno.fname, fno.fsize);       // It is a file?
        }
    }
    return res;
}


BYTE tone_pause(void)
{
    int i = 0;
    int j;

    do
    {
        i++;
        j = getcht(35);
    } while ((i < TEST_NOISE_LENGTH) && (j == -2));
    if (j != -2)                                                    // If any character received initiate pause
        return(pause());                                            // Returns 1 is ESC pressed, otherwise 0
    else
        return(0);
}


void speaker_test(int level)
{
    BYTE ans;
    int sw,lo;

    printf("Testing Speaker Operation level -%idB\n\r", ((20 - level) * 6));
    Florida_Write_Reg(0x070, (0x20 | level));                       // Enable White Noise Generator at specified amplitude
    Florida_Write_Reg(0x400,0xff);                                  // Enable all hp & skr outputs
    Florida_Write_Reg(0x519,0x03);                                  // Enable AIF1 TX for speaker 9 & 10
    Florida_Write_Reg(0x559,0x03);                                  // Enable AIF2 TX for sub and line-out
    lo = Florida_Read(0x768);                                       // Store Line Out connection address
    Florida_Write_Reg(0x768, 0x0000);                               // Disconnect Line Out to prevent pickup of speaker test tones

    printf("Speaker 1\n\r");
    Florida_Write_Reg(0x680, 0x000d);                               // Connect to Speaker 1
    ans = tone_pause();
    Florida_Write_Reg(0x680, 0x0068);                               // Reconnect Speaker 1
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 2\n\r");
    Florida_Write_Reg(0x688, 0x000d);                               // Connect to Speaker 2
    ans = tone_pause();
    Florida_Write_Reg(0x688, 0x0069);                               // Reconnect Speaker 2
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 3\n\r");
    Florida_Write_Reg(0x690, 0x000d);                               // Connect to Speaker 3
    ans = tone_pause();
    Florida_Write_Reg(0x690, 0x006a);                               // Reconnect Speaker 3
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 4\n\r");
    Florida_Write_Reg(0x698, 0x000d);                               // Connect to Speaker 4
    ans = tone_pause();
    Florida_Write_Reg(0x698, 0x006b);                               // Reconnect Speaker 4
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 5\n\r");
    Florida_Write_Reg(0x6a0, 0x000d);                               // Connect to Speaker 5
    ans = tone_pause();
    Florida_Write_Reg(0x6a0, 0x006c);                               // Reconnect Speaker 5
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 6\n\r");
    Florida_Write_Reg(0x6a8, 0x000d);                               // Connect to Speaker 6
    ans = tone_pause();
    Florida_Write_Reg(0x6a8, 0x0070);                               // Reconnect Speaker 6
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 7\n\r");
    Florida_Write_Reg(0x740, 0x000d);                               // Connect to Speaker 7
    ans = tone_pause();
    Florida_Write_Reg(0x740, 0x0071);                               // Reconnect Speaker 7
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 8\n\r");
    Florida_Write_Reg(0x748, 0x000d);                               // Connect to Speaker 8
    ans = tone_pause();
    Florida_Write_Reg(0x748, 0x0072);                               // Reconnect Speaker 8
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 9\n\r");
    Florida_Write_Reg(0x750, 0x000d);                               // Connect to Speaker 9
    ans = tone_pause();
    Florida_Write_Reg(0x750, 0x0073);                               // Reconnect Speaker 9
    if (ans == 1)
        goto ABORT_S;
    printf("Speaker 10\n\r");
    Florida_Write_Reg(0x758, 0x000d);                               // Connect to Speaker 10
    ans = tone_pause();
    Florida_Write_Reg(0x758, 0x0074);                               // Reconnect Speaker 10
    if (ans == 1)
        goto ABORT_S;
    printf("Sub Woofer\n\r");
    sw = Florida_Read(0x760);                                      // Store Sub Woofer connection address
    Florida_Write_Reg(0x760, 0x000d);                              // Connect to Sub Woofer
    ans = tone_pause();
    Florida_Write_Reg(0x760, sw);                                  // Reconnect Sub Woofer
    if (ans == 1)
        goto ABORT_S;
    printf("Line Out\n\r");
    Florida_Write_Reg(0x768, 0x000d);                              // Connect to Line Out
    ans = tone_pause();

ABORT_S:
    Florida_Write_Reg(0x768, lo);                                  // Reconnect Line Out
    Florida_Write_Reg(0x070,0x00);                                 // Reset White Noise Generator
}


void phase_test(int level)
{
    BYTE ans;

    printf("Testing Speaker Phase level -%idB\n\r", ((20 - level) * 6));
    Florida_Write_Reg(0x070, (0x20 | level));                       // Enable White Noise Generator at specified amplitude
    Florida_Write_Reg(0x400,0xff);                                  // Enable all hp & skr outputs
    Florida_Write_Reg(0x519,0x03);                                  // Enable AIF1 TX for speaker 9 & 10
    Florida_Write_Reg(0x559,0x03);                                  // Enable AIF2 TX for sub and line-out

    printf("Speaker 1 & 2\n\r");
    Florida_Write_Reg(0x680, 0x000d);                               // Connect to Speaker 1
    Florida_Write_Reg(0x688, 0x000d);                               // Connect to Speaker 2
    ans = tone_pause();
    Florida_Write_Reg(0x680, 0x0068);                               // Reconnect Speaker 1
    if (ans == 1)
    {
        Florida_Write_Reg(0x688, 0x0069);                           // Reconnect Speaker 2
        goto ABORT_P;
    }
    printf("Speaker 2 & 3\n\r");
    Florida_Write_Reg(0x690, 0x000d);                               // Connect to Speaker 3
    ans = tone_pause();
    Florida_Write_Reg(0x688, 0x0069);                               // Reconnect Speaker 2
    if (ans == 1)
    {
        Florida_Write_Reg(0x690, 0x006a);                           // Reconnect Speaker 3
        goto ABORT_P;
    }
    printf("Speaker 3 & 4\n\r");
    Florida_Write_Reg(0x698, 0x000d);                               // Connect to Speaker 4
    ans = tone_pause();
    Florida_Write_Reg(0x690, 0x006a);                               // Reconnect Speaker 3
    if (ans == 1)
    {
        Florida_Write_Reg(0x698, 0x006b);                           // Reconnect Speaker 4
        goto ABORT_P;
    }
    printf("Speaker 4 & 5\n\r");
    Florida_Write_Reg(0x6a0, 0x000d);                               // Connect to Speaker 5
    ans = tone_pause();
    Florida_Write_Reg(0x698, 0x006b);                               // Reconnect Speaker 4
    if (ans == 1)
    {
        Florida_Write_Reg(0x6a0, 0x006c);                           // Reconnect Speaker 5
        goto ABORT_P;
    }
    printf("Speaker 5 & 6\n\r");
    Florida_Write_Reg(0x6a8, 0x000d);                               // Connect to Speaker 6
    ans = tone_pause();
    Florida_Write_Reg(0x6a0, 0x006c);                               // Reconnect Speaker 5
    if (ans == 1)
    {
        Florida_Write_Reg(0x6a8, 0x0070);                           // Reconnect Speaker 6
        goto ABORT_P;
    }
    printf("Speaker 6 & 7\n\r");
    Florida_Write_Reg(0xa00, 0x0d);                                 // connect noise to DSP4_1_1
    Florida_Write_Reg(0xa02, 0x00);                                 // clear DSP4_1_2
    Florida_Write_Reg(0xa04, 0x00);                                 // clear DSP4_1_3
    Florida_Write_Reg(0xa08, 0x0d);                                 // connect noise to DSP4_2_1
    Florida_Write_Reg(0xa0a, 0x00);                                 // clear DSP4_2_2
    Florida_Write_Reg(0xa0c, 0x00);                                 // clear DSP4_2_3
    Florida_Write_Reg(0x940, 0x00);                                 // clear DSP1_1_1
    Florida_Write_Reg(0x948, 0x00);                                 // clear DSP1_2_1
    Florida_Write_Reg(0x980, 0x00);                                 // clear DSP2_1_1
    Florida_Write_Reg(0x988, 0x00);                                 // clear DSP2_2_1
    Florida_Write_Reg(0x6a8, 0x80);                                 // Connect DSP4_OUT1 to Speaker 6 (delay path)
    Florida_Write_Reg(0x740, 0x81);                                 // Connect DSP4_OUT2 to Speaker 7 (zero path)
    Florida_Write_Reg(ALGO_EN, 0x02);                               // activate DSP delay path
    ans = tone_pause();
    Florida_Write_Reg(0xa00, 0x10);                                 // restore DSP4_1_1
    Florida_Write_Reg(0xa02, 0x14);                                 // restore DSP4_1_2
    Florida_Write_Reg(0xa04, 0x92);                                 // restore DSP4_1_3
    Florida_Write_Reg(0xa08, 0x11);                                 // restore DSP4_2_1
    Florida_Write_Reg(0xa0a, 0x15);                                 // restore DSP4_2_2
    Florida_Write_Reg(0xa0c, 0x93);                                 // restore DSP4_2_3
    Florida_Write_Reg(0x940, 0x80);                                 // restore DSP1_1_1
    Florida_Write_Reg(0x948, 0x81);                                 // restore DSP1_2_1
    Florida_Write_Reg(0x980, 0x80);                                 // restore DSP2_1_1
    Florida_Write_Reg(0x988, 0x81);                                 // restore DSP2_2_1
    Florida_Write_Reg(0x6a8, 0x0070);                               // Reconnect Speaker 6
    Florida_Write_Reg(0x740, 0x000d);                               // Connect to Speaker 7
    Florida_Write_Reg(ALGO_EN, 0x01);                               // restore DSP4 functionality to normal
    if (ans == 1)
    {
        Florida_Write_Reg(0x740, 0x0071);                           // Reconnect Speaker 7
        goto ABORT_P;
    }
    printf("Speaker 7 & 8\n\r");
    Florida_Write_Reg(0x748, 0x000d);                               // Connect to Speaker 8
    ans = tone_pause();
    Florida_Write_Reg(0x740, 0x0071);                               // Reconnect Speaker 7
    if (ans == 1)
    {
        Florida_Write_Reg(0x748, 0x0072);                           // Reconnect Speaker 8
        goto ABORT_P;
    }
    printf("Speaker 8 & 9\n\r");
    Florida_Write_Reg(0x750, 0x000d);                               // Connect to Speaker 9
    ans = tone_pause();
    Florida_Write_Reg(0x748, 0x0072);                               // Reconnect Speaker 8
    if (ans == 1)
    {
        Florida_Write_Reg(0x750, 0x0073);                           // Reconnect Speaker 9
        goto ABORT_P;
    }
    printf("Speaker 9 & 10\n\r");
    Florida_Write_Reg(0x758, 0x000d);                               // Connect to Speaker 10
    ans = tone_pause();
    Florida_Write_Reg(0x750, 0x0073);                               // Reconnect Speaker 9
    Florida_Write_Reg(0x758, 0x0074);                               // Reconnect Speaker 10

ABORT_P:
    Florida_Write_Reg(0x070,0x00);                                  // Reset White Noise Generator
}


void nv_read(void)
{
     EEPROM_Read_Sector(0x4100, nv_buffer);
}


void nv_write(void)
{
    EEPROM_Read_Sector(0x4000, store_buffer);
    EEPROMEraseCluster(0x4000);
    EEPROM_wait_for_write_complete();
    EEPROM_Write_Sector(0x4000, store_buffer);
    EEPROM_Write_Sector(0x4100, nv_buffer);
}


void nv_default(void)
{
    EEPROM_Read_Sector(0x4000, store_buffer);
    EEPROMEraseCluster(0x4000);
    EEPROM_wait_for_write_complete();
    nv_buffer[0] = 0xaa;
    nv_buffer[1] = 0x55;
    nv_buffer[2] = 0xaa;
    nv_buffer[3] = 0x55;
    nv_buffer[4] = 0xaa;
    nv_buffer[5] = 0x55;
    nv_buffer[6] = 0xaa;
    nv_buffer[7] = 0x55;
    nv_buffer[8] = 0xaa;
    nv_buffer[9] = 0x55;
    nv_buffer[10] = 0xaa;
    nv_buffer[11] = 0x55;
    nv_buffer[12] = 0xaa;
    nv_buffer[13] = 0x55;
    nv_buffer[14] = 0xaa;
    nv_buffer[15] = 0x55;
    EEPROM_Write_Sector(0x4000, store_buffer);
    EEPROM_Write_Sector(0x4100, nv_buffer);
    EEPROM_wait_for_write_complete();
}


void dump_sector(WORD sector)
{
    int i, line;
    BYTE c;

    do
    {
        printf("Sector %d\n\r", sector);
        disk_read(dump_buff, sector);

        for (line = 0; line < 16; line++)
        {
            printf("%02x: ", line << 4);

            for (i = 0; i < 16; i++)
                printf("%02x ", dump_buff[(line * 16) + i]);

            printf("  ");

            for (i = 0; i < 16; i++)
            {
                c = dump_buff[(line * 16) + i];
                if ((c < 0x20) || (c > 0x7e))
                    printf(".");
                else
                    printf("%c", c);
            }
            printf("\n\r");
        }
        sector++;
        c = pause();
    }
    while ((!c) && (sector < MAX_SECTORS));
}

void help_func(void)
{
    printf("\n\r");
    printf("? | help <CR> Prints this help screen\n\r");
    printf("about <CR> Displays hardware and software version info\n\r");
    printf("BL <CR> Enter the bootloader\n\r");
    printf("BTD <CR> Display Bluetooth module settings\n\r");
    printf("BTRST <CR> Reset Bluetooth module to factory defaults\n\r");
    printf("BTS <CR> Initialise Bluetooth module\n\r");
    printf("BTV <CR> Display Bluetooth version\n\r");
    printf("BTP <CR> Set Bluetooth in Pairing Mode\n\r");
    printf("BTM <CR> Set Bluetooth maximum output level\n\r");
    printf("BTN <CR> Set Bluetooth normal output level\n\r");
    printf("cfg_florida <CR> Load config if available, defaults if not\n\r");
    printf("del <file> <CR> Deletes file\n\r");
    printf("dir <CR> List directory\n\r");
    printf("ds <sector> <CR> Dump sector\n\r");
    printf("florida <CR> Test PIC to Florida SPI Interface\n\r");
    printf("Kon <CR> KAP On\n\r");
    printf("Koff <CR> KAP Off\n\r");
    printf("load <file> <CR> Load a firmware file into Florida\n\r");
    printf("mic <CR> Toggle mic input enable off/on\n\r");
    printf("MKFS <CR> Erase, format and initialise filesystem and BT module\r\n");
    printf("mode <new_mode> <CR> Changes the DSP processing mode to that specified (in range 1 - 6)\n\r");
    printf("nv_default <CR> Reads and dispplays the contents of the non-volatile buffer\n\r");
    printf("nv_read <CR> Resets the non-volatile buffer to factory default values\n\r");
    printf("nv_write <index> <data> <CR> Writes to value data to non-volatile storage byte index\n\r");
    printf("phase <amplitude> <CR> Plays noise signal through each pair of speakers in turn to check phase correlation\n\r");
    printf("read <address> <CR> Reads data from florida\n\r");
    printf("Reboot <CR> Restarts system\n\r");
    printf("reset_florida <CR> Hardware resets the Florida Chip\n\r");
    printf("speaker <amplitude> <CR> Plays noise signal through each speaker in turn and finishes with the subwoofer\n\r");
    printf("toneon <channel> <level> <CR> Play 1 Khz tone through channel 1 - 12 at attenuation level 1 - 32dB\r\n");
    printf("type <file> <CR> Prints contents of file\n\r");
    printf("V+ <CR> Volume Up\n\r");
    printf("V- <CR> Volume Down\n\r");
    printf("ver <CR> Displays software version id\n\r");
    printf("verify <dspx> <filename> <CR> Compares dsp(1 - 4) in florida with .wmfw file\n\r");
    printf("write <address> <data> <CR> Writes to florida\n\r");
    printf("xfer <file> <CR> Saves file from xmodem transfer from PC\n\r");
}


void test_spi_to_Florida(void)
{
    WORD check, temp;
    BYTE error_flag;

    error_flag = 0;                                                 // Start with no errors

    check = Florida_Read(0x0);
    if (check != 0x5110)
    {
        printf("Error, Florida Device ID not 0x5110 (at address 0), read back %x\n\r", check);
        error_flag |= 1;
    }

    printf("Read / Writing to Florida address 0x87\n\r");
    temp = Florida_Read(0x87);                                      // Read contents first
    Florida_Write_Reg(0x87, 0x55aa);
    check = Florida_Read(0x87);
    if (check != 0x55aa)
    {
        printf("Error, wrote 0x55aa, read back %x at address 0x87\n\r", check);
        error_flag |= 1;
    }
    Florida_Write_Reg(0x87, temp);                                  // Write contents back

    printf("Read / Writing to Florida address 0x88\n\r");
    temp = Florida_Read(0x88);                                      // Read contents first
    Florida_Write_Reg(0x88, 0xffff);
    check = Florida_Read(0x88);
    if (check != 0xffff)
    {
        printf("Error, wrote 0xffff, read back %x at address 0x88\n\r", check);
        error_flag |= 1;
    }
    Florida_Write_Reg(0x88, temp);                                  // Write contents back

    printf("Read / Writing to Florida address 0x190000\n\r");
    temp = Florida_Read(0x190000);                                  // Read contents first
    Florida_Write_Reg(0x190000, 0x0034);
    check = Florida_Read(0x190000);
    if (check != 0x0034)
    {
        printf("Error, wrote 0x0034, read back %x at address 0x190000\n\r", check);
        error_flag |= 2;
    }
    Florida_Write_Reg(0x190000, temp);                              // Write contents back

    printf("Read / Writing to Florida address 0x190001\n\r");
    temp = Florida_Read(0x190001);                                  // Read contents first
    Florida_Write_Reg(0x190001, 0x9876);
    check = Florida_Read(0x190001);
    if (check != 0x9876)
    {
        printf("Error, wrote 0x9876, read back %x at address 0x190001\n\r", check);
        error_flag |= 2;
    }
    Florida_Write_Reg(0x190001, temp);                              // Write contents back

    if (error_flag == 0)
        printf("No Errors PIC to Florida via SPI\n\r");
    else if (error_flag == 3)
        printf("Multiple errors PIC to Florida via SPI\n\r");
    else
        printf("Need clock running for these program locations to work\n\r");
}


long xmodemReceive(BYTE file_type)
{
    unsigned char *p;
    int bufsz, crc = 0;
    unsigned char trychar = 'C';
    unsigned char packetno = 1;
    int i, c;
    int retry, retrans = MAXRETRANS;
    DWORD len = 0;

    data_in_index = 0;                                              // Used in save_file()
    data_out_index = 0;                                             // Used in save_file()

    for (;;)
    {
        for (retry = 0; retry < 64; ++retry)
        {
            if (trychar)
            {
                putch2(trychar);
            }
            if ((c = getcht(1000)) >= 0)
            {
                switch (c)
                {
                case SOH:
                    bufsz = 128;
                    goto start_recv;
                case EOT:
                    flushinput();
                    putch2(ACK);
                    len -= 4;
                    return len;                                     // Normal end
                case CAN:
                    if ((c = getcht(1000)) == CAN)
                    {
                        flushinput();
                        putch2(ACK);
                        return -1;                                  // Canceled by remote
                    }
                    break;
                default:
                    break;
                }
            }
        }

        if (trychar == 'C')
        {
            trychar = NAK;
            continue;
        }
        flushinput();
        putch2(CAN);
        putch2(CAN);
        putch2(CAN);
        return -2;                                                  // Sync error

start_recv:
        if (trychar == 'C')
            crc = 1;
        trychar = 0;
        p = xbuff;
        *p++ = c;

        for (i = 0; i < (bufsz + (crc ? 1 : 0) + 3); ++i)
        {
            if ((c = getcht(DLY_1S)) < 0)
                goto reject;
            len++;
            *p++ = c;
        }

        if (xbuff[1] == (unsigned char) (~xbuff[2]) &&
                (xbuff[1] == packetno || xbuff[1] == (unsigned char) packetno - 1) &&
                check(crc, &xbuff[3], bufsz))
        {
            if (xbuff[1] == packetno)
            {
                save_file(&xbuff[3], file_type);

                ++packetno;
                retrans = MAXRETRANS + 1;
            }
            if (--retrans <= 0)
            {
                flushinput();
                putch2(CAN);
                putch2(CAN);
                putch2(CAN);
                return -3;                                          // Too many retry error
            }
            putch2(ACK);
            continue;
        }

reject:

        flushinput();
        putch2(NAK);
    }
}

int getcht(WORD timeout)
{
    int delay = timeout;                                            // Timeout in milliseconds
    BYTE data;

    while (receive_data_out_index == receive_data_in_index)         // If they are equal, no chars in buffer
    {
        if (timeout)                                                // If 0 bypass timeout and loop indefinitely waiting for data ready
        {
            DelayMs(1);

            if (--delay <= 0)
                return -2;
        }
    }

    if (terminal == 1)
        IEC0bits.U1RXIE = 0;
    else
        IEC1bits.U2RXIE = 0;

    data = receive_buffer[receive_data_out_index];

    if (receive_data_out_index <= receive_data_in_index)
        chars_in_buffer = receive_data_out_index - receive_data_in_index + 1;
    else
        chars_in_buffer = RECEIVE_BUFFER_LEN - (receive_data_out_index - receive_data_in_index) + 1;

    if (receive_data_out_index < (RECEIVE_BUFFER_LEN - 1))
        receive_data_out_index++;
    else
        receive_data_out_index = 0;

    if (terminal == 1)
        IEC0bits.U1RXIE = 1;
    else
        IEC1bits.U2RXIE = 1;

    return (data);
}


void load_firmware(UINT8 core, 	DWORD file_size, BYTE verify)
{
    FRESULT st;
    UINT numbytes;

    HeaderInfo HeaderCode;
    DataBlock Data;
    int i;

    union u_type {
    UINT32 i;
    unsigned char char_buf[4];
    };

    union u_type convert;

    Page_Index = 0;
    file_byte_count = 0;

    st = f_read(&fdst,user_buffer,256,&numbytes);
    file_byte_count = numbytes;

#if FIRMWARE
    printf("load_firmware()1, numbytes %d\r\n",numbytes);
#endif


    if (FR_OK != st || numbytes == 0)
    {
        printf("load_firmware()1,\n\r");
        return;
    }

    HeaderCode.MagicId = ReadWordBigEndian(&user_buffer[Page_Index], FW_MGICID_ID_LENGTH); // Print magic id
    Page_Index += FW_MGICID_ID_LENGTH;

    if (FIRMWARE_MAGIC_ID == HeaderCode.MagicId)
    {                                                               // Get header length in bytes
        for(i = 0; i < FW_HEADER_LENGTH_LENGTH; i++)                // Little Endian
        {
            convert.char_buf[i] = user_buffer[Page_Index++];
        }

        HeaderCode.HeaderLength = convert.i;

        Page_Index += (HeaderCode.HeaderLength - FW_HEADER_LENGTH_LENGTH - 4); // Skip over format version, target core and core revision and the rest of the Main Header Block
#if FIRMWARE
        printf("load_firmware(), Main Header Length %ld\r\n",HeaderCode.HeaderLength);
        printf("load_firmware(), f_tell %ld, file_size %ld\n\r",f_tell(&fdst), file_size);
#endif
        while(file_byte_count < file_size)                          // Get a Data Block Header Info - should be pointing to start of a Data block
        {                                                           // First get memory region from data block
            for(i = 0;i < FW_DATA_START_OFFSET_LENGTH;i++)          // Little Endian
            {
                convert.char_buf[i] = user_buffer[Page_Index++];

                if(Page_Index >= 256)
                {
                    Page_Index = 0;
                    st = f_read(&fdst,user_buffer,256,&numbytes);
                    file_byte_count += numbytes;
#if FIRMWARE
                    printf("load_firmware()2, numbytes %d\r\n",numbytes);
#endif
                    if (FR_OK != st || numbytes == 0)
                    {
                        printf("load_firmware()2,\n\r");
                        return;
                    }
                }
            }
            convert.char_buf[i] = 0;

            Data.StartOffset = convert.i;

            Data.MemoryRegion = user_buffer[Page_Index++];          // Reading data block memory region

            if(Page_Index >= 256)
            {
                Page_Index = 0;
                st = f_read(&fdst,user_buffer,256,&numbytes);
                file_byte_count += numbytes;
#if FIRMWARE
		printf("load_firmware()3, numbytes %d\r\n",numbytes);
#endif
                if (st || numbytes == 0)
                {
                    printf("load_firmware()2,\n\r");
                    return;
                }
            }
                                                                    // Reading data block data length
            for(i = 0;i < FW_DATA_DATA_LENGTH_LENGTH;i++)           // Little Endian
            {
                convert.char_buf[i] = user_buffer[Page_Index++];

                if(Page_Index >= 256)
                {
                    Page_Index = 0;
                    st = f_read(&fdst,user_buffer,256,&numbytes);
                    file_byte_count += numbytes;
#if FIRMWARE
		printf("load_firmware()4, numbytes %d\r\n",numbytes);
#endif
                    if (st || numbytes == 0)
                    {
                        printf("load_firmware()3,\n\r");
                        return;
                    }
                }
            }

            Data.DataLength = convert.i;

#if FIRMWARE
            printf("load_firmware(), Memory Region %x, Start Offset %ld, Data Length %ld\n\r",
            Data.MemoryRegion, Data.StartOffset, Data.DataLength);
            printf("load_firmware(), f_tell() %ld\n\r",f_tell(&fdst));
            pause();
#endif
            if (Data.MemoryRegion == TEXT_REGION)
            {
                while (Data.DataLength--)
                {
                    char c = user_buffer[Page_Index];

                    if (c)
                    {
#if FIRMWARE
                        putch2(c);
#endif
                    }
                    Page_Index++;

                    if(Page_Index >= 256)
                    {
                        Page_Index = 0;
                        st = f_read(&fdst,user_buffer,256,&numbytes);
                        file_byte_count += numbytes;
#if FIRMWARE
                        printf("load_firmware()5, numbytes %d\r\n",numbytes);
#endif


                        if (st || numbytes == 0)
                        {
                            printf("load_firmware()3,\n\r");
                            return;
                        }
                    }
                }
#if FIRMWARE
                pause();
#endif
            }
            else
            {
                if(WriteDataBlock(user_buffer, &Data, core, verify))
                {
#if TRACE2
                    printf("\n\rError WriteDataBlock()\n\r");
#endif
                    break;
                }
            }
        }                                                           // End of while
    }
    else                                                            // FIRMWARE_MAGIC_ID != HeaderCode.MagicId
    {
        printf("HeaderCode.MagicId not equal to FIRMWARE_MAGIC_ID\n\r");
    }

    f_close(&fdst);                                                 // Close opened file
}


int WriteDataBlock(BYTE *user_buffer, DataBlock *Data, BYTE core, BYTE verify)
{
    BYTE numRegPerWord = 0;
    DWORD regionStart = 0;
    DWORD regStartOffset = 0;
    DWORD blockLength = Data->DataLength;
    FRESULT st;
    UINT numbytes;
    DWORD address = 0;
    WORD data = 0;
    WORD temp;

                                                                    // Setting register address
    switch (Data->MemoryRegion)
    {
    case PM_REGION:

        numRegPerWord = NUM_REGISTER_PER_PM_CODE;                   // 3

        if (core == ADSP1_CORE1)
        {
            regionStart = DSP1_PM_START;
        }
        else if (core == ADSP1_CORE2)
        {
            regionStart = DSP2_PM_START;
        }
        else if (core == ADSP1_CORE3)
        {
            regionStart = DSP3_PM_START;
        }
        else if (core == ADSP1_CORE4)
        {
            regionStart = DSP4_PM_START;
        }
        else
        {
            printf("PM Region core error \n");
            return (1);
        }
        break;

    case DM_REGION_X:

        numRegPerWord = NUM_REGISTER_PER_DM_DATA;                   // 2

        if (core == ADSP1_CORE1)
        {
            regionStart = DSP1_DM_X_START;
        }
        else if (core == ADSP1_CORE2)
        {
            regionStart = DSP2_DM_X_START;
        }
        else if (core == ADSP1_CORE3)
        {
            regionStart = DSP3_DM_X_START;
        }
        else if (core == ADSP1_CORE4)
        {
            regionStart = DSP4_DM_X_START;
        }
        else
        {
            printf("DM_X Region core error \n");
            return (1);
        }
        break;

    case ZM_REGION:

        numRegPerWord = NUM_REGISTER_PER_ZM_DATA;                   // 2

        if (core == ADSP1_CORE1)
        {
            regionStart = DSP1_ZM_START;
        }
        else if (core == ADSP1_CORE2)
        {
            regionStart = DSP2_ZM_START;
        }
        else if (core == ADSP1_CORE3)
        {
            regionStart = DSP3_ZM_START;
        }
        else if (core == ADSP1_CORE4)
        {
            regionStart = DSP4_ZM_START;
        }
        else
        {
            printf("ZM Region core error \n");
            return (1);
        }
        break;

    case DM_REGION_Y:

        numRegPerWord = NUM_REGISTER_PER_DM_DATA;                   // 2

        if (core == ADSP1_CORE1)
        {
            regionStart = DSP1_DM_Y_START;
        }
        else if (core == ADSP1_CORE2)
        {
            regionStart = DSP2_DM_Y_START;
        }
        else if (core == ADSP1_CORE3)
        {
            regionStart = DSP3_DM_Y_START;
        }
        else if (core == ADSP1_CORE4)
        {
            regionStart = DSP4_DM_Y_START;
        }
        else
        {
            printf("DM_Y Region core error \n");
            return (1);
        }
        break;

    case TEXT_REGION:
        printf("Text Region\n\r");
        break;

    default:
#if TRACE2
        printf("\n\rFound unknown region (Data->MemoryRegion) == %d\r\n",(Data->MemoryRegion));
#endif
        return (1);
    }

    regStartOffset = Data->StartOffset * numRegPerWord;

    regionStart += regStartOffset;

#if FIRMWARE
    printf("WriteDataBlock(), regStartOffset %ld, blockLength %ld, regionStart 0x%lx\n\r", regStartOffset, blockLength, regionStart);
#endif

    if (verify)
        address = regionStart;
    else
        Florida_Write_Address(regionStart);

    while (blockLength)
    {
        if (verify)
            data = (WORD) (((user_buffer[Page_Index++]) << 8) & 0xff00);
        else
            Write_Byte_SPI1(user_buffer[Page_Index++]);

        if (Page_Index >= 256)
        {
            Page_Index = 0;
            st = f_read(&fdst, user_buffer, 256, &numbytes);
            file_byte_count += numbytes;
#if FIRMWARE
            printf("WriteDataBlock()1, numbytes %d\r\n", numbytes);
#endif

            if (st || numbytes == 0)
            {
                return (1);
            }
        }

        if (verify)
        {
            data |= (WORD) (((user_buffer[Page_Index++]) & 0x00ff));

            temp = Florida_Read(address);

            if (temp != data)
            {
                printf("ERROR florida data 0x%04x != file data 0x04%x at address 0x%06lx\n\r", temp, data, address);
                return (1);
            }
        }
        else
            Write_Byte_SPI1(user_buffer[Page_Index++]);

        if (Page_Index >= 256)
        {
            Page_Index = 0;
            st = f_read(&fdst, user_buffer, 256, &numbytes);
            file_byte_count += numbytes;
#if FIRMWARE
            printf("WriteDataBlock()2, numbytes %d\r\n", numbytes);
#endif

            if (st || numbytes == 0)
            {
                return (1);
            }
        }

        blockLength -= 2;

        if (verify)
            address++;
    }

    if (verify == 0)                                                // load
        Florida_Write_Finish();                                     // deassert the chip select to florida to finish the block transfer

    return (0);                                                     // no errors
}


void load_fw_header(int core)
{
    int  i;

    if (fw_header_en == FALSE)
        return;

    if (core == 1)
        Florida_Write_Address(DSP1_PM_START);
    else if (core == 2)
        Florida_Write_Address(DSP2_PM_START);
    else
        return;

    for (i = 0; i < 0x400; i++)
    {
        if (core == 1)
            Write_Byte_SPI1(fw1[i]);
        else if (core == 2)
            Write_Byte_SPI1(fw2[i]);
    }
    Florida_Write_Finish();
}


void load_filter(DWORD Addr)
{
    FRESULT st;
    UINT numbytes;
    DWORD Offset = 0;
    int index, f, i;

    while (Offset < 0x101400)
    {
        Florida_Write_Address(Addr + Offset);

        for (f = 0; f < 10; f++)
        {
            index = 0;
            st = f_read(&fdst, user_buffer, 256, &numbytes);

            if ((Offset == 0) && (f == 0))
                if ((user_buffer[0] == 'C') && (user_buffer[1] == 'F') && (user_buffer[2] == 'L') && (user_buffer[3] == 'T'))
                {
                    if (Addr == FILTER_START_1)
                        for (i = 0; i < FLT_HDR_LEN; i++)
                            filter1_info[i] = user_buffer[4+i];
                    else
                        for (i = 0; i < FLT_HDR_LEN; i++)
                            filter2_info[i] = user_buffer[4+i];
                    st = f_read(&fdst, user_buffer, 256, &numbytes);
                }

            while (index < 256)
                Write_Byte_SPI1(user_buffer[index++]);

            index = 0;
            st = f_read(&fdst, user_buffer, 256, &numbytes);
            while (index < 256)
                Write_Byte_SPI1(user_buffer[index++]);

            for (index = 0; index < 512; index++)
                Write_Byte_SPI1(0);                                 // Pad 256 words of zero between coefficient blocks
        }
        Florida_Write_Finish();                                     // deassert the chip select to florida to finish the block transfer
        Offset += 0x100000;
    }
}


void load_tone(DWORD Addr)
{
    FRESULT st;
    UINT numbytes;
    DWORD Offset = 0;
    int index, f;

    while (Offset < 0x101400)
    {
        Florida_Write_Address(Addr + Offset);

        for (f = 0; f < 10; f++)
        {
            index = 0;
            st = f_read(&fdst, user_buffer, 256, &numbytes);
            while (index < 256)
                Write_Byte_SPI1(user_buffer[index++]);

            index = 0;
            st = f_read(&fdst, user_buffer, 256, &numbytes);
            while (index < 256)
                Write_Byte_SPI1(user_buffer[index++]);

            for (index = 0; index < 512; index++)
                Write_Byte_SPI1(0);                                 // Pad 256 words of zero between coefficient blocks
        }
        Florida_Write_Finish();                                     // De-assert the chip select to florida to finish the block transfer
        Offset += 0x100000;
    }
}


unsigned short crc16_ccitt(const unsigned char *buf, int len)
{
    unsigned short crc = 0;
    while (len--)
    {
        int i;
        crc ^= *(char *) buf++ << 8;
        for (i = 0; i < 8; ++i)
        {
            if (crc & 0x8000)
                crc = (crc << 1) ^ 0x1021;
            else
                crc = crc << 1;
        }
    }
    return crc;
}


int save_file(BYTE *buffer, BYTE file_type) // file_type:- 0 = .txt, 1 = .bat, 2 = .wmfm, 3 = .cflt, 4 = .beep
{
    BYTE data;
    BYTE *token_ptr;
    int i;
    UINT bw;
    FRESULT res;

    if (file_type == 0)                                             // .txt (WISCE script)
    {
        data_in_index = 0;                                          // Reset for start of new data

        while (1)
        {
            if (data_in_index >= 128)                               // Added because last character (index 127) is LF (0x0a) in the prevoius loop
            {
                return (0);                                         // Need more characters before end of line
            }

            data = buffer[data_in_index++];

            while (data != 0x0a)                                    // Search for end of line
            {
                if (data_out_index < 510)                           // Don't overflow buffer
                    temp_buffer[data_out_index++] = data;

                if (data_in_index >= 128)
                {
                    return (0);                                     // Need more characters before end of line
                }
                data = buffer[data_in_index++];
            }
            temp_buffer[data_out_index] = NUL;
                                                                    // Got a line of text, now process it
            data_out_index = 0;                                     // Reset to get another line

            token_ptr = (unsigned char *) strtok((char *) temp_buffer, "\n\r ");

            if ((token_ptr[0] == '*') || ((token_ptr[0] == 'S') && (token_ptr[1] == 'E') &&(token_ptr[2] == 'T')))
            {
                continue;                                           // Comment line, or a SET, get next line
            }

            if (token_ptr[0] == '0' && token_ptr[1] == 'x')         // Process address and data line
            {
                strcpy((char *) dump_buff, (const char *) token_ptr); // Output address
                strcat((char *) dump_buff, " ");
                token_ptr = (unsigned char *) strtok(NULL, "\n\r ");
                strcat((char *) dump_buff, (const char *) token_ptr); // Output data
                strcat((char *) dump_buff, " ");
                token_ptr = (unsigned char *) strtok(NULL, "\n\r "); // Skip over SMbus_32inx_16dat
                token_ptr = (unsigned char *) strtok(NULL, "\n\r "); // Get Read / Write
                if (token_ptr[0] == 'W')
                    strcat((char *) dump_buff, "W\n\r");
                else
                    strcat((char *) dump_buff, "R\n\r");

                res = f_write(&fdst, dump_buff, strlen((const char *) dump_buff), &bw); // Write it to the dst file
                continue;
            }

        }
    }
    else                                                            // Save as is for .bat and .wmfm and .cflt and .beep
    {
        for (i = 0; i < 128; i++)
        {
            dump_buff[i] = *buffer++;
        }
        res = f_write(&fdst, dump_buff, 128, &bw);                  // Write it to the dst file
    }
    return (0);
}


void USB_reset(void)
{
    USB_RST(HIGH);                                                  // Pull USB reset active high
    DelayMs(100);
    USB_RST(LOW);                                                   // Remove USB reset
    DelayMs(500);
}


void DelayMs(WORD delay)                                            // Delay in Millisecs
{
    __delay_ms(delay);
}

void putch1(char c)
{
    while (BusyUART1())
    {
    }
    putcUART1(c);
}

void putch2(char c)
{
    if (terminal == 1)
    {
        while (BusyUART1())
        {
        }
        putcUART1(c);
    }
    else
    {
        while (BusyUART2())
        {
        }
        putcUART2(c);
    }
}

void putch3(char c)
{
    while (BusyUART3())
    {
    }
    putcUART3(c);
}


BYTE pause(void)                                                    // Return 1 if ESC key pressed, 0 for any other key
{
    printf("\n\rPress escape to abort or any other key to continue\n\r");
    if (getUartTerm() == ESC)
        return (1);
    else
        return (0);
}

void type_command(BYTE extension) // to be completed // extension:- 0 = .txt, 1 = .bat, 2 = .wmfw, 3 = .cflt, 4 = .beep
{
    FRESULT st;
    UINT numbytes;
    int i, line;
    BYTE c;
    int lines_left;
    int chars_left;
    BYTE temp[30];

    if (extension == 0 || extension == 1)                           // 0 = .txt, 1 = .bat
    {
        i = 0;

        while (1)                                                   // This loop prints text file as is on the screen
        {
            st = f_read(&fdst, user_buffer, 256, &numbytes);
            if (FR_OK != st || numbytes == 0)
            {
                break;
            }

            line = 0;

            while (1)
            {

                c = user_buffer[line++];

                while ((c != CR) && (((int) (numbytes) - line) > 0))
                {
                    temp[i++] = c;
                    c = user_buffer[line++];
                }
                                                                    // Got here because either got a line to print or end of page
                if (c == CR)                                        // Print it
                {
                    temp[i++] = CR;
                    temp[i] = NUL;

                    printf("%s", temp);

                    i = 0;

                    if (line >= 256)                                // Special case if CR at the end of a page
                    {
                        if (pause())
                            return;
                        break;
                    }
                }
                else
                {
                    temp[i++] = c;

                    if (pause())
                        return;
                    break;                                          // Get another page if any other key (not escape) pressed
                }
            }

        }
    }

    if ((extension == 2) || (extension == 3) || (extension == 4))   // .wmfw or .cflt or .beep
    {
        do                                                          // This loop prints hex plus text on screen
        {
            st = f_read(&fdst, user_buffer, 256, &numbytes);
            if (st || numbytes == 0)
            {
                break;
            }

            lines_left = numbytes >> 4;                             // Divide by 16
            chars_left = numbytes % 16;

            for (line = 0; line < lines_left; line++)               // Output complete lines to the display
            {
                printf("%02x: ", line << 4);                        // Print index

                for (i = 0; i < 16; i++)
                    printf("%02x ", user_buffer[(line * 16) + i]);

                printf(" |");

                for (i = 0; i < 16; i++)                            // This for-loop is to display text characters on the right hand side of the screen
                {
                    c = user_buffer[(line * 16) + i];
                    if (c == 0x0a)
                        printf("a");
                    else
                    {
                        if (c == 0x0d)
                            printf("d");
                        else
                        {
                            if ((c < 0x20) || (c > 0x7e))
                                printf(".");
                            else
                                printf("%c", c);
                        }
                    }
                }
                printf("\n\r");
            }

            if (chars_left != 0)                                    // Print the remaining incomplete line
            {
                printf("%02x: ", line << 4);                        // Print index

                for (i = 0; i < chars_left; i++)
                    printf("%02x ", user_buffer[(line * 16) + i]);


                for (i = 0; i < (16 - chars_left); i++)             // Pad out number of spaces for text display on right hand side of screen
                    printf("   ");
                printf(" |");

                for (i = 0; i < chars_left; i++)                    // This for-loop is to display text characters on the right hand side of the screen
                {
                    c = user_buffer[(line * 16) + i];
                    if ((c < 0x20) || (c > 0x7e))
                        printf(".");
                    else
                        printf("%c", c);
                }
                printf("\n\r");
            }

            if (pause())
                return;
        }
        while (numbytes >= 256);
    }
}


UINT32 ReadWordBigEndian(UINT8 *ptr, UINT8 count)
{
	int i;
	union u_type {
	UINT32 i;
	unsigned char char_buf[4];
	};

	union u_type convert;

	for(i = 0;i < 4;i++)
		convert.char_buf[i] = 0x00;                         // Clear out buffer just in case count is less than 4

	for(i = 4;i > 0;i--)                                        // Big Endian
		convert.char_buf[i - 1] = *ptr++;

	return (convert.i);
}


int writeFileToMemory(int core, DWORD baseAddress, char *filename)
{
    FIL fileObject;
    FRESULT res = FR_OK;
    UINT bytesRead = 0;
    int result = 0;
    int dataBuffer[8];
    WORD tWord=0;

    if ((res = f_open(&fileObject, (char const *) filename, FA_READ)) != FR_OK)
    {
        printf("\n\rERROR opening %s\n", filename);
        result = 1;
    }
    else
    {
                                                                    // Cfg WM8280 SPI to auto-increment
        tWord = Florida_Read(0x0008);
        tWord |= 1;
        tWord &= ~(1<<1);
        Florida_Write_Reg(0x0008, tWord);

        Florida_Write_Address(baseAddress);                         // Write start address to WM8280
        while(!f_eof(&fileObject))
        {
            res = f_read(&fileObject, dataBuffer, 1, &bytesRead);   // Read a chunk of src file
            if (res != FR_OK || bytesRead == 0)
            {                                                       // Error or eof
                printf("\n\rERROR reading chunk from file\n");
                result=1;
                break;
            }
            else
            {                                                       // Write to memory
                WriteSPI1(dataBuffer[0]);
            }
        }
    }
    f_close(&fileObject);
    return(result);
}


void load_script(void)
{
    FRESULT st;
    UINT numbytes;
    int i, char_index, x;
    BYTE c;
    WORD line;
    DWORD iaddress;
    WORD idata;

    BYTE *address;
    BYTE *data;
    BYTE *r_w;
    WORD read_data;

    i = 0;
    line = 0;                                                       // Counts line number to report if error

    while (1)
    {
        st = f_read(&fdst, user_buffer, 256, &numbytes);
        if (FR_OK != st || numbytes == 0)
        {
            break;
        }

        char_index = 0;

        while (1)
        {
            c = user_buffer[char_index++];

            while ((c != CR) && (((int) (numbytes) - char_index) > 0))
            {
                strbuffer[i++] = c;
                c = user_buffer[char_index++];
            }
                                                                    // Got here because either got a line to print or end of page
            if ((c == CR) && (i > 5))                               // Load it
            {
                strbuffer[i++] = CR;
                strbuffer[i] = NUL;

                address = (unsigned char *) strtok(strbuffer, "\n\r "); // pch points to the address string

                iaddress = strtoul((const char *) address, NULL, 16);   // Convert to DWORD

                data = (unsigned char *) strtok(NULL, "\n\r ");         // pch points to the data string

                idata = (WORD) strtoul((const char *) data, NULL, 16);  // Convert to WORD

                r_w = (unsigned char *) strtok(NULL, "\n\r ");          // pch points to the R or W string

                if (iaddress > 0x4fffff)                                // Send commands to PIC via parameter with address outside Florida range
                {
                    if (iaddress == 0x500000)                           // Watchdog disable
                    {
                        if (idata == 0)
                            watchdog_en = FALSE;
                        else
                            watchdog_en = TRUE;
                    }
                    if (iaddress == 0x500001)                           // set LED brightness. default = 5
                    {
                        x = 0x05;
                        while (idata > x)
                        {
                            x++;
                            DelayMs(5);
                        }
                        while (idata < x)
                        {
                            x--;
                            DelayMs(5);
                        }
                    }
                    if (iaddress == 0x500002)                           // allow change of module revision to x2 default = x1
                    {
                        if (idata == 2)
                            module_rev = 2;
                    }
                    if (iaddress == 0x500003)                           // allow change of array size to enable Bar mode
                    {
                        if (idata == 1)
                            bar_mode = TRUE;
                    }
                    if (iaddress == 0x500004)                           // allow change of external subwoofer volume offset
                    {
                        if (idata > 0x0c)
                            idata = 0x0c;
                        sub_offset = (idata & 0xfe);                    // clear bit 0 for valid value
                    }
                    if (iaddress == 0x500005)                           // Watchdog disable
                    {
                        if (idata == 0)
                            fw_header_en = FALSE;
                        else
                            fw_header_en = TRUE;
                    }
                    if (iaddress == 0x500006)                           // allow swap of volume key detection
                    {
                        if (idata == 1)
                            key_swap = TRUE;
                    }
                    if (iaddress == 0x500007)                           // Watchdog disable
                    {
                        if (idata == 1)
                        {
                            terminal = 1;
                            __C30_UART = terminal;
                        }
                        if (idata == 2)
                        {
                            terminal = 2;
                            __C30_UART = terminal;
                        }
                    }
                    if (iaddress == 0xffffff)                           // Config script version
                        config_rev = idata;
                    if (iaddress == 0xfffff0)                           // Parameter script version
                        param_rev = idata;
                }
                
                else
                {

                    if (r_w[0] == 'W')                                  // Write
                    {

                        Florida_Write_Reg(iaddress, idata);
                    }
                    else                                                // Read
                    {
                        for (i = 5; i > 0; i--)
                        {

                            read_data = Florida_Read(iaddress);

                            if (read_data == idata)
                                break;

                            DelayMs(1);
                        }
                        if (i <= 0)
                        {
                            printf("Read back %x should be %x at address %lx, in line %d\n\r", read_data, idata, iaddress, line);
                        }
                    }

                }

                line++;
                i = 0;

                if (char_index >= 256)                              // Special case if CR at the end of a page
                {
                    break;
                }
            }

            else
            {
                strbuffer[i++] = c;
                break;                                              // Get another page if any other key (not escape) pressed
            }
        }
    }
    f_close(&fdst);                                                 // Close opened file
}


void SetMode(void)
{
    printf("\n\rChange to mode 0x%02x\n\r",DSP_Status);

    switch (DSP_Status)
    {
        case 0x01:                                                  // Beam,  flt 1 , no KAP, no call
            Florida_Write_Reg(HPF1_EN,0x01);
            Florida_Write_Reg(RENDER_REG1,0x01);
            Florida_Write_Reg(RENDER_REG2,0x01);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x01);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x01);
            Florida_Write_Reg(HEADPHONE_REG, 0x00);
            Florida_Write_Reg(KAP_REG, 0x00);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(OFF);
            break;
        case 0x09:                                                  // Beam,  flt 1 , KAP, no call
            Florida_Write_Reg(HPF1_EN,0x01);
            Florida_Write_Reg(RENDER_REG1,0x01);
            Florida_Write_Reg(RENDER_REG2,0x01);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x01);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x01);
            Florida_Write_Reg(HEADPHONE_REG, 0x00);
            Florida_Write_Reg(KAP_REG, 0x01);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(ON);
            break;
        case 0x02:
        case 0x06:                                                  // Headset, no KAP, no call
            Florida_Write_Reg(HPF1_EN,0x00);
            Florida_Write_Reg(RENDER_REG1,0x02);
            Florida_Write_Reg(RENDER_REG2,0x02);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x00);
            Florida_Write_Reg(HEADPHONE_REG, 0x01);
            Florida_Write_Reg(KAP_REG, 0x00);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(ON);
            break;
        case 0x0a:
        case 0x0e:                                                  // Headset, KAP, no call
            Florida_Write_Reg(HPF1_EN,0x00);
            Florida_Write_Reg(RENDER_REG1,0x02);
            Florida_Write_Reg(RENDER_REG2,0x02);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x00);
            Florida_Write_Reg(HEADPHONE_REG, 0x01);
            Florida_Write_Reg(KAP_REG, 0x01);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(ON);
            break;
        case 0x05:
        case 0x0d:                                                  // Beam, flt 2, no call
            Florida_Write_Reg(HPF1_EN,0x01);
            Florida_Write_Reg(RENDER_REG1,0x01);
            Florida_Write_Reg(RENDER_REG2,0x01);
            Florida_Write_Reg(FLT_BANK1_REG,0x01);
            Florida_Write_Reg(FLT_BANK2_REG,0x01);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x01);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x01);
            Florida_Write_Reg(HEADPHONE_REG, 0x00);
            Florida_Write_Reg(KAP_REG, 0x00);;
            LED_MODE_ORG(ON);
            break;
        case 0x11:
        case 0x19:                                                  // Beam, flt 1, call
            Florida_Write_Reg(HPF1_EN,0x01);
            Florida_Write_Reg(RENDER_REG1,0x00);
            Florida_Write_Reg(RENDER_REG2,0x00);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x01);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x01);
            Florida_Write_Reg(HEADPHONE_REG, 0x00);
            Florida_Write_Reg(KAP_REG, 0x00);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(ON);
            break;
        case 0x15:
        case 0x1d:                                                  // Beam, flt 2, call
            Florida_Write_Reg(HPF1_EN,0x01);
            Florida_Write_Reg(RENDER_REG1,0x00);
            Florida_Write_Reg(RENDER_REG2,0x00);
            Florida_Write_Reg(FLT_BANK1_REG,0x01);
            Florida_Write_Reg(FLT_BANK2_REG,0x01);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x01);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x01);
            Florida_Write_Reg(HEADPHONE_REG, 0x00);
            Florida_Write_Reg(KAP_REG, 0x00);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(ON);
            break;
        case 0x12:
        case 0x16:
        case 0x1a:
        case 0x1e:                                                  // Headset, call
            Florida_Write_Reg(HPF1_EN,0x00);
            Florida_Write_Reg(RENDER_REG1,0x02);
            Florida_Write_Reg(RENDER_REG2,0x02);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x00);
            Florida_Write_Reg(HEADPHONE_REG, 0x01);
            Florida_Write_Reg(KAP_REG, 0x00);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(ON);
            break;
        case 0x03:
        case 0x07:                                                  // Bar mode, no KAP, no call
            Florida_Write_Reg(HPF1_EN,0x01);
            Florida_Write_Reg(RENDER_REG1,0x03);
            Florida_Write_Reg(RENDER_REG2,0x03);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x00);
            Florida_Write_Reg(HEADPHONE_REG, 0x00);
            Florida_Write_Reg(KAP_REG, 0x00);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(OFF);
            break;
        case 0x0b:
        case 0x0f:                                                  // Bar mode, KAP, no call
            Florida_Write_Reg(HPF1_EN,0x01);
            Florida_Write_Reg(RENDER_REG1,0x03);
            Florida_Write_Reg(RENDER_REG2,0x03);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x00);
            Florida_Write_Reg(HEADPHONE_REG, 0x00);
            Florida_Write_Reg(KAP_REG, 0x01);
            LED_MODE_GRN(ON);
            LED_MODE_ORG(ON);
            break;
        default:
            printf("\n\rIllegal mode - reset mode to 0x01\n\r");
            DSP_Status = 0x01;
            Florida_Write_Reg(HPF1_EN,0x01);
            Florida_Write_Reg(RENDER_REG1,0x01);
            Florida_Write_Reg(RENDER_REG2,0x01);
            Florida_Write_Reg(FLT_BANK1_REG,0x00);
            Florida_Write_Reg(FLT_BANK2_REG,0x00);
            Florida_Write_Reg(FLT_UPDATE1_REG,0x01);
            Florida_Write_Reg(FLT_UPDATE2_REG,0x01);
            Florida_Write_Reg(HEADPHONE_REG, 0x00);
            Florida_Write_Reg(KAP_REG, 0x00);
            LED_MODE_GRN(ON);
            break;
    }
}


void adjustIPVolume(int dir)                                        // Adjust volume across all outputs using mixer input control
{
    WORD Sys_Vol;
 
    Sys_Vol = (Florida_Read(HP1L_VOLUME)  & 0x00ff);

    if (dir == VOL_RESET)
    {
        Sys_Vol = 0x80;                                             // Set default volume
        T4CONbits.TON = 1;                                          // Start timer 4/5
    }	

    if (dir == VOL_INIT)
    {
        Sys_Vol = nv_buffer[0];
        if ((Sys_Vol > MAX_VOL) || (Sys_Vol < MIN_VOL))
            Sys_Vol = 0x80;                                         // Set default if invalid volume read from EEPROM
    }

    if ((dir == VOL_UP) && (Sys_Vol < (MAX_VOL + sub_offset)))
    {
        Sys_Vol+=6;
        nv_buffer[0] = Sys_Vol;
		TMR4 = 0;													// Reset timer 4/5
		TMR5 = 0;
        T4CONbits.TON = 1;                                          // Start timer 4/5
    }

    if ((dir == VOL_DOWN) && (Sys_Vol > MIN_VOL))
    {
        Sys_Vol-=6;
        nv_buffer[0] = Sys_Vol;
		TMR4 = 0;													// Reset timer 4/5
		TMR5 = 0;
        T4CONbits.TON = 1;                                          // Start timer 4/5
    }

    printf("\r\nCurrent vol: %ddB\r\n", (Sys_Vol/2)-0x40);
    Florida_Write_Reg(HP1L_VOLUME, Sys_Vol);
    Florida_Write_Reg(HP1R_VOLUME, Sys_Vol);
    Florida_Write_Reg(HP2L_VOLUME, Sys_Vol);
    Florida_Write_Reg(HP2R_VOLUME, Sys_Vol);
    Florida_Write_Reg(HP3L_VOLUME, Sys_Vol);
    Florida_Write_Reg(HP3R_VOLUME, Sys_Vol);
    Florida_Write_Reg(SPKL_VOLUME, Sys_Vol);
    Florida_Write_Reg(SPKL_VOLUME, Sys_Vol);
    Florida_Write_Reg(AIF1_TX1_1_VOLUME, Sys_Vol);
    Florida_Write_Reg(AIF1_TX2_1_VOLUME, Sys_Vol);
    Florida_Write_Reg(AIF2_TX1_1_VOLUME, Sys_Vol + sub_offset);
    Florida_Write_Reg(AIF2_TX2_1_VOLUME, Sys_Vol);
}


void test_tone(void)
{
    Florida_Write_Reg(0x020, 0x01);                                 // 1kHz on
    DelayMs(500);                                                   // Delay for minimum 500ms tone
    do
    {
    } while (getcht(35) == -2);                                     // wait for any key press
    Florida_Write_Reg(0x020, 0x00);                                 // 1kHz off

}


BYTE sloadCores(void)
{
    int core = 1;
    BYTE dspCoreLoadedFlags = 0;
    FRESULT st;
    FILINFO fno;
    char filename[9];
    do
    {
        sprintf(filename, "fw%d.wmfw", core);
        st = f_stat(filename, &fno);
        if (st == FR_OK)
        {
            st = f_open(&fdst, filename, FA_OPEN_EXISTING | FA_READ);
            if (st == FR_OK)
            {
                load_firmware(core, f_size(&fdst), 0);
                f_close(&fdst);
                dspCoreLoadedFlags |= (1 << (core - 1));
            }
        }
        if ((core < 3) && (fw_header_en))
        {
            printf("Loading firmware secure block %d\n\r", core);
            load_fw_header(core);
        }
        if (core == 2)                                              // skip core 3
            core++;
        core++;
    } while (core < 5);
    return (dspCoreLoadedFlags);
}
 //************************ END OF FILE ****************************************