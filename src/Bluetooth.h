/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Definitions for Bluetooth MOdule Functions
 *
 *  Jan 2015 - SLL   First Release
 *
 ******************************************************************************/

#ifndef BLUETOOTH_H
#define	BLUETOOTH_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* BLUETOOTH_H */

void re_pairing_mode(void);
void pairing_mode(void);
void max_level(void);
void norm_level(void);
void SetBluetooth(void);
BYTE query_call(void);
void end_call(void);
void answer_call(void);
void bt_rst(void);
void bt_settings(void);
void bt_default(void);
void bt_version(void);

 /************************ END OF FILE ****************************************/