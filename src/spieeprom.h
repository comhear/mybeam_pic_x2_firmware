/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Definition for SPI EEPROM functions
 *
 *  May 2014 - P Gomme   First Release
 *
 ******************************************************************************/

#include <spi.h>
#include "integer.h"

/************************************************************************
* EEPROM Commands                                                       *
*                                                                       *    
************************************************************************/
#define EEPROM_PAGE_SIZE    		(unsigned)256
#define EEPROM_CMD_READ     		(unsigned)0x03
#define EEPROM_CMD_WRITE    		(unsigned)0x02
#define EEPROM_CMD_WRDI     		(unsigned)0x04
#define EEPROM_CMD_WREN     		(unsigned)0x06
#define EEPROM_CMD_RDSR1    		(unsigned)0x05
#define EEPROM_CMD_WRSR     		(unsigned)0x01
#define EEPROM_CMD_MANID    		(unsigned)0x90
#define EEPROM_CMD_JEDEC    		(unsigned)0x9F
#define EEPROM_CMD_ERASE_CLUSTER        (unsigned)0x20  // 4Kb
#define EEPROM_CMD_ERASE_CHIP		(unsigned)0xC7

#define Lo(X)   (unsigned char)(X&0x00ff)
#define Hi(X)   (unsigned char)((X>>8)&0x00ff)

#define mEEPROMSSLow()      LATBbits.LATB8 = 0;
#define mEEPROMSSHigh()     LATBbits.LATB8 = 1;


extern unsigned char EEPROMReadStatus1(void);
extern void EEPROMWriteEnable(void);
void EEPROMEraseCluster(unsigned long);
void EEPROM_wait_for_write_complete(void); // must be done if prevoius was a write before next EERPROM command
void EEPROM_Read_Sector(unsigned long Address, BYTE *buff);
void EEPROM_Write_Sector(unsigned long address, const BYTE *buff);
void EEPROMEraseChip(void);
void EEPROMReadManID(void);
void EEPROMReadWriteTest(void);
void EEPROMChipErase(void);
void EEPROMWriteByte(unsigned Data, unsigned long Address);
unsigned EEPROMReadByte(unsigned long Address);


/*------------------------------ END OF FILE ---------------------------------*/
