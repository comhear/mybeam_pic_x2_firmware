/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Bluetooth Module Functions
 *
 *  Jan 2015 - SLL   First Release - functions separated from main.c
 *  Aug 2015 - SLL   Remove LED function calls as should only be in main.c
 *  Sep 2015 - SLL   Changed counter variable i to long int from unsigned int to stop occasional hanging
 *  Dec 2015 - SLL   Fixed Settings command to read back 14 lines instead of 1
 *
 ******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdio.h>
#include <libpic30.h>
#include "xmodem.h"
#include "hwdefs.h"
#include "Bluetooth.h"

void DelayMs(WORD);

extern unsigned char Rx2_data[];
extern unsigned int buffer2_count;
extern BOOL rx2_complete;

void mux_pc(void)
{
    DelayMs(5);
}


void mux_bt(void)
{
    DelayMs(5);
}


void bt_ack(void)
{
    long int i = 0;

    while (!(rx2_complete) && (i < 500000))                         // Wait for received character but timeout eventually
    {
        __delay32(160);                                             // 10us delay
        i++;
    }

    BT_RX_INT_OFF;                                                  // Disable RX interrupt
    Rx2_data[buffer2_count] = 0;                                    // add NUL at end of buffer just in case

    mux_pc();
    printf("\n\r%s\r\n", Rx2_data);
    mux_bt();

    for(i=0; i < buffer2_count; i++)
        Rx2_data[i] = NUL;

    buffer2_count = 0;
    rx2_complete = FALSE;
    BT_RX_INT_ON;                                                   // Enable RX interrupt
}


void re_pairing_mode(void)
{
    unsigned char Txdata[] = "B\r\0";

    printf("\n\rBT Re-pairing");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);
    bt_ack();
    DelayMs(1000);

    BT_CMD(HIGH);
    bt_ack();
    mux_pc();                                                       // Set UART to PC
}


void pairing_mode(void)
{
    unsigned char Txdata[] = "@,1\r\0";

    printf("\n\rEntering Pairing Mode");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);
    bt_ack();

    BT_CMD(HIGH);
    bt_ack();
    mux_pc();                                                       // Set UART to PC
}


void max_level(void)
{
    unsigned char Txdata[] = "SS,0F\r\0";

    printf("\n\rSetting Max Level");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);
    bt_ack();

    BT_CMD(HIGH);
    bt_ack();
    mux_pc();                                                       // Set UART to PC
}


void norm_level(void)
{
    unsigned char Txdata[] = "SS,0A\r\0";

    printf("\n\rSetting Norm Level");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);
    bt_ack();

    BT_CMD(HIGH);
    bt_ack();
    mux_pc();                                                       // Set UART to PC
}


void SetBluetooth(void)
{
    unsigned char Txdata1[] = "S%,0402\r\0";                        // Set modes
    unsigned char Txdata2[] = "S-,MyBeam\r\0";                      // Set name
    unsigned char Txdata3[] = "ST,0C\r\0";                          // Set tone volume
    unsigned char Txdata4[] = "SS,0F\r\0";                          // Set max output level
    unsigned char Txdata5[] = "R,1\r\0";                            // Reset module to establish settings

    printf("\n\rBT Setup");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata1);
    bt_ack();

    BT_SEND(Txdata2);
    bt_ack();

    BT_SEND(Txdata3);
    bt_ack();

    BT_SEND(Txdata4);
    bt_ack();

    BT_SEND(Txdata5);
    bt_ack();

    DelayMs(500);
    BT_CMD(HIGH);
    bt_ack();
    mux_pc();                                                       // Set UART to PC
}


void bt_default(void)
{
    unsigned char Txdata1[] = "SF,1\r\0";                           // set factory defaults
    unsigned char Txdata2[] = "R,1\r\0";                            // reset module to establish settings

    printf("\n\rBT Factory Reset");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata1);
    bt_ack();

    BT_SEND(Txdata2);
    bt_ack();

    DelayMs(500);
    BT_CMD(HIGH);
    bt_ack();
    mux_pc();                                                       // Set UART to PC
}


void bt_settings(void)
{
    unsigned char Txdata[] = "D\r\0";
    long int i = 0;
    BYTE x;

    printf("\n\rBT Display Settings");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);

    for (x = 0; x < 28; x++)                                        // Get 14 lines of response (one CR + LF per line)
    {
        while (!(rx2_complete) && (i < 500000))                     // Wait for received characters but timeout eventually
        {
            __delay32(160);                                         // Delay waiting for rx buffer to fill
            i++;
        }
        rx2_complete = FALSE;
    }

    BT_RX_INT_OFF;                                                  // Disable RX interrupt
    Rx2_data[buffer2_count] = 0;                                    // add NUL at end of buffer just in case

    mux_pc();                                                       // Set UART to PC
    printf("\n\r%s\r\n", Rx2_data);
    mux_bt();                                                       // Set UART to BT

    for(i=0; i < buffer2_count; i++)
        Rx2_data[i] = NUL;
    buffer2_count = 0;
    rx2_complete = FALSE;
    BT_RX_INT_ON;                                                   // Enable RX interrupt

    BT_CMD(HIGH);
    bt_ack();
}


void bt_version(void)
{
    unsigned char Txdata[] = "V\r\0";
    long int i = 0;

    printf("\n\rBT Display Version");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);

    while (!(rx2_complete) && (i < 500000))                         // Wait for received characters but timeout eventually
    {
        __delay32(160);                                             // Delay waiting for rx buffer to fill
        i++;
    }

    BT_RX_INT_OFF;                                                  // Disable RX interrupt
    Rx2_data[buffer2_count] = 0;                                    // add NUL at end of buffer just in case

    mux_pc();                                                       // Set UART to PC
    printf("\n\r%s\r\n", Rx2_data);
    mux_bt();                                                       // Set UART to BT

    for(i=0; i < buffer2_count; i++)
        Rx2_data[i] = NUL;
    buffer2_count = 0;
    rx2_complete = FALSE;
    BT_RX_INT_ON;                                                   // Enable RX interrupt

    BT_CMD(HIGH);
    bt_ack();
    mux_pc();
}


void answer_call(void)
{
    unsigned char Txdata[] = "C\r\0";

    printf("\n\rBT Call Answer");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);
    bt_ack();

    BT_CMD(HIGH);
    bt_ack();
    mux_pc();                                                       // Set UART to PC
}


void end_call(void)
{
    unsigned char Txdata[] = "E\r\0";

    printf("\n\rBT Call End");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);
    bt_ack();

    BT_CMD(HIGH);
    bt_ack();
    mux_pc();                                                       // Set UART to PC
}


BYTE query_call(void)                                               // Queries the Event/Status register and returns 1 if in Active Call
{
    unsigned char Txdata[] = "Q\r\0";
    unsigned char call_status;
    long int i = 0;

    printf("\n\rBT Query Call");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);                                                    // Set CMD line active
    bt_ack();

    BT_SEND(Txdata);

    while (!(rx2_complete) && (i < 500000))                         // Wait for received characters but timeout eventually
    {
        __delay32(160);                                             // Delay waiting for rx buffer to fill
        i++;
    }

    BT_RX_INT_OFF;                                                  // Disable RX interrupt
    Rx2_data[buffer2_count] = 0;                                    // add NUL at end of buffer just in case
    call_status = Rx2_data[4];

    mux_pc();                                                       // Set UART to PC
    printf("\n\r%s\r\n", Rx2_data);
    mux_bt();                                                       // Set UART to BT

    for(i=0; i < buffer2_count; i++)
        Rx2_data[i] = NUL;
    buffer2_count = 0;
    rx2_complete = FALSE;
    BT_RX_INT_ON;                                                   // Enable RX interrupt

    BT_CMD(HIGH);
    bt_ack();
    mux_pc();

    return(call_status);
}


void bt_rst(void)
{
    unsigned char Txdata[] = "R,1\r\0";
    long int i = 0;

    printf("\n\rBT Reset");

    mux_bt();                                                       // Set UART to BT
    BT_CMD(LOW);
    bt_ack();

    BT_SEND(Txdata);
    while (!(rx2_complete) && (i < 500000))                         // Wait for received characters but timeout eventually
    {
        __delay32(160);                                             // Delay waiting for rx buffer to fill
        i++;
    }

    BT_RX_INT_OFF;                                                  // Disable RX interrupt
    Rx2_data[buffer2_count] = 0;                                    // add NUL at end of buffer just in case

    mux_pc();                                                       // Set UART to PC
    printf("\n\r%s\r\n", Rx2_data);
    mux_bt();                                                       // Set UART to BT

    for(i=0; i < buffer2_count; i++)
        Rx2_data[i] = NUL;
    buffer2_count = 0;
    rx2_complete = FALSE;
    BT_RX_INT_ON;                                                   // Enable RX interrupt

    DelayMs(500);
    BT_CMD(HIGH);
    DelayMs(50);                                                    // No need for ack() as nothing is coming back
    mux_pc();                                                       // Set UART to PC
}
 /************************ END OF FILE ****************************************/
