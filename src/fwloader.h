/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Definitions for firmware download functions
 *
 *  May 2014 - P Gomme   First Release
 *
 ******************************************************************************/

#ifndef FWLOADER_H
#define	FWLOADER_H

#ifdef	__cplusplus
extern "C" {
#endif


#ifdef	__cplusplus
}
#endif


#ifndef LOAD_FIRMWARE_H
#define LOAD_FIRMWARE_H

//#include <GenericTypeDefs.h>

/* D E F I N I T I O N S *******************************************/
#ifdef      FIRMWARE_DEBUG
#define     FIRMWARE_DEBUG_PRINTF(x)            {printf x;}
#else
#define     FIRMWARE_DEBUG_PRINTF(x)
#endif

/** Firmware Magic ID -- WMFM */
#define     FIRMWARE_MAGIC_ID                   0x574D4657

/** Default DSP firmware checksum */
#define     DSP_DEFAULT_CHECKSUM                0xFFFFFFFF

/** Florida device register map REV. D -- DSP memory locations . */

    /* ADSP memory location registers */
#define     DSP1_DM_X_START      0x190000
#define     DSP1_DM_Y_START      0x1A8000
#define     DSP1_PM_START        0x100000
#define     DSP1_ZM_START        0x180000

#define     DSP2_DM_X_START      0x290000
#define     DSP2_DM_Y_START      0x2A8000
#define     DSP2_PM_START        0x200000
#define     DSP2_ZM_START        0x280000

#define     DSP3_PM_START        0x300000
#define     DSP3_ZM_START        0x380000
#define     DSP3_DM_X_START      0x390000
#define     DSP3_DM_Y_START      0x3A8000

#define     DSP4_DM_X_START      0x490000
#define     DSP4_DM_Y_START      0x4A8000
#define     DSP4_PM_START        0x400000
#define     DSP4_ZM_START        0x480000

/* Number of registers for one field: 3 for PM code, 2 for DM & ZM data */
#define     NUM_REGISTER_PER_PM_CODE                3
#define     NUM_REGISTER_PER_DM_DATA                2
#define     NUM_REGISTER_PER_ZM_DATA                2

/* Maximum number of register to write per I2C block */
#define     DSP_BlOCK_CHUNK                         32

#define     FW_MGICID_ID_LENGTH                     4
#define     FW_HEADER_LENGTH_LENGTH                 4

#define     FW_DATA_START_OFFSET_LENGTH             3
#define     FW_DATA_MEMMORY_REGION_LENGTH           1
#define     FW_DATA_DATA_LENGTH_LENGTH              4

#define     FIRMWARE_DOWNLOAD_DISABLED              0
#define     FIRMWARE_DOWNLOAD_ENABLED               1


/* S T R U C T U R E S *********************************************/

/** DSP firmware file header structure. */
typedef struct _HeaderInfo
{
    /** DSP Firmware magic ID - WMFM */
    UINT32 MagicId;

    /** Total firmware header length (in bytes) */
    UINT32 HeaderLength;

    /** File format version - always be zero */
    UINT8 FileFormat;

    /** Target core - 1 for ADSP1 */
    UINT8 TargetCore;

    /** Revision of the DSP core */
    UINT16 CoreVersion;

    /** Data memory size */
    UINT32 DMSize;

    /** Program memory size */
    UINT32 PMSize;

    /** Coefficient memory size */
    UINT32 ZMSize;

    /** The time the firmware file was created - high 32 bit word */
    UINT32 CreationTimeStampHighWord;

    /** The time the firmware file was created - low 32 bit word */
    UINT32 CreationTimeStampLowWord;

    /** Checksum is based on the whole firmware file */
    UINT32 Checksum;

    /** This test field provides the readable information on this firmware */
    UINT32 InfoText;
} HeaderInfo;

/** Data block regions definition for DSP firmware file */
typedef enum _ADSPMemoryRegion
{
    PM_REGION = 2,      /*!< Program memory section. */
    DM_REGION_X = 5,      /*!< Data memory section. */
    ZM_REGION = 4,      /*!< Coefficient memory section. */
    DM_REGION_Y = 6,      /*!< Data memory section. */
   TEXT_REGION = 0xFF  /*!< Informational text section. */

} ADSPMemoryRegion;

/** Firmware file data block header structure. */
typedef struct _DataBlock
{
    /** Memory start offset from the memory starting address. */
    DWORD StartOffset;

    /** Memory region: PM, DM or ZM */
    ADSPMemoryRegion MemoryRegion;

    /** Length of data in this block (in bytes) */
    DWORD DataLength;
} DataBlock;

typedef unsigned char UINT8;

void load_firmware(UINT8 core, 	DWORD file_size, BYTE verify);
int WriteDataBlock(BYTE *user_buffer, DataBlock *Data, BYTE core, BYTE verify);
UINT32 ReadWordBigEndian(UINT8 *ptr, UINT8 count);
int writeFileToMemory(int core, DWORD baseAddress, char *filename);


/* E X T E R N S ***************************************************/
/**
 * @defgroup Firmware_download_grp DSP firmware file handler
 *
 * DSP Firmware file handling functions.
 *
 * @{
 */
/**
 * @brief Initialise the firmware downloading task.
 *
 * @return void
 *
 */
void InitFirmwareTasks(void);

/** @} */

#endif /*LOAD_FIRMWARE_H*/

#endif	/* FWLOADER_H */

 /************************ END OF FILE ****************************************/