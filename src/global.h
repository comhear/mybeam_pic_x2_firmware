/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Global Definitions (for debug etc)
 *
 *  May 2014 - P Gomme   First Release
 *  Jan 2015 - SLL       Removed unused definitions and updated SW version to 3.0
 *  Jul 2015 - SLL       Updated SW version to 3.2
 *  Sep 2015 - SLL       Updated SW version to 3.3
 *  Oct 2015 - SLL       Updated SW version to 3.4 and add PARAM_LIMIT
 *
 ******************************************************************************/

#define SW_VERSION "v3.4"                                           // Used in displayed SW Banner

#define PARAM_LIMIT 3                                               // If parameter file revision less than this the code will MKFS

#define TRACE       0
#define TRACE1      0
#define TRACE2      0
#define DEL         0
#define TRACE_SMALL 0
#define TRACE_FLOW  0
#define FIRMWARE    0

/*------------------------------ END OF FILE ---------------------------------*/