/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

#ifndef BOOTLOADER_H
#define	BOOTLOADER_H

// Flag to indicate to the bootloader when it should be launched. This is
// defined as:
//   int __attribute__((persistent,address(LAUNCH_BOOTLOADER_FLAG)) launch_bootloader;
// It is initialized to 0 on power-on, then set to 1 when the user elects to
// launch the bootloader from the main PIC code.
//
// To use this, add DECLARE_LAUNCH_BOOTLOADER somewhere in the C source for the app.
// Use set_launch_bootloader(1) to set the flag, and set_launch_bootloader(0) to clear
// it.
#define LAUNCH_BOOTLOADER_ADDR (0x4800-2)
#define DECLARE_LAUNCH_BOOTLOADER  volatile int __attribute__((persistent,address(LAUNCH_BOOTLOADER_ADDR))) launch_btldr;
extern volatile int launch_btldr;

#ifdef	__cplusplus
extern "C" {
#endif

    inline void set_launch_bootloader(int value)
    {
        launch_btldr = value;
    }

    inline int should_launch_bootloader()
    {
        return launch_btldr;
    }


#ifdef	__cplusplus
}
#endif

#endif	/* BOOTLOADER_H */

