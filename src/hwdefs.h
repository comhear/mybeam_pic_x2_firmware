/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Hardware Definitions File
 *
 *  May 2014 - P Gomme   First Release
 *  Oct 2014 - SLL       Multiple updates for the following functiosn;
 *                        Command Register definations
 *                        USB IC specific definitions
 *                        Presets for parameter file defaults
 *                        SW version string
 *  Dec 2014 - SLL       Update for v3 PIC device
 *  Apr 2015 - SLL       Add compiler switch options for debug UART and NV delay definition
 *  Jun 2015 - SLL       Updates include;
 *                        Replace DSP_REVISION value with individual major and minor values for all 4 DSPs
 *                        Define SPARE IO pins for self test
 *                        Add watchdog and self test register definitions
 * Aug 2015  - SLL       Add FLT_HDR_LEN for definition of filter header array length
 *
 ******************************************************************************/

#ifndef HWDEFS_H
#define	HWDEFS_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* HWDEFS_H */
/*
From PIC24FJ256GA110 FAMILY datasheet - DS39905E-page 132 - 2010 Microchip Technology Inc.

TABLE 10-3: SELECTABLE OUTPUT SOURCES (MAPS FUNCTION TO OUTPUT)
*/

#define  _NULL              0
#define  C1OUT              1
#define  C2OUT              2
#define  U1TX               3
#define  nU1RTS             4
#define  U2TX               5
#define  nU2RTS             6
#define  SDO1               7
#define  SCK1OUT            8
#define  SS1OUT             9
#define  SDO2               10
#define  SCK2OUT            11
#define  SS2OUT             12
#define  _OC1               18
#define  _OC2               19
#define  _OC3               20
#define  _OC4               21
#define  _OC5               22
#define  _OC6               23
#define  _OC7               24
#define  _OC8               25
#define  U3TX               28
#define  nU3RTS             29
#define  U4TX               30
#define  nU4RTS             31
#define  SDO3               32
#define  SCK3OUT            33
#define  SS3OUT             34
#define  _OC9               35
#define  C3OUT              36

// I/O line usage
// PORT A - all defined as outputs

// PORT B
#define RESET_8280          (1<<5)

// PORT C
#define IRQ_8280            (1<<3)

// PORT D
#define HEADIN              (1<<13)
#define BT_EVENT            (1<<14)
#define CABLE               (1<<15)

// PORT E - all defined as outputs

// PORT F
#define BT_COMMAND          (1<<0)

// PORT G
#define SW_PWR_PAIR         (1<<0)
#define SW_VOLUP            (1<<1)
#define SW_VOLDN            (1<<2)
#define SW_MODE             (1<<3)
#define SW_PROCESS          (1<<7)
#define SW_CALLPU           (1<<8)

#define BUTTON_REPEAT_DELAY 300                                     // time to wait before detecting next volume button press
#define TWIN_PRESS_WAIT     200                                     // time to wait for second of twin volume key press
#define LONG_PRESS_WAIT     300                                     // time to wait for long press detection
#define TEST_NOISE_LENGTH   40                                      // time of each speaker test noise in 50's of milliseconds

#define ADSP1_CORE1         1
#define ADSP1_CORE2         2
#define ADSP1_CORE3         3
#define ADSP1_CORE4         4

#define HALT while(1){}

#define PC_nBT(st)          LATBbits.LATB4 = st                     // Not used for v2 except to avoid chaning Bluetooth.c
#define RST_8280(st)        LATBbits.LATB5 = st

#define DACMUTE(st)         LATDbits.LATD0 = st
#define AMPMUTE(st)         LATDbits.LATD1 = st

#define LED_PROCESS(st)     LATEbits.LATE0 = st
#define LED_CHG(st)         LATEbits.LATE1 = st
#define LED_PON(st)         LATEbits.LATE2 = st
#define LED_MODE_ORG(st)    LATEbits.LATE3 = st
#define LED_MODE_GRN(st)    LATEbits.LATE4 = st
#define USB_VOLDN(st)       LATEbits.LATE5 = st
#define USB_VOLUP(st)       LATEbits.LATE6 = st
#define USB_PMUTE(st)       LATEbits.LATE7 = st
#define USB_RMUTE(st)       LATEbits.LATE8 = st
#define USB_RST(st)         LATEbits.LATE9 = st

#define BT_CMD(st)          LATFbits.LATF0 = st
#define CS_8280(st)         LATFbits.LATF3 = st

#define ON (0)
#define OFF (1)

#define HIGH                (1)
#define LOW                 (0)

#define VOL_UP              (0)
#define VOL_DOWN            (1)
#define VOL_INIT            (2)
#define VOL_RESET           (3)

#define MAX_EXT_LEN         5                                       // Max len of filename extension (including '.')

enum { TBS, TBB, TBK, WDB };                                        // MyBeam Control Messages

#define FILTER_START_1      0x190400                                // MyBeam Filter coefficient start locations
#define FILTER_START_2      0x191800

#define TONE_START_1        0x490400                                // Volume beep wav data start locations
#define TONE_START_2        0x491800

#define HPF1_EN             0x1a801d
#define DSP1_REVISION_MAJOR 0x190012
#define DSP1_REVISION_MINOR 0x190013
#define DSP2_REVISION_MAJOR 0x290012
#define DSP2_REVISION_MINOR 0x290013
#define DSP3_REVISION_MAJOR 0x390012
#define DSP3_REVISION_MINOR 0x390013
#define DSP4_REVISION_MAJOR 0x490012
#define DSP4_REVISION_MINOR 0x490013
#define DSP1_WATCHDOG       0x190203
#define DSP2_WATCHDOG       0x290203
#define RENDER_REG1         0x190205
#define RENDER_REG2         0x290205
#define FLT_BANK1_REG       0x190207
#define FLT_BANK2_REG       0x290207
#define FLT_UPDATE1_REG     0x190209
#define FLT_UPDATE2_REG     0x290209
#define DSP4_WATCHDOG       0x49004f
#define ALGO_EN             0x49004d
#define HEADPHONE_REG       0x490051
#define SUB_CONNECT_REG     0x490053
#define ALERT_REG           0x490055
#define KAP_REG             0x490057
#define SPATIAL_REG         0x490059
#define CONTROL_REG         0x4a8001                                // Location of command control register from DSP
#define LENGTH_REG          0x4a8003                                // Location of command length register from DSP
#define DECODER_EN          0x4a8013
#define FLT_HDR_LEN         64                                      // Length of array for filter version and parameter description (max 256)
#define NV_DELAY            0x000a                                  // Delay time in approx seconds to write to EEPROM after last voluem key press

#define BT_SEND(dat)        while (BusyUART3()); putsUART3((unsigned int *) dat)
#define BT_RX_INT_OFF       IEC5bits.U3RXIE = 0
#define BT_RX_INT_ON        IEC5bits.U3RXIE = 1

 /************************ END OF FILE ****************************************/
