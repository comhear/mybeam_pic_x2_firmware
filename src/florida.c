/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  WM8280 specific communication functions
 *
 *  May 2014 - P Gomme   First Release
 *  October 2014  - SLL  Updates to rationalise multiple write & read functions
 *  December 2014 - SLL  Update for v3 PIC device
 *
 ******************************************************************************/


#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdio.h>
#include "integer.h"
#include "florida.h"
#include "hwdefs.h"
#include <spi.h>

extern void DelayMs(WORD delay);


void Write_Byte_SPI1(unsigned char Value)
{
    int Data = Value;
    while (SPI1STATbits.SPITBF);
    SPI1BUF = Data & 0xff;
    while (SPI1STATbits.SPIRBF == 0);
    SPI1BUF;
}


// Write address and keep CS low. Subsequent 16-bit values will use WM8280 auto-increment addressing
void Florida_Write_Address(DWORD address)
{
    CS_8280(LOW);
    Write_Byte_SPI1((BYTE) ((address >> 24) & 0x0000007f));         // MSB is the R/W bit which must be low to write
    Write_Byte_SPI1((BYTE) ((address >> 16) & 0x000000ff));
    Write_Byte_SPI1((BYTE) ((address >> 8) & 0x000000ff));
    Write_Byte_SPI1((BYTE) (address & 0x000000ff));
    Write_Byte_SPI1((BYTE) (0));                                    // padding
    Write_Byte_SPI1((BYTE) (0));                                    // padding
}


void Florida_Write_Finish(void)
{
    CS_8280(HIGH);
}


void Florida_Reset(void)
{
    Florida_Write_Reg(MISC_PAD_CTRL_1, 0x00);                       // Disable LDOENA Pull-Up and Pull-Down to stop "bus-keeper" function on LDOENA pin
    DelayMs(10);
    RST_8280(LOW);                                                  // Pull RESET active low on Florida
    DelayMs(100);
    RST_8280(HIGH);                                                 // Remove reset to Florida
    DelayMs(500);
}


void Florida_Write_Reg(DWORD address, WORD data)
{
    CS_8280(LOW);
    Write_Byte_SPI1((BYTE) ((address >> 24) & 0x0000007f));         // msb is the R/W bit which must be low to write
    Write_Byte_SPI1((BYTE) ((address >> 16) & 0x000000ff));
    Write_Byte_SPI1((BYTE) ((address >> 8) & 0x000000ff));
    Write_Byte_SPI1((BYTE) (address & 0x000000ff));
    Write_Byte_SPI1((BYTE) (0));                                    // padding
    Write_Byte_SPI1((BYTE) (0));                                    // padding
    Write_Byte_SPI1((BYTE) ((data >> 8) & 0x00ff));
    Write_Byte_SPI1((BYTE) (data & 0x00ff));
    CS_8280(HIGH);
}

WORD Florida_Read(DWORD address)
{
    WORD data;

    CS_8280(LOW);
    address |= (DWORD) (0x80000000);                                // MSB = 0 for Write, set to 1 for Read
    Write_Byte_SPI1((BYTE) ((address >> 24) & 0x000000ff));
    Write_Byte_SPI1((BYTE) ((address >> 16) & 0x000000ff));
    Write_Byte_SPI1((BYTE) ((address >>  8) & 0x000000ff));
    Write_Byte_SPI1((BYTE) (address & 0x000000ff));
    Write_Byte_SPI1((BYTE) (0));                                    // padding
    Write_Byte_SPI1((BYTE) (0));                                    // padding
    Write_Byte_SPI1((BYTE) (0x00));                                 // padding
    data=SPI1BUF << 8;
    Write_Byte_SPI1((BYTE) (0x00));                                 // padding
    data|=SPI1BUF; 
    CS_8280(HIGH);
    return (data);
}

//************************ END OF FILE ****************************************