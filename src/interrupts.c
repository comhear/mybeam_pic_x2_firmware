/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  Interrupt Service Routine
 *
 *  May 2014 - P Gomme   First Release
 *  Oct 2014 - SLL       Multiple updates adding the following functionality;
 *                         Headset detect
 *                         Interrupt driven switch detect and handling
 *  Dec 2014 - SLL       Modify UART interrupts for v3 system and remove button handlers
 *  Apr 2015 - SLL       Add compiler switching for debug UART and Timer3 INT for timer2/3 32-bit combination
 *  Jul 2015 - SLL       Multiple updates as follows
 *                         Replace Timer 2/3 with 4/5 to free timer 2 for security code
 *                         Change anable AIF2TX6 to enable Mic_Bias at headsert detect
 *                         Implement new control code and mode status register
 *  Sep 2015 - SLL       Add resistance to 8281 interrupt interfering with BT
 *
 ******************************************************************************/

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
#include <xc.h>
#elif defined(__C30__)
#if defined(__PIC24E__)
#include <p24Exxxx.h>
#elif defined (__PIC24F__)||defined (__PIC24FK__)
#include <p24Fxxxx.h>
#elif defined(__PIC24H__)
#include <p24Hxxxx.h>
#endif
#endif

#include <stdint.h>                                                 // Includes uint16_t definition
#include <stdbool.h>                                                // Includes true/false definition
#include <ports.h>
#include <spi.h>
#include <uart.h>
#include <stdio.h>
#include "hwdefs.h"
#include "florida.h"
#include "xmodem.h"


/******************************************************************************/
/* Interrupt Vector Options                                                   */
/******************************************************************************/
/*                                                                            */
/* Refer to the C30 (MPLAB C Compiler for PIC24F MCUs and dsPIC33F DSCs) User */
/* Guide for an up to date list of the available interrupt options.           */
/* Alternately these names can be pulled from the device linker scripts.      */
/*                                                                            */
/* PIC24F Primary Interrupt Vector Names:                                     */
/*                                                                            */
/* _INT0Interrupt      _IC4Interrupt                                          */
/* _IC1Interrupt       _IC5Interrupt                                          */
/* _OC1Interrupt       _IC6Interrupt                                          */
/* _T1Interrupt        _OC5Interrupt                                          */
/* _Interrupt4         _OC6Interrupt                                          */
/* _IC2Interrupt       _OC7Interrupt                                          */
/* _OC2Interrupt       _OC8Interrupt                                          */
/* _T2Interrupt        _PMPInterrupt                                          */
/* _T3Interrupt        _SI2C2Interrupt                                        */
/* _SPI1ErrInterrupt   _MI2C2Interrupt                                        */
/* _SPI1Interrupt      _INT3Interrupt                                         */
/* _U1RXInterrupt      _INT4Interrupt                                         */
/* _U1TXInterrupt      _RTCCInterrupt                                         */
/* _ADC1Interrupt      _U1ErrInterrupt                                        */
/* _SI2C1Interrupt     _U2ErrInterrupt                                        */
/* _MI2C1Interrupt     _CRCInterrupt                                          */
/* _CompInterrupt      _LVDInterrupt                                          */
/* _CNInterrupt        _CTMUInterrupt                                         */
/* _INT1Interrupt      _U3ErrInterrupt                                        */
/* _IC7Interrupt       _U3RXInterrupt                                         */
/* _IC8Interrupt       _U3TXInterrupt                                         */
/* _OC3Interrupt       _SI2C3Interrupt                                        */
/* _OC4Interrupt       _MI2C3Interrupt                                        */
/* _T4Interrupt        _U4ErrInterrupt                                        */
/* _T5Interrupt        _U4RXInterrupt                                         */
/* _INT2Interrupt      _U4TXInterrupt                                         */
/* _U2RXInterrupt      _SPI3ErrInterrupt                                      */
/* _U2TXInterrupt      _SPI3Interrupt                                         */
/* _SPI2ErrInterrupt   _OC9Interrupt                                          */
/* _SPI2Interrupt      _IC9Interrupt                                          */
/* _IC3Interrupt                                                              */
/*                                                                            */
/* PIC24H Primary Interrupt Vector Names:                                     */
/*                                                                            */
/* _INT0Interrupt      _SPI2Interrupt                                         */
/* _IC1Interrupt       _C1RxRdyInterrupt                                      */
/* _OC1Interrupt       _C1Interrupt                                           */
/* _T1Interrupt        _DMA3Interrupt                                         */
/* _DMA0Interrupt      _IC3Interrupt                                          */
/* _IC2Interrupt       _IC4Interrupt                                          */
/* _OC2Interrupt       _IC5Interrupt                                          */
/* _T2Interrupt        _IC6Interrupt                                          */
/* _T3Interrupt        _OC5Interrupt                                          */
/* _SPI1ErrInterrupt   _OC6Interrupt                                          */
/* _SPI1Interrupt      _OC7Interrupt                                          */
/* _U1RXInterrupt      _OC8Interrupt                                          */
/* _U1TXInterrupt      _DMA4Interrupt                                         */
/* _ADC1Interrupt      _T6Interrupt                                           */
/* _DMA1Interrupt      _T7Interrupt                                           */
/* _SI2C1Interrupt     _SI2C2Interrupt                                        */
/* _MI2C1Interrupt     _MI2C2Interrupt                                        */
/* _CNInterrupt        _T8Interrupt                                           */
/* _INT1Interrupt      _T9Interrupt                                           */
/* _ADC2Interrupt      _INT3Interrupt                                         */
/* _IC7Interrupt       _INT4Interrupt                                         */
/* _IC8Interrupt       _C2RxRdyInterrupt                                      */
/* _DMA2Interrupt      _C2Interrupt                                           */
/* _OC3Interrupt       _DCIErrInterrupt                                       */
/* _OC4Interrupt       _DCIInterrupt                                          */
/* _T4Interrupt        _U1ErrInterrupt                                        */
/* _T5Interrupt        _U2ErrInterrupt                                        */
/* _INT2Interrupt      _DMA6Interrupt                                         */
/* _U2RXInterrupt      _DMA7Interrupt                                         */
/* _U2TXInterrupt      _C1TxReqInterrupt                                      */
/* _SPI2ErrInterrupt   _C2TxReqInterrupt                                      */
/*                                                                            */
/* PIC24E Primary Interrupt Vector Names:                                     */
/*                                                                            */
/* __INT0Interrupt     __C1RxRdyInterrupt      __U3TXInterrupt                */
/* __IC1Interrupt      __C1Interrupt           __USB1Interrupt                */
/* __OC1Interrupt      __DMA3Interrupt         __U4ErrInterrupt               */
/* __T1Interrupt       __IC3Interrupt          __U4RXInterrupt                */
/* __DMA0Interrupt     __IC4Interrupt          __U4TXInterrupt                */
/* __IC2Interrupt      __IC5Interrupt          __SPI3ErrInterrupt             */
/* __OC2Interrupt      __IC6Interrupt          __SPI3Interrupt                */
/* __T2Interrupt       __OC5Interrupt          __OC9Interrupt                 */
/* __T3Interrupt       __OC6Interrupt          __IC9Interrupt                 */
/* __SPI1ErrInterrupt  __OC7Interrupt          __DMA8Interrupt                */
/* __SPI1Interrupt     __OC8Interrupt          __DMA9Interrupt                */
/* __U1RXInterrupt     __PMPInterrupt          __DMA10Interrupt               */
/* __U1TXInterrupt     __DMA4Interrupt         __DMA11Interrupt               */
/* __AD1Interrupt      __T6Interrupt           __SPI4ErrInterrupt             */
/* __DMA1Interrupt     __T7Interrupt           __SPI4Interrupt                */
/* __NVMInterrupt      __SI2C2Interrupt        __OC10Interrupt                */
/* __SI2C1Interrupt    __MI2C2Interrupt        __IC10Interrupt                */
/* __MI2C1Interrupt    __T8Interrupt           __OC11Interrupt                */
/* __CM1Interrupt      __T9Interrupt           __IC11Interrupt                */
/* __CNInterrupt       __INT3Interrupt         __OC12Interrupt                */
/* __INT1Interrupt     __INT4Interrupt         __IC12Interrupt                */
/* __AD2Interrupt      __C2RxRdyInterrupt      __DMA12Interrupt               */
/* __IC7Interrupt      __C2Interrupt           __DMA13Interrupt               */
/* __IC8Interrupt      __DMA5Interrupt         __DMA14Interrupt               */
/* __DMA2Interrupt     __RTCCInterrupt         __OC13Interrupt                */
/* __OC3Interrupt      __U1ErrInterrupt        __IC13Interrupt                */
/* __OC4Interrupt      __U2ErrInterrupt        __OC14Interrupt                */
/* __T4Interrupt       __CRCInterrupt          __IC14Interrupt                */
/* __T5Interrupt       __DMA6Interrupt         __OC15Interrupt                */
/* __INT2Interrupt     __DMA7Interrupt         __IC15Interrupt                */
/* __U2RXInterrupt     __C1TxReqInterrupt      __OC16Interrupt                */
/* __U2TXInterrupt     __C2TxReqInterrupt      __IC16Interrupt                */
/* __SPI2ErrInterrupt  __U3ErrInterrupt        __ICDInterrupt                 */
/* __SPI2Interrupt     __U3RXInterrupt                                        */
/*                                                                            */
/*                                                                            */
/* For alternate interrupt vector naming, simply add 'Alt' between the prim.  */
/* interrupt vector name '_' and the first character of the primary interrupt */
/* vector name.  There are no Alternate or 'Alt' vectors for the 24E family.  */
/*                                                                            */
/* For example, the vector name _ADC2Interrupt becomes _AltADC2Interrupt in   */
/* the alternate vector table.                                                */
/*                                                                            */
/* Example Syntax:                                                            */
/*                                                                            */
/* void __attribute__((interrupt,auto_psv)) <Vector Name>(void)               */
/* {                                                                          */
/*     <Clear Interrupt Flag>                                                 */
/* }                                                                          */
/*                                                                            */
/* For more comprehensive interrupt examples refer to the C30 (MPLAB C        */
/* Compiler for PIC24 MCUs and dsPIC DSCs) User Guide in the                  */
/* <compiler installation directory>/doc directory for the latest compiler    */
/* release.                                                                   */
/*                                                                            */
/******************************************************************************/

extern BYTE terminal;
extern BYTE DSP_Status;
extern BYTE old_mode;
extern BOOL Command_Locked;
extern BOOL auto_nman;
extern BOOL dev_mode;
extern BOOL bt_req;
extern BOOL mode_change;
extern BOOL first_time;

extern WORD data_in_index;
extern WORD data_out_index;
extern WORD receive_data_in_index;
extern WORD receive_data_out_index;
extern BYTE receive_buffer[RECEIVE_BUFFER_LEN];
extern WORD max_chars;
extern WORD chars_in_buffer;
extern BYTE key_code;

extern BOOL rx2_complete;
extern unsigned int buffer2_count;
extern unsigned char Rx2_data[];

void nv_write(void);
void DelayMs(WORD);

//******************************************************************************
//* Interrupt Routines                                                         *
//******************************************************************************

void __attribute__((interrupt, no_auto_psv)) _CNInterrupt(void)
{
    int i;
    BOOL headsetdetect;
    BYTE cmd_type, cmd_dat;
    BOOL bt_int = FALSE;

    if (CNEN2bits.CN20IE)
        bt_int = TRUE;

    CNEN2bits.CN20IE = 0;                                           // Disable BT_EVENT interrupt
    CNEN2bits.CN19IE = 0;                                           // Disable HEADIN interrupt
    CNEN3bits.CN47IE = 0;                                           // Disable IRQ_8280 interrupt

// ********** BT Interrupt *********
    if (!(PORTD & BT_EVENT) && (bt_int))
    {
        printf("\n\rBT interrupt!");
        if (bt_req == FALSE)
            bt_req = TRUE;
        goto End;
    }

// ******* Key Press Interrupt *******
    if (!(PORTG & SW_VOLUP))
    {
        do
        {
            for (i=0; i < TWIN_PRESS_WAIT; i++)
            {
                if (!(PORTG & SW_VOLDN))
                {
                    key_code = 7;                                   // AutoMan
                    i = (TWIN_PRESS_WAIT + 10);
                    while (!(PORTG & SW_VOLUP) || !(PORTG & SW_VOLDN));
                }
            DelayMs(1);
            }

            if (i < (TWIN_PRESS_WAIT + 10))
                key_code = 3;                                       // Vol+

            DelayMs(BUTTON_REPEAT_DELAY);
        } while (!(PORTG & SW_VOLUP));                              // Wait for switch release
    }

    if (!(PORTG & SW_VOLDN))
    {
        do
        {
            for (i=0; i < TWIN_PRESS_WAIT; i++)
            {
                if (!(PORTG & SW_VOLUP))
                {
                key_code = 7;                                       // AutoMan
                i = (TWIN_PRESS_WAIT + 10);
                while (!(PORTG & SW_VOLUP) || !(PORTG & SW_VOLDN));
                }
            DelayMs(1);
            }

            if (i < (TWIN_PRESS_WAIT + 10))
                key_code = 4;                                       // Vol-

            DelayMs(BUTTON_REPEAT_DELAY);
        } while (!(PORTG & SW_VOLUP));                              // Wait for switch release
     }

    if (!(PORTG & SW_MODE))
    {
        i = 0;
        do
        {
            i++;
            DelayMs(10);
            if (i == LONG_PRESS_WAIT)
                key_code = 6;                                       // Process
         } while (!(PORTG & SW_MODE) && (key_code != 6));

        if (i < LONG_PRESS_WAIT)
            key_code = 5;                                           // Mode

        DelayMs(10);
    }

    if (!(PORTG & SW_CALLPU))
    {
        i = 0;
        do
        {
            i++;
            DelayMs(10);
            if (i == LONG_PRESS_WAIT)
                key_code = 2;                                       // Pair
         } while (!(PORTG & SW_CALLPU) && (key_code !=2));

        if (i < LONG_PRESS_WAIT)
            key_code = 1;                                           // Pickup

        DelayMs(10);
    }

    if (!(PORTG & SW_PWR_PAIR))
    {
        printf(".\n\r");
    }

    if (!(PORTG & SW_PROCESS))
    {
        printf(",\n\r");
    }

// ******* Headset Interrupt *******
    if (!(PORTD & HEADIN))
        headsetdetect = TRUE;
    else
        headsetdetect = FALSE;

    if ((headsetdetect == TRUE) && ((DSP_Status & 0x03) != 0x02))
    {
        printf("\n\rHeadset inserted\n\r");

        if ((DSP_Status & 0x10) == 0x10)                            // If in call change mic gain for headphone and change return mode for end of call
        {
            Florida_Write_Reg(0x769, 0x0080);
            Florida_Write_Reg(0x300, 0x003f);                       // Enable mic inputs
            printf("\n\r0dB on Line Out for headset call");
        }
        old_mode = (DSP_Status & 0x03);
        DSP_Status = (DSP_Status & 0xf8);                           // Ensure that WDB mode is not saved as old mode
        DSP_Status = (DSP_Status | 0x02);                           // Set headset code in status register
        mode_change = TRUE;                                         // Flag to change mode
        AMPMUTE(LOW);                                               // Disable AMP output
    }

    if ((headsetdetect == FALSE) && ((DSP_Status & 0x03) == 0x02))
    {
        printf("\n\rHeadset removed\n\r");
        AMPMUTE(HIGH);                                              // Enable AMP output
        if ((DSP_Status & 0x10) == 0x10)                            // If in call change mic gain for headphone and change return mode for end of call
        {
            Florida_Write_Reg(0x769, 0x0060);
            Florida_Write_Reg(0x300, 0x003f);                       // Enable mic inputs
            printf("\n\r-16dB on Line Out optimised for array call");
        }
        DSP_Status = (DSP_Status & 0xf8);                           // Ensure that WDB mode is not saved as old mode
        DSP_Status = (DSP_Status | old_mode);                       // Return to previous mode (Beam, Bar or Array Call)
        mode_change = TRUE;                                         // Flag to change mode
    }

// ****** Florida Interrupt ******
    if (!(PORTC & IRQ_8280) && (first_time))                        // First timei initialisation (as DSP register never reads correctly first time)
    {
        printf("\r\n.");
        first_time = FALSE;
        Florida_Write_Reg(0x0c0f, 0x0400);                          // Clear IRQ back high
        goto End;
    }

    if (!(PORTC & IRQ_8280) && (auto_nman))
    {
        cmd_type = (Florida_Read(LENGTH_REG) & 0xFF);
        cmd_dat = (Florida_Read(CONTROL_REG) & 0xFF);               // Read Length Register set by DSP
        printf("\n\rInterrupt! DSP Code Length 0x%02x Data %02x", cmd_type,cmd_dat);

        if (cmd_type == 0x04)
        {
            switch (cmd_dat)
            {
                case TBS:
                    Florida_Write_Reg(SPATIAL_REG, 0x01);           // Set Spatial bit on
                    DSP_Status &= 0xfb;                             // Select Filter 1
                    break;
                case TBB:
                    Florida_Write_Reg(SPATIAL_REG, 0x00);           // Clear Spatial bit
                    DSP_Status &= 0xfb;                             // Select Filter 1
                    break;
                case TBK:
                    Florida_Write_Reg(SPATIAL_REG, 0x01);
                    DSP_Status &= 0xfb;                             // Select Filter 1
                    DSP_Status |= 0x08;                             // Set Spatial bit and activate KAP
                    break;
                case WDB:
                    DSP_Status |= 0x04;                             // Select Filter 2
                    break;
                default:
                    printf("Invalid Command Register value - no action taken\n\r");
                    break;
            }
            mode_change = TRUE;
        }
        else
        {
            printf("\n\rInvalid Command Register value - no action taken");
        }
    }

End:
    InputChange_Clear_Intr_Status_Bit;
    Florida_Write_Reg(0x0c0f, 0x0400);                              // Clear IRQ back high
    CNEN2bits.CN19IE = 1;                                           // Enable HEADIN interrupt
    if (!(dev_mode))                                                // Re-enble BT & 8280 interrupts only if not in development mode
    {
        CNEN2bits.CN20IE = 1;                                       // Enable BT_EVENT interrupt
        CNEN3bits.CN47IE = 1;                                       // Enable IRQ_8280 interrupt
    }
}


// ****** Interrupt sevice rotuine for timer 5 ******
void __attribute__((interrupt, no_auto_psv)) _T5Interrupt(void)
{
    printf("\n\rStoring NV data now\n\r");
    T4CONbits.TON = 0;                                              // Stop timer 4/5
    nv_write();                                                     // Store nv_buffer to EEPROM
    IFS1bits.T5IF = 0;                                              // Clear Interrupt status of timer5
}


// ****** Interrupt sevice rotuine for SPI1 Master ******
void __attribute__((interrupt, no_auto_psv)) _SPI1Interrupt(void)
{
    SPI1_Clear_Intr_Status_Bit;                                     // Clear Interrupt status of SPI1
}


void __attribute__((interrupt, no_auto_psv)) _SPI2Interrupt(void)
{
    SPI2_Clear_Intr_Status_Bit;                                     // Clear Interrupt status of SPI1
}


void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void)
{
    if (terminal == 1)
        if (IFS0bits.U1RXIF && U1STAbits.URXDA)                     // UART RX received character
        {
            receive_buffer[receive_data_in_index] = U1RXREG;

            if (receive_data_out_index <= receive_data_in_index)
                chars_in_buffer = receive_data_in_index - receive_data_out_index + 1;
            else
                chars_in_buffer = RECEIVE_BUFFER_LEN - (receive_data_out_index - receive_data_in_index) - 1;

            if (chars_in_buffer > max_chars)                        // Get max chars in buffer so far
                max_chars = chars_in_buffer;

            if (receive_data_in_index < (RECEIVE_BUFFER_LEN - 1))
                receive_data_in_index++;
            else
                receive_data_in_index = 0;
            dev_mode = TRUE;
            IFS0bits.U1RXIF = 0;                                    // U2RX_Clear_Intr_Status_Bit;
        }
        else
        {}
    else
        IEC0bits.U1RXIE = 0;
}


void __attribute__ ((interrupt,no_auto_psv)) _U2RXInterrupt(void)
{
    if (terminal == 2)
        if (IFS1bits.U2RXIF && U2STAbits.URXDA)                     // UART RX received character
        {
            receive_buffer[receive_data_in_index] = U2RXREG;

            if (receive_data_out_index <= receive_data_in_index)
                chars_in_buffer = receive_data_in_index - receive_data_out_index + 1;
            else
                chars_in_buffer = RECEIVE_BUFFER_LEN - (receive_data_out_index - receive_data_in_index) - 1;

            if (chars_in_buffer > max_chars)                        // Get max chars in buffer so far
                max_chars = chars_in_buffer;

            if (receive_data_in_index < (RECEIVE_BUFFER_LEN - 1))
                receive_data_in_index++;
            else
                receive_data_in_index = 0;
            dev_mode = TRUE;
            IFS1bits.U2RXIF = 0;                                    // U2RX_Clear_Intr_Status_Bit;
        }
        else
        {}
    else
        IEC1bits.U2RXIE = 0;
}


void __attribute__ ((interrupt,no_auto_psv)) _U3RXInterrupt(void)
{
    if (IFS5bits.U3RXIF && U3STAbits.URXDA)                         // UART RX received character
    {
        Rx2_data[buffer2_count] = U3RXREG;
        if ((buffer2_count > 0) && (((Rx2_data[buffer2_count] == NUL) || (Rx2_data[buffer2_count] == LF))|| (Rx2_data[buffer2_count] == CR)))
            rx2_complete = TRUE;
        buffer2_count++;
	IFS5bits.U3RXIF = 0;                                        // U3RX_Clear_Intr_Status_Bit;
    }
}

// ************************ END OF FILE ****************************************