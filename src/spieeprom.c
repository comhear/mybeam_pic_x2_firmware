/*******************************************************************************
 *
 * Copyright (c) 2014 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/*******************************************************************************
 *  SPI EEPROM functions
 *
 *  May 2014 - P Gomme   First Release
 *  October 2014 - SLL   Rationalised duplicated functions
 *
 ******************************************************************************/


#include "global.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "spieeprom.h"
#include "system.h"

void DelayMs(WORD delay);

void WriteByteEEPROM(unsigned char Data)
{
    while (SPI2STATbits.SPITBF);
    SPI2BUF = Data & 0xff;
    while (SPI2STATbits.SPIRBF == 0);
    SPI2BUF;
}


void EEPROMWriteEnable()
{
    mEEPROMSSLow();
    WriteByteEEPROM(EEPROM_CMD_WREN);
    mEEPROMSSHigh();
}


unsigned char EEPROMReadStatus(void)
{
    mEEPROMSSLow();
    while (SPI2STATbits.SPITBF);
    WriteByteEEPROM(EEPROM_CMD_RDSR1);
    WriteByteEEPROM(0x0);
    mEEPROMSSHigh();
    return (SPI2BUF);
}


void EEPROMWriteByte(unsigned Data, unsigned long Address)
{
    EEPROMWriteEnable();
    mEEPROMSSLow();
    WriteByteEEPROM(EEPROM_CMD_WRITE);
    WriteByteEEPROM((unsigned char) ((Address >> 16) & 0x000000ff));
    WriteByteEEPROM((unsigned char) ((Address >> 8) & 0x000000ff));
    WriteByteEEPROM((unsigned char) (Address & 0x000000ff));
    WriteByteEEPROM(Data);
    mEEPROMSSHigh();
    while ((EEPROMReadStatus() & 0x1) == 1);                        // Wait for completion of previous write operation
}


unsigned EEPROMReadByte(unsigned long Address)
{
    mEEPROMSSLow();
    WriteByteEEPROM(EEPROM_CMD_READ);
    WriteByteEEPROM((unsigned char) ((Address >> 16) & 0x000000ff));
    WriteByteEEPROM((unsigned char) ((Address >> 8) & 0x000000ff));
    WriteByteEEPROM((unsigned char) (Address & 0x000000ff));
    WriteByteEEPROM(0x0);
    mEEPROMSSHigh();
    return (SPI2BUF);
}


void EEPROMEraseChip()
{
    EEPROMWriteEnable();                                            // Write enable the eeprom
    mEEPROMSSLow();
    WriteByteEEPROM(EEPROM_CMD_ERASE_CHIP);
    mEEPROMSSHigh();
    while ((EEPROMReadStatus() & 0x1) == 1);
}


void EEPROM_Write_Sector(unsigned long address, const BYTE *buffer)
{
     int nbytes = 256;
     unsigned char tmp;
     do
     {
        tmp = *buffer++;
        EEPROMWriteByte(tmp, address++);
        nbytes--;
     } while (nbytes > 0);
}


void EEPROM_wait_for_write_complete(void)
{
    while (EEPROMReadStatus1() & 0x01)
    {
    }
}


void EEPROMReadWriteTest(void)
{
    unsigned long Address = 0;

    EEPROMWriteEnable();
    EEPROMEraseChip();
    EEPROMWriteEnable();
    mEEPROMSSLow();
    WriteByteEEPROM(EEPROM_CMD_WRITE);
    WriteByteEEPROM((unsigned char) ((Address >> 16) & 0x000000ff));
    WriteByteEEPROM((unsigned char) ((Address >> 8) & 0x000000ff));
    WriteByteEEPROM((unsigned char) (Address & 0x000000ff));
    WriteByteEEPROM(0x42);
    mEEPROMSSHigh();
    EEPROM_wait_for_write_complete();
    printf("\n\rAfter write\r\n");
    mEEPROMSSLow();
    WriteByteEEPROM(EEPROM_CMD_READ);
    WriteByteEEPROM((unsigned char) ((Address >> 16) & 0x000000ff));
    WriteByteEEPROM((unsigned char) ((Address >> 8) & 0x000000ff));
    WriteByteEEPROM((unsigned char) (Address & 0x000000ff));
    WriteByteEEPROM(0x42);
    mEEPROMSSHigh();
    printf("\n\rSPI2BUF == 0x%04x\r\n", SPI2BUF);
}


void EEPROMEraseCluster(unsigned long Address)
{
    EEPROM_wait_for_write_complete();
    EEPROMWriteEnable();
    mEEPROMSSLow();
    WriteByteEEPROM(EEPROM_CMD_ERASE_CLUSTER);
    WriteByteEEPROM((unsigned char) ((Address >> 16) & 0x000000ff));
    WriteByteEEPROM((unsigned char) ((Address >> 8) & 0x000000ff));
    WriteByteEEPROM((unsigned char) (Address & 0x000000ff));
    DelayMs(500);                                                   // Erase should take 300ms but can't read status so wait 500ms to be safe
    mEEPROMSSHigh();
}


void EEPROM_Read_Sector(unsigned long Address, BYTE *buffer)
{
     int nbytes = 256;
     do
     {
        *buffer++ = EEPROMReadByte((unsigned long) Address++);
        nbytes--;
     } while (nbytes > 0);
}


unsigned char EEPROMReadStatus1()
{
    mEEPROMSSLow();
    while (SPI2STATbits.SPITBF);
    WriteByteEEPROM(EEPROM_CMD_RDSR1);
    WriteByteEEPROM(0);
    mEEPROMSSHigh();
    return (SPI2BUF);
}


void EEPROMReadManID()
{
    unsigned char cTmp;
    mEEPROMSSLow();
    WriteByteEEPROM(EEPROM_CMD_MANID);
    WriteByteEEPROM(0);
    WriteByteEEPROM(0);
    WriteByteEEPROM(0);
    WriteByteEEPROM(0);
    cTmp = SPI2BUF;
    printf("0x%02x  ", cTmp);
    WriteByteEEPROM(0);
    cTmp = SPI2BUF;
    printf("0x%02x\r\n\r\n", cTmp);
    mEEPROMSSHigh();
}



//************************ END OF FILE ****************************************


