/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/


#define SYS_FREQ        32000000L   // PNG - template was 8000000L
#define FCY             SYS_FREQ/2

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <libpic30.h>
/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>          /* For uint32_t definition */
#include <stdbool.h>         /* For true/false definition */
#include <uart.h>
#include <ports.h>
#include <spi.h>
#include "config.h"
#include "xmodem.h"
#include "memory.h"
#include "global.h"
#include "bootloader.h"
/* Constant definitions go here                                               */
#define BAUDRATE1               115200
#define BAUDRATE2               9600
#define SYSCLK                  32000000

#define BAUDRATEREG_BRGH_1      SYSCLK/8/BAUDRATE1-1
#define BAUDRATEREG_BRGH_2      SYSCLK/8/BAUDRATE2-1

#include "hwdefs.h"

#define RECEIVE_BUFFER_LEN 1024

WORD data_in_index;
WORD data_out_index;
WORD chars_in_buffer;
WORD max_chars;
BOOL dev_mode = FALSE;
WORD receive_data_in_index = 0;
WORD receive_data_out_index = 0;
BYTE rec = 0;                                                     // Front_panel UART items
BOOL message_rec = FALSE;
BOOL message_complete = FALSE;
int rx_index;
WORD chars_in_buffer;
unsigned char Rx2_data[256];
unsigned int buffer2_count = 0;
BYTE rec_buffer[20];
BYTE receive_buffer[RECEIVE_BUFFER_LEN];
BOOL rx2_complete = FALSE;


void ConfigureOscillator(void)
{

// Disable Watch Dog Timer
    RCONbits.SWDTEN = 0;

// When clock switch occurs switch to Prim Osc (HS, XT, EC)with PLL
    __builtin_write_OSCCONH(0x03);                                  // Set OSCCONH for clock switch
    __builtin_write_OSCCONL(0x01);                                  // Start clock switching
    while(OSCCONbits.COSC != 0b011);

// Wait for Clock switch to occur
// Wait for PLL to lock, if PLL is used
    while(OSCCONbits.LOCK != 1);



}

void InitApp(void)
{
// Setup analog functionality and port direction

//CONFIGURING UART1 & SPI1/2 PIN MAPPINGS
// SPI1 <-> WM8280, SPI2 <-> SPI Flash EEPROM
// Unlock Registers
    __builtin_write_OSCCONL(OSCCON & 0xffBF);

// Configure Input Functions (Table 10-2))
// **************************************
// SPI1 is used for connection to WM8280 and SPI2 to EEPROM
    RPINR20bits.SDI1R = 45;                                         // Assign SPI1 MISO to pin RPI45
    RPINR22bits.SDI2R = 27;                                         // SDI2 Data In SPI2 to RPI27

// UART1 is used for connection to FTDI, UART2 for USB and UART3 for BT
    RPINR18bits.U1RXR = 38;                                         // U1RX = RPI38
    RPINR19bits.U2RXR = 42;                                         // U2RX = RPI42
    RPINR17bits.U3RXR = 44;                                         // U3RX = RPI44

// Configure Output Functions (Table 10-3)
// **************************************
// SPI1 is used for connection to WM8280
    RPOR8bits.RP16R  = SS1OUT;                                      // Assign SPI1 Slave Select Out (nCS) (function 9) to RP16
    RPOR8bits.RP17R  = SDO1;                                        // Assign SPI1 Data Out (MOSI) (function 7) to RP17
    RPOR5bits.RP10R  = SCK1OUT;                                     // Assign SPI1 Clock Output (SCLK) (function 8) to RP10
    RPOR4bits.RP8R   = SS2OUT;                                      // nCS for SPI2
    RPOR7bits.RP14R  = SDO2;                                        // SPI2 MOSI (to EEPROM)
    RPOR14bits.RP29R = SCK2OUT;                                     // SCLK for SPI2

// UART1 is used for connection to FTDI, UART2 for USB and UART3 for BT
    RPOR10bits.RP21R = U1TX;                                        // Assign UART1 TX to RP21  (FDTI)
    RPOR6bits.RP12R  = U2TX;                                        // Assign UART2 TX to RP12  (2114)
    RPOR7bits.RP15R  = U3TX;                                        // Assign UART3 TX to RP15  (BT)

// Lock Registers
    __builtin_write_OSCCONL(OSCCON | 0x0040);

// Initialize peripherals
    SPI1STATbits.SPIEN = 0;
    SPI2STATbits.SPIEN = 0;

    SPI1STATbits.SPISIDL = 1;                                       // stop in Idle mode

    SPI1CON1bits.DISSCK = 0;                                        // enable internal SPI clk
    SPI1CON1bits.DISSDO = 0;                                        // SDO pin controlled by the module
    SPI1CON1bits.MODE16 = 0;                                        // xfers are 8-bits wide
    SPI1CON1bits.SMP    = 1;                                        // data sampled at end of data output time
    SPI1CON1bits.CKE = 0;                                           // CPHA ?
    SPI1CON1bits.SSEN = 1;                                          // Use SS pin for Slave Mode (assume this means module drives nCS in Master Mode)
    SPI1CON1bits.CKP = 0;                                           // CPOL ?
    SPI1CON1bits.MSTEN = 1;                                         // Master Mode
    SPI1CON1bits.PPRE = 1;                                          // Primary   Prescale = 16:1
    SPI1CON1bits.SPRE = 6;                                          // Secondary Prescale =  2:1  (gives SCK of 500KHz of 32Mhz Master Clock)

    SPI1CON2bits.FRMEN = 0;                                         // Framed support disabled
    SPI1CON2bits.SPIFSD = 0;                                        // Frame sync pulse output (master)
    SPI1CON2bits.SPIFPOL = 1;                                       // Frame sync pulse is active high
    SPI1CON2bits.SPIFE = 1;                                         // Frame sync pulse coincides with first bit clock
    SPI1CON2bits.SPIBEN = 0;                                        // Enhanced buffer disabled

// Configure SPI2 (SPI Flash EEPROM) interface
    SPI2CON1bits.DISSCK = 0;                                        // enable internal SPI clk
    SPI2CON1bits.DISSDO = 0;                                        // SDO pin controlled by the module
    SPI2CON1bits.MODE16 = 0;                                        // xfers are 8-bits wide
    SPI2CON1bits.SMP = 0;                                           // data sampled in middle of data output time
    SPI2CON1bits.CKE = 1;                                           // CPHA ?
    SPI2CON1bits.SSEN = 1;                                          // Use SS pin for Slave Mode (assume this means module drives nCS in Master Mode)
    SPI2CON1bits.CKP = 0;                                           // CPOL ?
    SPI2CON1bits.MSTEN = 1;                                         // Master Mode
    SPI2CON1bits.PPRE = 3;                                          // Prescale =
    SPI2CON1bits.SPRE = 6;

    SPI2CON2bits.FRMEN = 0;                                         // Framed support disabled
    SPI2CON2bits.SPIFSD = 0;                                        // Frame sync pulse output (master)
    SPI2CON2bits.SPIFPOL = 1;                                       // Frame sync pulse is active high
    SPI2CON2bits.SPIFE = 1;                                         // Frame sync pulse coincides with first bit clock
    SPI2CON2bits.SPIBEN = 0;                                        // Enhanced buffer disabled

// Initialize SPI Port 1 for 8280
    SPI1STATbits.SPIEN = 0;
    SPI1STAT = 0;
    SPI1CON1 = 0;
    SPI1CON2 = 0;
    SPI1CON1bits.MSTEN = 1;
    SPI1CON2 = 0;
    SPI1CON1bits.MODE16 = 0;                                        // 0 for 8 bit
    SPI1CON1bits.CKE = 1;                                           // 0= Serial output data changes on transition from Idle clock state to active clock state (see bit 6)
    SPI1CON1bits.CKP = 0;
    SPI1CON1bits.SMP = 0;                                           // 1= Input data sampled at end of data output time
    SPI1CON1bits.PPRE = 3;                                          // Prescale =
    SPI1CON1bits.SPRE = 6;                                          // Secondary Prescale =  2:1  (gives SCK of 500KHz of 32Mhz Master Clock)
    IEC0bits.SPI1IE = 0;
    IFS0bits.SPI1IF = 0;
    SPI1STATbits.SPIEN = 1;
    SPI2STATbits.SPIEN = 1;

    CNPU5bits.CN72PUE=1;
    LATFbits.LATF3 = 1;

// Initialize SPI Port 2 for eeprom
    SPI2STATbits.SPIEN = 0;
    SPI2STAT = 0;
    SPI2CON1 = 0;
    SPI2CON1bits.MSTEN = 1;
    SPI2CON2 = 0;
    SPI2CON1bits.MODE16 = 0;
    SPI2CON1bits.PPRE = 3;                                          // Primary   Prescale =
    SPI2CON1bits.SPRE = 6;                                          // Secondary Prescale =  2:1  (gives SCK of 500KHz of 32Mhz Master Clock)
    SPI2CON1bits.CKE = 1;
    SPI2CON1bits.CKP = 0;
    SPI2CON1bits.SMP = 0;
    IEC2bits.SPI2IE = 0;
    IFS2bits.SPI2IF = 0;
    SPI2STATbits.SPIEN = 1;

    LATBbits.LATB8 = 1;

// Initialize Uart 1 for PC communications 115200,No Parity, 8 bits, 1 stop bit
    U1BRG = BAUDRATEREG_BRGH_1;
    U1MODE = 0;
    U1MODEbits.BRGH = 1;
    U1STA = 0;
    U1STAbits.UTXISEL0=1;
    U1STAbits.URXISEL0=0;
    U1MODEbits.UARTEN = 1;
    U1STAbits.UTXEN = 1;
    IFS0bits.U1RXIF = 0;
    IEC0bits.U1RXIE = 1;
    IFS0bits.U1TXIF = 0;
    IPC2bits.U1RXIP = 1;

// Initialize Uart 2 for  USB interface 115200,No Parity, 8 bits, 1 stop bit
    U2BRG = BAUDRATEREG_BRGH_1;
    U2MODE = 0;
    U2MODEbits.BRGH = 1;
    U2STA = 0;
    U2STAbits.UTXISEL0 = 1;
    U2STAbits.URXISEL0 = 0;
    U2MODEbits.UARTEN = 1;
    U2STAbits.UTXEN = 1;
    IFS1bits.U2RXIF = 0;
    IEC1bits.U2RXIE = 1;
    IFS1bits.U2TXIF = 0;
    IPC7bits.U2RXIP = 1;

// Initialize Uart 3 for Bluetooth module 115200,No Parity, 8 bits, 1 stop bit
    U3BRG = BAUDRATEREG_BRGH_1;
    U3MODE = 0;
    U3MODEbits.BRGH = 1;
    U3STA = 0;
    U3STAbits.UTXISEL0 = 1;
    U3STAbits.URXISEL0 = 0;
    U3MODEbits.UARTEN = 1;
    U3STAbits.UTXEN = 1;
    IFS5bits.U3RXIF = 0;
    IFS5bits.U3TXIF = 0;
    IEC5bits.U3RXIE = 1;

// Initialise Timer4 for volume update delay
    T4CON = 0x0038;                                                 // Set 256 prescaler value
    TMR4 = 0x00;
    PR4 = 0xd555;                                                   // Set MSB of 32-bit counter to make MSB count in seconds
    PR5 = NV_DELAY;                                                 // Set NV write delay in seconds
    IPC7bits.T5IP = 0x01;
    IFS1bits.T5IF = 0;

    mPORTBOutputConfig(RESET_8280);

    mPORTCInputConfig(IRQ_8280 | 0x02);                             // FTDI_RX

    mPORTDInputConfig(BT_EVENT | HEADIN);

    mPORTDOutputConfig(0x0803);                                     // HID_TX, DACMUTE, AMPMUTE

    mPORTEOutputConfig(0x3ff);                                      // Configure PORT E as outputs for LEDs on 0-4 and USB control on 5-9

    mPORTFInputConfig(0xC4);                                        // BT_RTS, CIF1MISO, BT_RX

    mPORTFOutputConfig(0x13b);                                      // BT_CMD, BT_CTS, CIF1SS, CIF1SCLK, CIF1MOSI, BT_TX

    mPORTGInputConfig(SW_PWR_PAIR |                                 // Configure PORT G lines as button inputs (0,1,2,3,7,8)
                      SW_VOLUP    |
                      SW_VOLDN    |
                      SW_MODE     |
                      SW_PROCESS  |
                      SW_CALLPU);

    mPORTGOutputConfig(0x40);                                       // FDTI_TX

    DisableIntInputChange;

    InputChange_Clear_Intr_Status_Bit;
    ConfigIntCN(INT_ENABLE|INT_PRI_4);

// Set port directions
    TRISA = 0x0;                                                    // All outputs
    TRISB = 0xC0;                                                   // 6, 7
    TRISC = 0x100A;                                                 // 1, 3, 12
    TRISD = 0xF000;                                                 // 12, 13, 14, 15
    TRISE = 0x0;                                                    // All outputs
    TRISF = 0xC4;                                                   // 2, 6, 7
    TRISG = 0x38F;                                                  // 0, 1, 2, 3, 7, 8, 9

    AD1PCFGL = 0xffff;

    EnableIntInputChange;
}

BYTE getUartTerm(int ms_timeout)
{
    BYTE data;
    int cur_timeout = 0;

    while (receive_data_out_index == receive_data_in_index)        // If they are equal, no chars in buffer
    {
        __delay_ms(1);
        cur_timeout += 1;
        if (ms_timeout <= cur_timeout && ms_timeout != -1)
        {
            return -1;
        }
    }

    // Disable Rx interrupts for the appropriate UART.
#if TERMINAL_UART == 1
    IEC0bits.U1RXIE = 0;
#else
    IEC1bits.U2RXIE = 0;
#endif

    data = receive_buffer[receive_data_out_index];

    if (receive_data_out_index < (RECEIVE_BUFFER_LEN - 1))
        receive_data_out_index++;
    else
        receive_data_out_index = 0;

    // Enable Rx interrupts for the appropriate UART
#if TERMINAL_UART == 1
        IEC0bits.U1RXIE = 1;
#else
        IEC1bits.U2RXIE = 1;
#endif
    return (data);
}

// ResetDevice
//   Uses GOTO instruction to jump to a specified address.
void ResetDevice(WORD addr)
{
    asm("goto %0" : : "r"(addr));
}

int getcht(WORD timeout)
{
    int delay = timeout;                                            // Timeout in milliseconds
    BYTE data;

    while (receive_data_out_index == receive_data_in_index)         // If they are equal, no chars in buffer
    {
        if (timeout)                                                // If 0 bypass timeout and loop indefinitely waiting for data ready
        {
            __delay_ms(1);

            if (--delay <= 0)
                return -2;
        }
    }

#if TERMINAL_UART == 1
    IEC0bits.U1RXIE = 0;
#else
    IEC1bits.U2RXIE = 0;
#endif

    data = receive_buffer[receive_data_out_index];

    if (receive_data_out_index <= receive_data_in_index)
        chars_in_buffer = receive_data_out_index - receive_data_in_index + 1;
    else
        chars_in_buffer = RECEIVE_BUFFER_LEN - (receive_data_out_index - receive_data_in_index) + 1;

    if (receive_data_out_index < (RECEIVE_BUFFER_LEN - 1))
        receive_data_out_index++;
    else
        receive_data_out_index = 0;

#if TERMINAL_UART == 1
    IEC0bits.U1RXIE = 1;
#else
    IEC1bits.U2RXIE = 1;
#endif
    return (data);
}

#if TERMINAL_UART == 1
void putch2(char c)
{
    while (BusyUART1())
    {
    }
    putcUART1(c);
}
#else
void putch2(char c)
{
    while (BusyUART2())
    {
    }
    putcUART2(c);
}
#endif

// Contains state information about an XMODEM transfer.
// This data is provided in the XMODEM callback when
// new data has arrived.
typedef struct XModemData
{
    DWORD_VAL current_address;
    BYTE xbuff[134];   // 128 for XMODEM data plus headers.
    BYTE receive_buffer[PM_PAGE_SIZE];
    DWORD receive_buffer_index;
    DWORD bytes_received;
} XModemData;

// Initialize XModemData structure.
void XModemDataInit(XModemData *data)
{
    data->current_address.Val = 0;
    memset(data->receive_buffer, 0, sizeof(data->receive_buffer));
    data->receive_buffer_index = 0;
    data->bytes_received = 0;
}

// Callback for XMODEM transfer. It can be assumed that "size" will
// always be 128.
void xmodemCB(BYTE *data, int size, void *context)
{
    DWORD_VAL addr;
    unsigned int page;
    XModemData *xmd = (XModemData*) context;
    xmd->bytes_received += size;

    // Is this a new page?
    if (strncmp((const char*) data, "PAGE", 4) == 0)
    {
        sscanf((const char *) data, "PAGE %d", &page);
        // For now, do not write page 85 -- these are the configuration bits.
        if (page != 85)
        {
            // Need to erase this page. The page that is transferred
            // needs to be converted to the address that the eraser expects,
            // which is really the byte address divided by 2.
            addr.Val = page;  // To make sure it expands to 32-bit.
            addr.Val *= PM_PAGE_SIZE / 2;
            // Erase the page.
            ErasePM(1, addr);
            // Set up pointers to load the page data.
            xmd->current_address = addr;
            xmd->receive_buffer_index = 0;
        }
        else
        {
            xmd->current_address.Val = 0;
        }
    }
    else
    {
        // This is a data buffer. If xmd->current_address is not 0, then
        // we need to store this buffer, and write the page if the data buffer
        // is full. (Data is sent 128 bytes at a time, but a page is 2KB.)
        if (xmd->current_address.Val != 0)
        {
            // Copy this data to the receive buffer.
            memcpy(&xmd->receive_buffer[xmd->receive_buffer_index], data, size);
            xmd->receive_buffer_index += size;
            if (xmd->receive_buffer_index == sizeof(xmd->receive_buffer))
            {
                // Page is fully loaded, so write it out.
                WritePM(PM_PAGE_SIZE / PM_ROW_SIZE, xmd->current_address, xmd->receive_buffer);
            }
        }
    }
}

// Determines if the flash has a valid firmware image to launch once the
// bootloader is complete.
// The image is considered valid if there is a GOTO instruction at USER_PROG_RESET.
int is_firmware_image_valid()
{
    DWORD_VAL addr;
    DWORD_VAL instr;
    addr.Val = USER_PROG_RESET;
    ReadPM(1, addr, (BYTE *) &instr);
    return instr.word.HW == 0x0004;
}

// Resets the IVT. The bootloader uses the AIVT for its interrupts.
// The main program uses the IVT. This function remaps the main
// program's IVT to the IVT starting at address 4. If the IVT has
// already been remapped, this function does nothing. (This prevents
// an unnecessary flash write.)
// Also, this requires that the entire first page is rewritten, since the
// flash must be erased first, and the PIC24F only supports erasing an
// entire page.
void reset_ivt()
{
    int old_ipl;
    DWORD_VAL addr;
    DWORD_VAL user_ivt[PM_PAGE_SIZE / PM_INSTR_SIZE];
    DWORD_VAL current_ivt[PM_PAGE_SIZE / PM_INSTR_SIZE];

    // Read the first page to get the current IVT.
    addr.Val = 0;
    ReadPM(PM_PAGE_SIZE / PM_INSTR_SIZE, addr, (BYTE *) user_ivt);

    // Overwrite the first IVT with the application's IVT. This IVT is 126 words.
    // (118 interrupts plus 8 traps.)
    addr.Val = USER_IVT_BASE;
    ReadPM(126, addr, (BYTE *) &user_ivt[2]);

    // Compare this IVT with the currently stored IVT. If they are the same,
    // the flash does not need to be reprogrammed.
    addr.Val = 0;
    ReadPM(PM_PAGE_SIZE / PM_INSTR_SIZE, addr, (BYTE *) current_ivt);

    if (memcmp(current_ivt, user_ivt, sizeof(current_ivt)) != 0)
    {
        printf("Configuring IVT\n\r");
        // Disable interrupts while we reprogram the flash.
        SET_AND_SAVE_CPU_IPL(old_ipl, 7);
        // Erase the first page.
        addr.Val = 0;
        ErasePM(1, addr);
        // And write the new page back.
        WritePM(PM_PAGE_SIZE / PM_ROW_SIZE, addr, (BYTE*) user_ivt);
        RESTORE_CPU_IPL(old_ipl);
    }
}

DECLARE_LAUNCH_BOOTLOADER;

void init_persistent_variables()
{
    if ((RCONbits.POR == 0) &&
        (RCONbits.BOR == 0))
    {
        // Not a cold boot, so leave persistent variables alone.
    }
    else
    {
        set_launch_bootloader(0);
    }

    // According to Microchip, these bits need to be cleared when no longer
    // needed. On a power-on reset, POR will be set back to 1.
    RCONbits.POR = 0;
    RCONbits.BOR = 0;
}

void display_menu()
{
    printf("D - Download firmware\n\r");
    printf("L - Launch normally\n\r");
}

char get_menu_selection()
{
    printf("Enter selection: ");
    char c = getUartTerm(-1);
    printf("%c\n\r", c);
    return c;
}

void download_firmware()
{
    XModemData data;
    XModemDataInit(&data);
    printf("Begin new firmware transfer...\n\r");
    xmodemReceive(data.xbuff, xmodemCB, &data);
    printf("\n\rTransfer completed\n\r");
}

/*
 * 
 */
int main(int argc, char** argv) {

    BYTE char_hit;
    int menu_loop;
    __C30_UART = TERMINAL_UART;
    INTCON2bits.ALTIVT = 1;
    init_persistent_variables();
    ConfigureOscillator();

    InitApp();
    USB_RST(LOW);                                                   // USB and 8280 resets non active
    RST_8280(HIGH);
    BT_CMD(HIGH);
    DACMUTE(HIGH);                                                  // Ensure DAC and AMPs muted
    AMPMUTE(LOW);

    // Need to initialize these lines or else the Windows volume goes loopy.
    USB_VOLDN(HIGH);                                                // Initialise discrete output lines for USB volume & mute
    USB_VOLUP(HIGH);
    USB_PMUTE(HIGH);
    USB_RMUTE(HIGH);

    __delay_ms(500);
    // Give the user one second to press 'y' to force a bootloader launch.
    // There is no indication to an end user that this is happening.
    printf("\n\rComhear Bootloader %s\n\r", SW_VERSION);
    printf("Copyright 2015 Comhear, Inc - All rights reserved\n\r");
    char_hit = getUartTerm(1000);
    if (char_hit == 'y')
    {
        set_launch_bootloader(1);
    }

    while (1)
    {
        if (should_launch_bootloader() || !is_firmware_image_valid())
        {
            menu_loop = 1;
            while (menu_loop)
            {
                display_menu();
                char_hit = get_menu_selection();
                switch(tolower(char_hit))
                {
                    case 'd':
                        download_firmware();
                        printf("Launching...\n\r");
                        menu_loop = 0;
                        break;
                    case 'l':
                        menu_loop = 0;
                        break;
                    default:
                        printf("\n\rInvalid selection\n\r");
                }
            }
        }

        set_launch_bootloader(0);

        if (is_firmware_image_valid())
        {

            // Load the main program's IVT into main_ivt;
            reset_ivt();

            // Switch to the main IVT.
            INTCON2bits.ALTIVT = 0;
            // Jump to the application code. This will never return.
            ResetDevice(USER_PROG_RESET);
        }
        else
        {
            printf("\n\r*** No firmware image found. Image must be reloaded. ***\n\r");
        }
    }

    return (EXIT_SUCCESS);
}

