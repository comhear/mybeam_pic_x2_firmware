/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

/* XMODEM Download Implementation
 * Copyright 2001-2010 Georges Menie (www.menie.org)
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University of California, Berkeley nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "config.h"
#include "xmodem.h"

extern WORD chars_in_buffer;
extern WORD data_in_index;
extern WORD data_out_index;
int getcht(WORD timeout);
void putch2(char c);

static void flushinput(void)
{
    while (getcht(((DLY_1S) * 3) >> 1) >= 0)
    {
    }
}

unsigned short crc16_ccitt(const unsigned char *buf, int len)
{
    unsigned short crc = 0;
    while (len--)
    {
        int i;
        crc ^= *(char *) buf++ << 8;
        for (i = 0; i < 8; ++i)
        {
            if (crc & 0x8000)
                crc = (crc << 1) ^ 0x1021;
            else
                crc = crc << 1;
        }
    }
    return crc;
}

static int check(int crc, const unsigned char *buf, int sz)
{
    if (crc)
    {
        unsigned short crc = crc16_ccitt(buf, sz);
        unsigned short tcrc = (buf[sz] << 8) + buf[sz + 1];
        if (crc == tcrc)
            return 1;
    }
    else
    {
        int i;
        unsigned char cks = 0;
        for (i = 0; i < sz; ++i)
        {
            cks += buf[i];
        }
        if (cks == buf[sz])
            return 1;
    }

    return 0;
}

long xmodemReceive(BYTE xbuff[134], xmodemCallback callback, void *context)
{
    unsigned char *p;
    int bufsz, crc = 0;
    unsigned char trychar = 'C';
    unsigned char packetno = 1;
    int i, c;
    int retry, retrans = MAXRETRANS;
    DWORD len = 0;

    data_in_index = 0;                                              // Used in save_file()
    data_out_index = 0;                                             // Used in save_file()

    for (;;)
    {
        for (retry = 0; retry < 64; ++retry)
        {
            if (trychar)
            {
                putch2(trychar);
            }
            if ((c = getcht(1000)) >= 0)
            {
                switch (c)
                {
                case SOH:
                    bufsz = 128;
                    goto start_recv;
                case EOT:
                    flushinput();
                    putch2(ACK);
                    len -= 4;
                    return len;                                     // Normal end
                case CAN:
                    if ((c = getcht(1000)) == CAN)
                    {
                        flushinput();
                        putch2(ACK);
                        return -1;                                  // Canceled by remote
                    }
                    break;
                default:
                    break;
                }
            }
        }

        if (trychar == 'C')
        {
            trychar = NAK;
            continue;
        }
        flushinput();
        putch2(CAN);
        putch2(CAN);
        putch2(CAN);
        return -2;                                                  // Sync error

start_recv:
        if (trychar == 'C')
            crc = 1;
        trychar = 0;
        p = xbuff;
        *p++ = c;

        for (i = 0; i < (bufsz + (crc ? 1 : 0) + 3); ++i)
        {
            if ((c = getcht(DLY_1S)) < 0)
                goto reject;
            len++;
            *p++ = c;
        }

        if (xbuff[1] == (unsigned char) (~xbuff[2]) &&
                (xbuff[1] == packetno || xbuff[1] == (unsigned char) packetno - 1) &&
                check(crc, &xbuff[3], bufsz))
        {
            if (xbuff[1] == packetno)
            {
                (*callback)(&xbuff[3], 128, context);

                ++packetno;
                retrans = MAXRETRANS + 1;
            }
            if (--retrans <= 0)
            {
                flushinput();
                putch2(CAN);
                putch2(CAN);
                putch2(CAN);
                return -3;                                          // Too many retry error
            }
            putch2(ACK);
            continue;
        }

reject:

        flushinput();
        putch2(NAK);
    }
}

