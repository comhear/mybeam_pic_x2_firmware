/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

#ifndef CONFIG_H
#define	CONFIG_H

#include <p24fxxxx.h>
#include "GenericTypeDefs.h"

//Self-write NVMCON opcodes
#if defined(__PIC24F__)     //PIC24F "J" type devices
    #define PM_PAGE_ERASE 		0x4042	//NVM page erase opcode
    #define PM_ROW_WRITE 		0x4001	//NVM row write opcode

#elif defined(__PIC24FK__)  //PIC24F "K" type devices
    #define PM_PAGE_ERASE 		0x4058	//NVM page erase opcode
    #define PM_ROW_WRITE 		0x4004	//NVM row write opcode
#endif

#ifdef	__cplusplus
extern "C" {
#endif

#define USER_PROG_RESET 0x4000
#define USER_IVT_BASE   (USER_PROG_RESET + 4)
#define USER_DATA_BASE  0x800
#define USER_DATA_LEN   0x3000
#define PM_INSTR_SIZE 		4		//bytes per instruction
#define PM_ROW_SIZE 		256  	//user flash row size in bytes
#define PM_PAGE_SIZE   2048

#define CONFIG_START 		0x157FC
#define CONFIG_END   		0x157FE

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

