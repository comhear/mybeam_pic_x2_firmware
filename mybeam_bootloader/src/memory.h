/*******************************************************************************
 *
 * Copyright (c) 2015 ComHear, Inc.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license. The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by ComHear, Inc.
 *
 * ComHear, Inc assumes no responsibility or liability for any errors or
 * inaccuracies that may appear in this document or any software that may be
 * provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of ComHear, Inc.
 *
 ******************************************************************************/

#ifndef MEMORY_H
#define	MEMORY_H

#ifdef	__cplusplus
extern "C" {
#endif

//void Erase(WORD page, WORD addrLo, WORD cmd);
void ErasePM(WORD length, DWORD_VAL sourceAddr);
void ReadPM(WORD length, DWORD_VAL sourceAddr, BYTE *buffer);
void WritePM(WORD length, DWORD_VAL sourceAddr, const BYTE *buffer);

#ifdef	__cplusplus
}
#endif

#endif	/* MEMORY_H */

